//----------------------------------------------------------------------------//
// BLINKY.C :SafeRTOS Blinky Task for TI TMS570LC43xHDK
//----------------------------------------------------------------------------//
//Thomas Latrofa
//Zoomlion Heavy Industries North America
//11-25-2019
#define BLINKY_C
//----------------------------------------------------------------------------//
// SafeRTOS Includes
//----------------------------------------------------------------------------//
#include "SafeRTOS_API.h"
//----------------------------------------------------------------------------//
//HALCoGen Includes
//----------------------------------------------------------------------------//
#include "HL_gio.h"
#include "HL_het.h"
//----------------------------------------------------------------------------//
// TASK Includes
//----------------------------------------------------------------------------//
#include "PortSpecifics.h"
#include "blinky.h"
//----------------------------------------------------------------------------//
// END Includes
//----------------------------------------------------------------------------//
//
//----------------------------------------------------------------------------//
// FUNCTION Declarations
//----------------------------------------------------------------------------//
portBaseType xStartBlinky( void );
void vBlinky(void *pvParameters);
//----------------------------------------------------------------------------//
//Task Control Block
//----------------------------------------------------------------------------//
portspecTCB_DATA_SECTION static xTCB xBlinkyTCB = { 0 };
//----------------------------------------------------------------------------//
//
//----------------------------------------------------------------------------//
// xStartBlinky()
//      Creates Task
//----------------------------------------------------------------------------//
portBaseType xStartBlinky( void )
{
hetInit();
gioInit();
portBaseType xStatus = pdPASS;
xTaskParameters xBlinkyParameters =
{
	vBlinky,			/* Task Code */
	"vBlinky",			/* Task Name */
	&xBlinkyTCB,			/* TCB */
	acBlinkyStack,			/* Stack Buffer */
	portspecBLINKY_STACK_SIZE,	/* Stack Depth Bytes */
	NULL,	                        /* Parameters */
	taskIDLE_PRIORITY,		/* Priority */
	NULL,			        /* TLS Object */
	xBlinkyPortParameters		/* Port Specific Task Parameters */
};
	/* Create the unprivileged test tasks. */
    if( xTaskCreate( &xBlinkyParameters, NULL ) != pdPASS )
	{
		xStatus = pdFAIL;
	}
    return xStatus;
}
//
//----------------------------------------------------------------------------//
// vBlinky()
//      Blinky Task
//----------------------------------------------------------------------------//
void vBlinky(void *pvParameters)
{
  //LED Pwm output put duty cycles
  uint32_t puw_PwmOut[8] = {0U,5U,10U,15U,25U,30U,70U,0};
  //index variable for array rotation
  uint32_t a = 0U;
  //Previous and current state variables
  uint8_t ub_pLedState = 1U;
  uint8_t ub_cLedState = 0U;
  uint8_t ub_pBtnState = 0U;
  uint8_t ub_cBtnState = 0U;
  while(1)
  {
    ub_cBtnState = (uint8_t) gioGetBit(gioPORTA, 7U);
    if( ub_pBtnState != ub_cBtnState)	//Check for State Change
    {
      ub_pBtnState = ub_cBtnState;      //Save context for future
      if(ub_pBtnState == 1U)		//Button High
      {
        ub_cLedState = 1U;
      }
      else 				//Button Low
      {
        ub_cLedState = 0U;
      }
      ub_pLedState = ub_cLedState;
    }
    else if(ub_pLedState == ub_cLedState) //No State Change - Rotate array
    {
      if(ub_cLedState) //rotation dir based on button state
      {
        
        a++;
      }
      else
      {
        a--;
      }
      pwmSetDuty(hetRAM1, (0U + a) & 7U, puw_PwmOut[0U]);
      pwmSetDuty(hetRAM1, (1U + a) & 7U, puw_PwmOut[1U]);
      pwmSetDuty(hetRAM1, (2U + a) & 7U, puw_PwmOut[2U]);
      pwmSetDuty(hetRAM1, (3U + a) & 7U, puw_PwmOut[3U]);
      pwmSetDuty(hetRAM1, (4U + a) & 7U, puw_PwmOut[4U]);
      pwmSetDuty(hetRAM1, (5U + a) & 7U, puw_PwmOut[5U]);
      pwmSetDuty(hetRAM1, (6U + a) & 7U, puw_PwmOut[6U]);
      pwmSetDuty(hetRAM1, (7U + a) & 7U, puw_PwmOut[7U]);
    }
    else //should never end up here
    {
      while(1U);
    }
    xTaskDelay( 100U ); //Relinquish control to RTOS
  }
}
//----------------------------------------------------------------------------//
// EOF
//----------------------------------------------------------------------------//