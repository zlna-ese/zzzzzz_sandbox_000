
/*
 * Creates six tasks that operate on three queues as follows:
 *
 * The first two tasks send and receive an incrementing number to/from a queue.
 * One task acts as a producer and the other as the consumer.  The consumer is a
 * higher priority than the producer and is set to block on queue reads.  The queue
 * only has space for one item - as soon as the producer posts a message on the
 * queue the consumer will unblock, pre-empt the producer, and remove the item.
 *
 * The second two tasks work the other way around.  Again the queue used only has
 * enough space for one item.  This time the consumer has a lower priority than the
 * producer.  The producer will try to post on the queue blocking when the queue is
 * full.  When the consumer wakes it will remove the item from the queue, causing
 * the producer to unblock, pre-empt the consumer, and immediately re-fill the
 * queue.
 *
 * The last two tasks use the same queue producer and consumer functions.  This time the queue has
 * enough space for lots of items and the tasks operate at the same priority.  The
 * producer will execute, placing items into the queue.  The consumer will start
 * executing when either the queue becomes full (causing the producer to block) or
 * a context switch occurs (tasks of the same priority will time slice).
 *
 */

/* PortSpecifics.h includes some 'declare once' declarations. */
#define BLINKYQ_C

#include <stdlib.h>

/* Scheduler include files. */
#include "SafeRTOS_API.h"


/* Demo program include files. */
#include "blinkyQ.h"
#include "HL_het.h"
#include "HL_gio.h"
#include "PortSpecifics.h"

#if ( configQUEUE_REGISTRY_SIZE > 0 )
#include "queue_register.h"
#endif

#define blinkyqNUM_TASK_SETS		( 3 )

/* Structure used to pass parameters to the blocking queue tasks. */
typedef struct BLINKY_QUEUE_PARAMETERS
{
	xQueueHandle xQueue;					/*< The queue to be used by the task. */
	portTickType xBlockTime;				/*< The block time to use on queue reads/writes. */
	volatile portInt16Type *psCheckVariable;	/*< Incremented on each successful cycle to check the task is still running. */
} xBlinkyQueueParameters;

/* Queue Parameters. */
portspecBLINKY_Q_DATA_SECTION static xBlinkyQueueParameters xQueueParameters1 = { NULL, 100, NULL };
portspecBLINKY_Q_DATA_SECTION static xBlinkyQueueParameters xQueueParameters2 = { NULL, 0, NULL };
/* Task function that creates an incrementing number and posts it on a queue. */
static void vBlinkyQueueProducer( void *pvParameters );

/* Task function that removes the incrementing number from a queue and checks that
it is the expected number. */
static void vBlinkyQueueConsumer( void *pvParameters );

/* Variables which are incremented each time an item is removed from a queue, and
found to be the expected value.
These are used to check that the tasks are still running. */
portspecBLINKY_Q_DATA_SECTION static volatile portInt16Type asBlockingConsumerCount[ blinkyqNUM_TASK_SETS ] = { ( portInt16Type ) 0, ( portInt16Type ) 0, ( portInt16Type ) 0 };

/* Variable which are incremented each time an item is posted on a queue.   These
are used to check that the tasks are still running. */
portspecBLINKY_Q_DATA_SECTION static volatile portInt16Type asBlockingProducerCount[ blinkyqNUM_TASK_SETS ] = { ( portInt16Type ) 0, ( portInt16Type ) 0, ( portInt16Type ) 0 };

/* Blocking Queue Task TCBs. */
portspecTCB_DATA_SECTION static xTCB xBlinkyQueueConsumerTask1TCB = { 0 };
portspecTCB_DATA_SECTION static xTCB xBlinkyQueueProducerTask1TCB = { 0 };

/*-----------------------------------------------------------*/

portBaseType xStartBlinkyQueueTasks( portUnsignedBaseType uxPriority )
{
hetInit();
gioInit();
const portTickType xBlockTime = ( portTickType ) 1000 / configTICK_RATE_MS;
const portTickType xDontBlock = ( portTickType ) 0;
portBaseType xStatus = pdPASS;
xTaskParameters xBlinkyQueueConsumerTask1Parameters =
{
	vBlinkyQueueConsumer,						/* Task Code */
	"QBlinkyCons",									/* Task Name */
	&xBlinkyQueueConsumerTask1TCB,			/* TCB */
	acBlinkyQTask1Stack,						/* Stack Buffer */
	portspecBLINKY_Q_STACK_SIZE,					/* Stack Depth Bytes */
	( void * ) &xQueueParameters1,				/* Parameters */
	taskIDLE_PRIORITY,							/* Priority */
	NULL,										/* TLS Object */
	xBlinkyQueueConsumerTask1PortParameters	/* Port Specific Task Parameters */
};
xTaskParameters xBlinkyQueueProducerTask1Parameters =
{
	vBlinkyQueueProducer,						/* Task Code */
	"QBlinkyProd",									/* Task Name */
	&xBlinkyQueueProducerTask1TCB,			/* TCB */
	acBlinkyQTask2Stack,						/* Stack Buffer */
	portspecBLINKY_Q_STACK_SIZE,					/* Stack Depth Bytes */
	( void * ) &xQueueParameters2,				/* Parameters */
	taskIDLE_PRIORITY,							/* Priority */
	NULL,										/* TLS Object */
	xBlinkyQueueProducerTask1PortParameters	/* Port Specific Task Parameters */
};


	/* Create the queue used by the first two tasks to pass the incrementing number.
	Pass a pointer to the queue in the parameters to be used to create the consumer tasks. */
	if( xQueueCreate( acBlinkyQBuffer, portspecBLINKY_Q_BUFFER_LENGTH_1, portspecBLINKY_Q_QUEUE_LENGTH_1, portspecBLINKY_Q_QUEUE_ITEM_SIZE, &( xQueueParameters1.xQueue ) ) == pdPASS )
	{
#if ( configQUEUE_REGISTRY_SIZE > 0 )
		vQueueAddToRegistry( xQueueParameters1.xQueue, "BlockQ #1" );
#endif
	}
	else
	{
		xStatus = pdFAIL;
	}

	/* The consumer is created first so gets a block time as described above. */
	xQueueParameters1.xBlockTime = xBlockTime;

	/* Pass in the variable that this task is going to increment so we can check it
	is still running. */
	xQueueParameters1.psCheckVariable = &( asBlockingConsumerCount[ 0 ] );

	/* Pass the queue to the structure used to pass parameters to the producer task. */
	xQueueParameters2.xQueue = xQueueParameters1.xQueue;

	/* The producer is not going to block - as soon as it posts the consumer will
	wake and remove the item so the producer should always have room to post. */
	xQueueParameters2.xBlockTime = xBlockTime;

	/* Pass in the variable that this task is going to increment so we can check
	it is still running. */
	xQueueParameters2.psCheckVariable = &( asBlockingProducerCount[ 0 ] );

	/* Create the first two tasks as described at the top of the file. Note the
	producer has a lower priority than the consumer when the tasks are spawned. */
	xBlinkyQueueConsumerTask1Parameters.uxPriority = uxPriority;
    if( xTaskCreate( &xBlinkyQueueConsumerTask1Parameters, NULL ) != pdPASS )
	{
		xStatus = pdFAIL;
	}
    if( xTaskCreate( &xBlinkyQueueProducerTask1Parameters, NULL ) != pdPASS )
	{
		xStatus = pdFAIL;
	}
	return xStatus;
}
/*-----------------------------------------------------------*/

static void vBlinkyQueueProducer( void *pvParameters )
{
portUInt16Type usValue = 0;
xBlinkyQueueParameters *pxQueueParameters;
portBaseType xErrorEverOccurred = pdFALSE;

	pxQueueParameters = ( xBlinkyQueueParameters * ) pvParameters;

	for( ;; )
	{
                usValue = (uint16_t) gioGetBit(gioPORTA, 7U);
		if( xQueueSend( pxQueueParameters->xQueue, ( void * ) &usValue, pxQueueParameters->xBlockTime ) != pdPASS )
		{
			xErrorEverOccurred = pdTRUE;
		}
		else
		{
			/* We have successfully posted a message, so increment the variable
			used to check we are still running. */
			if( pdFALSE == xErrorEverOccurred )
			{
				( *pxQueueParameters->psCheckVariable )++;
			}

			/* Increment the variable we are going to post next time round.  The
			consumer will expect the numbers to	follow in numerical order. */
			++usValue;
		}
	}
}
/*-----------------------------------------------------------*/

static void vBlinkyQueueConsumer( void *pvParameters )
{
portUInt16Type usData, usExpectedValue = 0;
xBlinkyQueueParameters *pxQueueParameters;
portBaseType xErrorEverOccurred = pdFALSE;
uint8_t ub_pState = 0U;
uint8_t ub_cState = 0U;
uint32_t puw_PwmOut[8] = {0U,5U,10U,15U,25U,30U,70U,0};
  //index variable for array rotation
uint32_t a = 0U;
  //Previous and current state variables
pxQueueParameters = ( xBlinkyQueueParameters * ) pvParameters;
for( ;; )
{
  if( xQueueReceive( pxQueueParameters->xQueue, &usData, pxQueueParameters->xBlockTime ) == pdPASS )
  {
    if( ub_pState != (uint8_t) usData)	//Check for State Change
    {
      ub_pState = usData;      //Save context for future
    }
    if(ub_pState) //rotation dir based on button state
    {
      a++;
    }
    else
    {
      a--;
    }
    pwmSetDuty(hetRAM1, (0U + a) & 7U, puw_PwmOut[0U]);
    pwmSetDuty(hetRAM1, (1U + a) & 7U, puw_PwmOut[1U]);
    pwmSetDuty(hetRAM1, (2U + a) & 7U, puw_PwmOut[2U]);
    pwmSetDuty(hetRAM1, (3U + a) & 7U, puw_PwmOut[3U]);
    pwmSetDuty(hetRAM1, (4U + a) & 7U, puw_PwmOut[4U]);
    pwmSetDuty(hetRAM1, (5U + a) & 7U, puw_PwmOut[5U]);
    pwmSetDuty(hetRAM1, (6U + a) & 7U, puw_PwmOut[6U]);
    pwmSetDuty(hetRAM1, (7U + a) & 7U, puw_PwmOut[7U]);  
  }
  else
  {
  /* We have successfully received a message, so increment the
  variable used to check we are still running. */
    if( pdFALSE == xErrorEverOccurred )
    {
      ( *pxQueueParameters->psCheckVariable )++;
    }
  /* Increment the value we expect to remove from the queue next time
  round. */
  ++usExpectedValue;
  }  
  xTaskDelay( 100U );
  }
}
/*-----------------------------------------------------------*/

/* This is called to check that all the created tasks are still running. */
portBaseType xAreBlinkyQueuesStillRunning( void )
{
//portspecCOMMON_PRIV_DATA_SECTION static portInt16Type asLastBlockingConsumerCount[ blinkyqNUM_TASK_SETS ] = { ( portInt16Type  ) 0, ( portInt16Type ) 0, ( portInt16Type ) 0 };
//portspecCOMMON_PRIV_DATA_SECTION static portInt16Type asLastBlockingProducerCount[ blinkyqNUM_TASK_SETS ] = { ( portInt16Type ) 0, ( portInt16Type ) 0, ( portInt16Type ) 0 };
//portBaseType xReturn = pdPASS, xTasks;

	/* Not too worried about mutual exclusion on these variables as they are 16
	bits and we are only reading them. We also only care to see if they have
	changed or not.

	Loop through each check variable to and return pdFALSE if any are found not
	to have changed since the last call. */

/*	for( xTasks = 0; xTasks < blckqNUM_TASK_SETS; xTasks++ )
	{
		if( asBlockingConsumerCount[ xTasks ] == asLastBlockingConsumerCount[ xTasks ]  )
		{
			xReturn = pdFALSE;
		}
		asLastBlockingConsumerCount[ xTasks ] = asBlockingConsumerCount[ xTasks ];


		if( asBlockingProducerCount[ xTasks ] == asLastBlockingProducerCount[ xTasks ]  )
		{
			xReturn = pdFALSE;
		}
		asLastBlockingProducerCount[ xTasks ] = asBlockingProducerCount[ xTasks ];
	}
*/
	//return xReturn;
}

