//----------------------------------------------------------------------------//
// BLINKY.H :SafeRTOS Blinky Task for TI TMS570LC43xHDK
//----------------------------------------------------------------------------//
//Thomas Latrofa
//Zoomlion Heavy Industries North America
//11-25-2019
#ifndef BLINKY_H
#define BLINKY_H
//----------------------------------------------------------------------------//
// xStartBlinky()
//      Creates Blinky Task
//----------------------------------------------------------------------------//
portBaseType xStartBlinky( void );
#endif /* BLINKY_H */