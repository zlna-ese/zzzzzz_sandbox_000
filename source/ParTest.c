/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

/*--------------------------------------------------------------------------*/
/* Simple IO routines for the LEDs.											*/
/*--------------------------------------------------------------------------*/

/* SafeRTOS includes. */
#include "SafeRTOS_API.h"

/* Demo application includes. */
#include "partest.h"


/*--------------------------------------------------------------------------*/
/* Local constant definitions												*/
/*--------------------------------------------------------------------------*/
/* Register definitions. */
#define partestN2HET1_GLOBAL_CONFIGURATION_REGISTER		( *( volatile portUInt32Type * )0xFFF7B800UL )
#define partestN2HET1_DIRECTION_REGISTER				( *( volatile portUInt32Type * )0xFFF7B84CUL )
#define partestN2HET1_DATA_OUTPUT_REGISTER				( *( volatile portUInt32Type * )0xFFF7B854UL )
#define partestN2HET1_PULL_DISABLE_REGISTER				( *( volatile portUInt32Type * )0xFFF7B864UL )

/* Bit masks within the Global Configuration Register. */
#define partestHET_PIN_ENABLE_BIT						( 1UL << 24 )
#define partestTURN_ON_OFF_BIT							( 1UL )

/* There are 8 LEDs connected to N2HET1. */
#define partestNUMBER_OF_USER_LEDS						( ( portUnsignedBaseType ) 8 )
#define partestMASK_ALL_LEDS							( ( 1UL << 17 ) | ( 1UL << 31 ) | ( 1UL << 0 )  | \
														  ( 1UL << 25 ) | ( 1UL << 18 ) | ( 1UL << 29 ) | \
                                                          ( 1UL << 27 ) | ( 1UL << 5  ) )


/*--------------------------------------------------------------------------*/
/* Local variable declarations												*/
/*--------------------------------------------------------------------------*/
/* Bit offset of each of the pins that control an LED. */
const portUInt32Type ulLed_N2het1_Offset[ partestNUMBER_OF_USER_LEDS ] =
{
	/* White LEDs */
	17, /* PCB Ident: D3,   Top Left		*/
	31, /* PCB Ident: D4,   Top Middle		*/
	 0, /* PCB Ident: D5,   Top Right		*/
	27, /* PCB Ident: LED1, Middle Left		*/
	 5, /* PCB Ident: LED2, Middle Right	*/
	29, /* PCB Ident: D8,   Bottom Left		*/
	18, /* PCB Ident: D7,   Bottom Middle	*/
	25, /* PCB Ident: D6,   Bottom Right	*/
};

/*--------------------------------------------------------------------------*/
/* Public function definitions												*/
/*--------------------------------------------------------------------------*/

void vParTestInitialise( void )
{
	/* Configure N2HET1 as Master and enable the output buffer. */
	partestN2HET1_GLOBAL_CONFIGURATION_REGISTER = ( partestHET_PIN_ENABLE_BIT | partestTURN_ON_OFF_BIT );

	/* Enable the Pull functionality on all LED control pins. */
	partestN2HET1_PULL_DISABLE_REGISTER &= ~partestMASK_ALL_LEDS;

	/* Configure the LED pins as outputs. */
	partestN2HET1_DIRECTION_REGISTER = partestMASK_ALL_LEDS;

	/* Turn a LED indicators off */
	partestN2HET1_DATA_OUTPUT_REGISTER &= ~partestMASK_ALL_LEDS;
}
/*--------------------------------------------------------------------------*/

void vParTestSetLED( portUnsignedBaseType uxLED, portBaseType xValue )
{
	if( uxLED < partestNUMBER_OF_USER_LEDS )
	{
		if( partestLED_OFF == xValue )
		{
			partestN2HET1_DATA_OUTPUT_REGISTER &= ~( partestLED_ON << ulLed_N2het1_Offset[ uxLED ] );
		}
		else
		{
			partestN2HET1_DATA_OUTPUT_REGISTER |= ( partestLED_ON << ulLed_N2het1_Offset[ uxLED ] );
		}
	}
}
/*--------------------------------------------------------------------------*/

void vParTestToggleLED( portUnsignedBaseType uxLED )
{
	if( uxLED < partestNUMBER_OF_USER_LEDS )
	{
		if( partestLED_OFF == ( partestN2HET1_DATA_OUTPUT_REGISTER & ( partestLED_ON << ulLed_N2het1_Offset[ uxLED ] ) ) )
		{
			partestN2HET1_DATA_OUTPUT_REGISTER |= ( partestLED_ON << ulLed_N2het1_Offset[ uxLED ] );
		}
		else
		{
			partestN2HET1_DATA_OUTPUT_REGISTER &= ~( partestLED_ON << ulLed_N2het1_Offset[ uxLED ] );
		}
	}
}
/*--------------------------------------------------------------------------*/
