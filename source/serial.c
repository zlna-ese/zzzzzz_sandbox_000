/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

/* Scheduler include files. */
#define COMTEST_C

#include "SafeRTOS_API.h"

/* Demo application includes. */
#include "serial.h"
#include "PortSpecifics.h"

/*-----------------------------------------------------------------------------
 * Constant definitions
 *---------------------------------------------------------------------------*/

#define serialSCI_1_GCR0_REGISTER			( *( volatile portUInt32Type * ) 0xFFF7E400 )
#define serialSCI_GCR0_RESET_BIT			( ( portUInt32Type ) 0x00000001 )

#define serialSCI_1_GCR1_REGISTER			( *( volatile portUInt32Type * ) 0xFFF7E404 )
#define serialSCI_GCR1_TXENA_BIT			( ( portUInt32Type ) 0x02000000 )
#define serialSCI_GCR1_RXENA_BIT			( ( portUInt32Type ) 0x01000000 )
#define serialSCI_GCR1_CONT_BIT				( ( portUInt32Type ) 0x00020000 )
#define serialSCI_GCR1_LOOPBACK_BIT			( ( portUInt32Type ) 0x00010000 )
#define serialSCI_GCR1_MBUF_MODE_BIT		( ( portUInt32Type ) 0x00000000 )
#define serialSCI_GCR1_SWNRST_BIT			( ( portUInt32Type ) 0x00000080 )
#define serialSCI_GCR1_CLOCK_BIT			( ( portUInt32Type ) 0x00000020 )
#define serialSCI_GCR1_1_STOP_BIT			( ( portUInt32Type ) 0x00000000 )
#define serialSCI_GCR1_NO_PARITY			( ( portUInt32Type ) 0x00000000 )

#define serialSCI_1_SET_INT_REGISTER		( *( volatile portUInt32Type * ) 0xFFF7E40C )
#define serialSET_RX_INT_BIT				( ( portUInt32Type ) 0x00000200 )
#define serialSET_TX_INT_BIT				( ( portUInt32Type ) 0x00000100 )

#define serialSCI_1_INT_VECT_0_REGISTER		( *( volatile portUInt32Type * ) 0xFFF7E420 )
#define serialSOURCE_RECEIVE				( ( portUInt32Type ) 11 )
#define serialSOURCE_TRANSMIT				( ( portUInt32Type ) 12 )

#define serialSCI_1_FORMAT_REGISTER			( *( volatile portUInt32Type * ) 0xFFF7E428 )
#define serialSCI_FORMAT_8_BIT_CHARS		( ( portUInt32Type ) 0x00000007 )

#define serialSCI_1_BRS_REGISTER			( *( volatile portUInt32Type * ) 0xFFF7E42C )
#define serialCLOCK_SCALING					( ( portUInt32Type ) 16 )
#define serialBAUD_RATE						( ( portUInt32Type ) 38400 )

#define serialSCI_1_RD_REGISTER				( *( volatile portUInt32Type * ) 0xFFF7E434 )
#define serialSCI_DATA_MASK					( ( portUInt32Type ) 0x000000FF )

#define serialSCI_1_TD_REGISTER				( *( volatile portUInt32Type * ) 0xFFF7E438 )

#define serialSCI_1_PIO_0_REGISTER			( *( volatile portUInt32Type * ) 0xFFF7E43C )
#define serialSCI_PIO_0_TX_FUNC				( ( portUInt32Type ) 0x00000004 )
#define serialSCI_PIO_0_RX_FUNC				( ( portUInt32Type ) 0x00000002 )


#define serialNO_BLOCK						( ( portTickType ) 0 )

/* Define the buffers to be used by the queues.  These can hold 30 bytes. */
#define serialQUEUE_LENGTH						( 30 )
#define serialRX_QUEUE_BUFFER_LEN				( serialQUEUE_LENGTH + portQUEUE_OVERHEAD_BYTES )
#define serialTX_QUEUE_BUFFER_LEN				( serialQUEUE_LENGTH + portQUEUE_OVERHEAD_BYTES )

static portInt8Type acRxQueueBuffer[ serialRX_QUEUE_BUFFER_LEN ] __attribute__( ( aligned ( 8 ) ) ) = { 0 };
static portInt8Type acTxQueueBuffer[ serialTX_QUEUE_BUFFER_LEN ] __attribute__( ( aligned ( 8 ) ) ) = { 0 };

/* Queues used to hold received characters, and characters waiting to be
 * transmitted. */
/* Force all serial test variables into a defined section */
portspecCOM_TEST_ZERO_DATA_SECTION static xQueueHandle xRxedChars = NULL;
portspecCOM_TEST_ZERO_DATA_SECTION static xQueueHandle xCharsForTx = NULL;
portspecCOM_TEST_ZERO_DATA_SECTION static volatile portBaseType xCharsWaitingForTx = pdFALSE;

/******************************************************************************
 * Local prototypes
 */

__irq __arm void vSerialTestIntrHandler( void );
/*-----------------------------------------------------------------------------
 * Public functions
 *---------------------------------------------------------------------------*/

void vSerialPortInit( void )
{
portBaseType xRxQueueStatus, xTxQueueStatus;
portUInt32Type ulPrescaler;

	/* Create the queues used to hold Rx and Tx characters. */
	xRxQueueStatus = xQueueCreate( acRxQueueBuffer, serialRX_QUEUE_BUFFER_LEN, serialQUEUE_LENGTH, ( portUnsignedBaseType ) sizeof( portCharType ), &xRxedChars );
	xTxQueueStatus = xQueueCreate( acTxQueueBuffer, serialTX_QUEUE_BUFFER_LEN, serialQUEUE_LENGTH, ( portUnsignedBaseType ) sizeof( portCharType ), &xCharsForTx );

	if( ( pdPASS == xRxQueueStatus ) && ( pdPASS == xTxQueueStatus ) )
	{
		taskENTER_CRITICAL();
		{
			/* Enable SCI 1 by setting RESET bit. */
			serialSCI_1_GCR0_REGISTER |= serialSCI_GCR0_RESET_BIT;

			/* Clear SWnRST to 0 before configuring the SCI. */
			serialSCI_1_GCR1_REGISTER &= ~serialSCI_GCR1_SWNRST_BIT;

			/* Select the desired frame format by programming SCIGCR1. */
			serialSCI_1_GCR1_REGISTER = ( serialSCI_GCR1_NO_PARITY | serialSCI_GCR1_1_STOP_BIT );
			serialSCI_1_FORMAT_REGISTER = serialSCI_FORMAT_8_BIT_CHARS;

			/* Configure the LINRX and LINTX pins for SCI functionality by
			 * setting the RX FUNC and TX FUNC bit. */
			serialSCI_1_PIO_0_REGISTER = ( serialSCI_PIO_0_RX_FUNC | serialSCI_PIO_0_TX_FUNC );

			/* Select the baud rate to be used for communication by programming
			 * BRSR. */
			ulPrescaler = ( configCPU_CLOCK_HZ / 2UL );
			ulPrescaler /= ( serialBAUD_RATE * serialCLOCK_SCALING );
			ulPrescaler -= 1UL;
			serialSCI_1_BRS_REGISTER = ulPrescaler;

			/* Select internal clock by programming the CLOCK bit. */
			serialSCI_1_GCR1_REGISTER |= serialSCI_GCR1_CLOCK_BIT;

			/* Set the CONT bit to make SCI not to halt for an emulation
			 * breakpoint until its current reception or transmission is
			 * complete. (This bit is used only in an emulation
			 * environment). */
			serialSCI_1_GCR1_REGISTER |= serialSCI_GCR1_CONT_BIT;

			/* Set LOOP BACK bit to connect the transmitter to the receiver
			 * internally. (This feature is used to perform a self-test.) */
			serialSCI_1_GCR1_REGISTER |= serialSCI_GCR1_LOOPBACK_BIT;

			/* Select the receiver enable bit and transmit enable bit. */
			serialSCI_1_GCR1_REGISTER |= ( serialSCI_GCR1_RXENA_BIT );

			/* Enable the RX and Tx interrupts. */
			serialSCI_1_SET_INT_REGISTER = ( serialSET_RX_INT_BIT | serialSET_TX_INT_BIT );

			/* Set SWnRST to 1 after the SCI is configured. */
			serialSCI_1_GCR1_REGISTER |= serialSCI_GCR1_SWNRST_BIT;
		}
		taskEXIT_CRITICAL();
	}
}
/*---------------------------------------------------------------------------*/

portBaseType xSerialGetChar( portCharType *pcRxedChar, portTickType xBlockTime )
{
portBaseType xResult;

	/* Get the next character from the buffer. Return false if no characters
	 * are available, or arrive before xBlockTime expires. */
	if( xQueueReceive( xRxedChars, pcRxedChar, xBlockTime ) == pdPASS )
	{
		xResult = pdTRUE;
	}

	return xResult;
}
/*---------------------------------------------------------------------------*/

portBaseType xSerialPutChar( portCharType cOutChar, portTickType xBlockTime )
{
portBaseType xReturn = pdPASS;

	/* Place the character in the queue of characters to be transmitted. */
	taskENTER_CRITICAL();
	{
		/* Is there space to write directly to the UART? */
		if( pdFALSE == xCharsWaitingForTx )
		{
			/* There is no characters waiting to be sent, so write the
			 * character directly to the UART. */
			xCharsWaitingForTx = pdTRUE;

			/* Select the transmit enable bit before sending. */
			serialSCI_1_GCR1_REGISTER |= serialSCI_GCR1_TXENA_BIT;
			serialSCI_1_TD_REGISTER = ( portUInt32Type )cOutChar;
		}
		else
		{
			/* We cannot write directly to the UART, so queue the character.
			 * Block for a maximum of xBlockTime if there is no space in the
			 * queue. It is ok to block within a critical section as each task
			 * has its own critical section management. */
			if( xQueueSend( xCharsForTx, &cOutChar, xBlockTime ) != pdPASS )
			{
				xReturn = pdFAIL;
			}
			else
			{
				/* Depending on queue sizing and task prioritisation: While we
				 * were blocked waiting to post interrupts were not disabled.
				 * It is possible that the serial ISR has emptied the Tx queue,
				 * in which case we need to start the Tx off again. */
				if( pdFALSE == xCharsWaitingForTx )
				{
					if( xQueueReceive( xCharsForTx, &cOutChar, serialNO_BLOCK ) == pdPASS )
					{
						xCharsWaitingForTx = pdTRUE;

						/* Select the transmit enable bit before sending. */
						serialSCI_1_GCR1_REGISTER |= serialSCI_GCR1_TXENA_BIT;
						serialSCI_1_TD_REGISTER = ( portUInt32Type )cOutChar;
					}
				}
			}
		}
	}
	taskEXIT_CRITICAL();

	return xReturn;
}
/*---------------------------------------------------------------------------*/

__irq __arm void vSerialTestIntrHandler( void )
{
portCharType cChar;
portBaseType xHigherPriorityTaskWoken = pdFALSE;

	/* What caused the interrupt? */
	switch( serialSCI_1_INT_VECT_0_REGISTER )
	{
	case serialSOURCE_RECEIVE:
		/* A character was received.
		 * Place it in the queue of received characters. */
		cChar = ( portCharType )( serialSCI_1_RD_REGISTER & serialSCI_DATA_MASK );
		xQueueSendFromISR( xRxedChars, &cChar, &xHigherPriorityTaskWoken );

		/* The previous transmit has completed.
		 * If there is another character in the Tx queue, send it now. */
		if( xQueueReceiveFromISR( xCharsForTx, &cChar, &xHigherPriorityTaskWoken ) == pdTRUE )
		{
			serialSCI_1_TD_REGISTER = ( portUInt32Type )cChar;
		}
		else
		{
			/* There are no further characters queued to send. */
			xCharsWaitingForTx = pdFALSE;

			/* Disable the transmitter until the next char is sent. */
			serialSCI_1_GCR1_REGISTER &= ~serialSCI_GCR1_TXENA_BIT;
		}
		break;

	default:
		/* There is nothing to do, leave the ISR. */
		break;
	}

	/* Exit the ISR. If a task was woken by either a character being received
	 * or transmitted then a context switch will occur. */
	taskYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
/*---------------------------------------------------------------------------*/
