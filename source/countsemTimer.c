/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

/*-----------------------------------------------------------------------------
 * Counting Semaphore Timer Initialisation and Configuration Function.
 *---------------------------------------------------------------------------*/

/* Scheduler include files. */
#include "SafeRTOS_API.h"

/* Hardware include files. */

/* Demo Program include files. */
#include "countsem.h"
#include "countsemTimer.h"
#include "PortSpecifics.h"
#include "partest.h"


/* RTI Peripheral Register Address Definitions */
#define RTIGCTRL			( *( ( volatile portUInt32Type * ) 0xFFFFFC00UL ) )
#define RTITBCTRL 			( *( ( volatile portUInt32Type * ) 0xFFFFFC04UL ) )
#define RTICOMPCTRL 		( *( ( volatile portUInt32Type * ) 0xFFFFFC0CUL ) )

#define RTIFRC1 			( *( ( volatile portUInt32Type * ) 0xFFFFFC30UL ) )
#define RTIUC1				( *( ( volatile portUInt32Type * ) 0xFFFFFC34UL ) )
#define RTICPUC1 			( *( ( volatile portUInt32Type * ) 0xFFFFFC38UL ) )

#define RTICOMP1 			( *( ( volatile portUInt32Type * ) 0xFFFFFC58UL ) )
#define RTIUDCP1 			( *( ( volatile portUInt32Type * ) 0xFFFFFC5CUL ) )

#define RTIINTSET			( *( ( volatile portUInt32Type * ) 0xFFFFFC80UL ) )
#define RTIINTCLR			( *( ( volatile portUInt32Type * ) 0xFFFFFC84UL ) )
#define RTIINTFLAG			( *( ( volatile portUInt32Type * ) 0xFFFFFC88UL ) )

#define RTIVIMMASKSET0		( *( ( volatile portUInt32Type * ) 0xFFFFFE30UL ) )

#define INTREQ_VIM_REQ		( 0x00000002 )

/* Semaphore ISR Test Task Operational State & Counter */
#define cntsemtimPERIPH_CLOCK_HZ	( configCPU_CLOCK_HZ / 2U )


/*-----------------------------------------------------------------------------
 * Counting Semaphore ISR Timer Initialisation
 */
void vCountSemTimerInit( void )
{
portUInt32Type ulInitialValue;

	/* Disable Counter1 */
	ulInitialValue = RTIGCTRL;
	ulInitialValue &= ~( 0x02UL );
	RTIGCTRL = ulInitialValue;

	/* Use Internal CLock */
	RTITBCTRL = 0x00000000UL;

	/* COMPSEL1 Uses RTIFRC1 Counter */
	ulInitialValue = RTICOMPCTRL;
	ulInitialValue |= 0x00000010UL;
	RTICOMPCTRL = ulInitialValue;

	/* Initialise Counter and Prescale Counter Registers */
	RTIUC1 = 0x00000000UL;
	RTIFRC1 = 0x00000000UL;

	/* Set Prescalar Counter for RTI Clock */
 	ulInitialValue = cntsemtimPERIPH_CLOCK_HZ  / countsemCOUNTSEM_ISR_TIMER_FREQUENCY;

	RTICPUC1 = 0x00000001U;
	RTICOMP1 = ulInitialValue;
	RTIUDCP1 = ulInitialValue;

	/* Enable Counter1 Interrupt Channel in VIM */
	ulInitialValue = RTIVIMMASKSET0;
	ulInitialValue |= 0x00000008UL;
	RTIVIMMASKSET0 = ulInitialValue;

	/* Enable Counter1 Compare Interrupt INT1 */
	ulInitialValue = RTIINTSET;
	ulInitialValue |= 0x00000002UL;
	RTIINTSET = ulInitialValue;

	/* Enable Counter1 */
	ulInitialValue = RTIGCTRL;
	ulInitialValue |= 0x00000002UL;
	RTIGCTRL = ulInitialValue;
}
/*---------------------------------------------------------------------------*/

__irq __arm void vSafeDemoRtiTickHandler( void )
{
portBaseType xHigherPriorityTaskWoken = pdFALSE;

	/* Write '1' to clear the interrupt. */
	RTIINTFLAG = INTREQ_VIM_REQ;

	/* Call the counting semaphore demo timer ISR. */
	xHigherPriorityTaskWoken = xCountingSemaphoreTimerHandler();

	/* Context switch if needed. */
	taskYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
/*---------------------------------------------------------------------------*/
