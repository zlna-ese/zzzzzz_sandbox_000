/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

/* Generic headers */
#include <stdlib.h>

/* Hardware Includes */
#include "HL_reg_rti.h"
#include "HL_reg_vim.h"

/* SafeRTOS includes */
#include "SafeRTOS_API.h"

/* Program Includes */
#include "FullDemo.h"		/* Required for Hook Function Prototypes */


/*-----------------------------------------------------------------------------
 * Local Constants
 *---------------------------------------------------------------------------*/

/* Scheduler Initialisation Definitions */

/* SafeRTOS inspects the vector table to ensure
 * the necessary handlers have been installed. */
#define configSTACK_CHECK_MARGIN			( 0UL )

/* The user configuration for the idle task. */
#define	configIDLE_TASK_STACK_SIZE			( configMINIMAL_STACK_SIZE_NO_FPU )

/* The user configuration for the timer module. */
#define	configTIMER_TASK_STACK_SIZE			( 512UL )
#define configTIMER_TASK_PRIORITY			( fulldemoTIMER_TASK_PRIORITY )
#define configTIMER_CMD_QUEUE_LEN			( fulldemoTIMER_CMD_QUEUE_LEN )
#define configTIMER_CMD_QUEUE_BUFFER_SIZE 	( ( configTIMER_CMD_QUEUE_LEN * sizeof( timerQueueMessageType ) ) + portQUEUE_OVERHEAD_BYTES )

/* The size of the Idle task data section. */
#define configIDLE_TASK_DATA_SIZE			( 0x20 )


/*-----------------------------------------------------------------------------
 * Local Prototypes
 *---------------------------------------------------------------------------*/

/* The call to xTaskInitializeScheduler is included within a wrapper
 * initialisation function. */
portBaseType xInitializeScheduler( void );


/*-----------------------------------------------------------------------------
 * Local Variables
 */

#pragma data_alignment=configIDLE_TASK_STACK_SIZE
static portInt8Type acIdleTaskStack[ configIDLE_TASK_STACK_SIZE ] = { 0 };

/* Declare the stack for the timer task, it cannot be done in the timer
 * module as the syntax for alignment is port specific. Also the callback
 * functions are executed in the timer task and their complexity/stack
 * requirements are application specific. */
#pragma data_alignment=configTIMER_TASK_STACK_SIZE
static portInt8Type acTimerTaskStack[ configTIMER_TASK_STACK_SIZE ] = { 0 };

/* The buffer for the timer command queue. */
#pragma data_alignment=portWORD_ALIGNMENT
static portInt8Type acTimerCommandQueueBuffer[ configTIMER_CMD_QUEUE_BUFFER_SIZE ] = { 0 };

/* A linker defined symbol that gives the start address of the Idle task
 * data section. */
extern portUInt32Type __idle_task_data_block__$$Base[];


/*-----------------------------------------------------------------------------
 * Routines
 *---------------------------------------------------------------------------*/

portBaseType xInitializeScheduler( void )
{
portBaseType xInitSchedResult;
/* The structure passed to xTaskInitializeScheduler() to configure the kernel
 * with the application defined constants and call back functions. */
xPORT_INIT_PARAMETERS xPortInit =
{
	configCPU_CLOCK_HZ, 				/* ulCPUClockHz */
	configTICK_RATE_HZ,					/* ulTickRateHz */

	/* Hook Functions */
	NULL,                               /* pxSetupTickInterruptHookFunction */
	&vDemoApplicationTaskDeleteHook,	/* pxTaskDeleteHookFunction */
	&vDemoApplicationErrorHook,			/* pxErrorHookFunction */
	&vDemoApplicationIdleHook,			/* pxIdleHookFunction */
	&vDemoApplicationTickHook,			/* pxTickHookFunction */
	&uxDemoApplicationSvcHook,			/* pxSvcHookFunction */
	&vDemoApplicationSSIHook,			/* pxSSIHookFunction */

	/* System Stack parameters */
	configSTACK_CHECK_MARGIN,			/* uxAdditionalStackCheckMarginBytes */

	/* Idle Task parameters */
	acIdleTaskStack,					/* pcIdleTaskStackBuffer */
	configIDLE_TASK_STACK_SIZE,			/* uxIdleTaskStackSizeBytes */
	pdFALSE,							/* xIdleTaskUsingFPU */

	/* MPU Operating Parameters */
	{									/* xIdleTaskMPUParameters */
		mpuUNPRIVILEGED_TASK,			/* The idle hook will be executed in unprivileged mode. */
		{
			{
				__idle_task_data_block__$$Base,
				configIDLE_TASK_DATA_SIZE,
				( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE |
				  portmpuREGION_EXECUTE_NEVER |
				  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY ),
				0UL
			},
			{ NULL, 0UL, 0UL, 0UL },
			{ NULL, 0UL, 0UL, 0UL },
			{ NULL, 0UL, 0UL, 0UL },
			{ NULL, 0UL, 0UL, 0UL },
			{ NULL, 0UL, 0UL, 0UL }
		}
	},

	NULL, 								/* pvIdleTaskTLSObject */

	/* Timer feature initialisation parameters */
	configTIMER_TASK_PRIORITY,			/* uxTimerTaskPriority */
	configTIMER_TASK_STACK_SIZE,		/* uxTimerTaskStackSize */
	acTimerTaskStack,					/* pcTimerTaskStackBuffer */
	configTIMER_CMD_QUEUE_LEN, 			/* uxTimerCommandQueueLength */
	configTIMER_CMD_QUEUE_BUFFER_SIZE,	/* uxTimerCommandQueueBufferSize */
	acTimerCommandQueueBuffer,			/* pcTimerCommandQueueBuffer */
	pdTRUE								/* xEnableCache */
};

	/* Initialise the kernel by passing in a pointer to the xPortInit structure
	 * and return the resulting error code. */
	xInitSchedResult = xTaskInitializeScheduler( &xPortInit );

	return xInitSchedResult;
}
/*--------------------------------------------------------------------------*/

/* The code below is just an example on how to configure a different RTI
 * module to implement the RTOS system tick.
 */
#if 0
static void prvDemoSetupTickInterrupt( portUInt32Type ulClockHz, portUInt32Type ulRateHz )
{
portUInt32Type ulCompareValue;

	/* Calculate compare value. */
	ulCompareValue = ( ( ulClockHz / 2UL ) / ulRateHz );

	/* Disable the Counter Block. */
	rtiREG1->GCTRL &= ~( 0x01UL << configRTI_BLOCK_NUM );

	/* Use the internal counter. */
	rtiREG1->TBCTRL = 0x00UL;

	/* Initialise the counter and the prescale counter registers. */
	rtiREG1->CNT[ configRTI_BLOCK_NUM ].UCx = 0UL;
	rtiREG1->CNT[ configRTI_BLOCK_NUM ].FRCx = 0UL;
	rtiREG1->CNT[ configRTI_BLOCK_NUM ].CPUCx = 1UL;

	/* Set compare value for RTI clock. */
	rtiREG1->CMP[ configRTI_COMPARE_NUM ].COMPx = ulCompareValue;
	rtiREG1->CMP[ configRTI_COMPARE_NUM ].UDCPx = ulCompareValue;

#if ( configRTI_BLOCK_NUM == 0 )
	/* Program COMPSELx to use the RTIFRC0 counter. */
	rtiREG1->COMPCTRL &= ~( 01UL << ( configRTI_COMPARE_NUM * 4U ) );
#else
	/* Program COMPSELx to use the RTIFRC1 counter. */
	rtiREG1->COMPCTRL |= ( 01UL << ( configRTI_COMPARE_NUM * 4U ) );
#endif

	/* Enable Timer0 compare interrupt INT0. */
	rtiREG1->SETINTENA = ( 0x01UL << configRTI_COMPARE_NUM );

	/* Enable the interrupt channel in the VIM. */
	vimREG->REQMASKSET0 = ( 0x01UL << ( configRTI_COMPARE_NUM + 2U ) );

	/* Enable the Counter Block. */
	rtiREG1->GCTRL |= ( 0x01UL << configRTI_BLOCK_NUM );
}
/*---------------------------------------------------------------------------*/

__irq __arm static void prvDemoTickISR( void )
{
	/* Write '1' to clear the interrupt. */
	rtiREG1->INTFLAG = ( 0x01U << configRTI_COMPARE_NUM );

	/* Call the kernel hook */
	vTaskProcessSystemTickFromISR();
}
#endif
