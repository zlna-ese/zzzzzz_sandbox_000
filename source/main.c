/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

/****************************************************************************
 * This is a 'quick start' project to demonstrate use of SafeRTOS running on
 * an TMS570LC43x platform with FPU and MPU enabled.
 * It can be used to quickly evaluate SafeRTOS and as a reference for the
 * project settings and syntax necessary to utilise SafeRTOS.
 *
 * The following tasks and demonstrations are created:
 *
 * - A set of standard demo tasks
 * These are tasks that are common to nearly all SafeRTOS demo projects.
 * Their only purpose is to provide examples of how the SafeRTOS API can be
 * used and provide no particular functionality of interest.  The Demo tasks
 * exist to demonstrate queues, semaphores, task management and interrupt
 * interactions.
 *
 * - A 'check' task
 * The check task only executes every 5 seconds, but has the highest priority
 * so is guaranteed to get processing time.  It queries all the standard demo
 * tasks to see if they are all executing as expected.
 *
 * Particular points to note within this file include, how task stacks are
 * defined and aligned, how queues are dimensioned and aligned, and how the
 * kernel is initialised.
 *
 ****************************************************************************/

/* SafeRTOS Includes */
#include "SafeRTOS_API.h"

/* Program Includes */
#include "partest.h"
#include "FullDemo.h"
#include "aborts.h"


/*-- Local CONSTANT Definitions -------------------------------------------*/

/*-- Local ENUM-TYPE Definitions ------------------------------------------*/

/*-- Local VARIABLE Declarations ------------------------------------------*/


/*-- Public FUNCTION Declarations -----------------------------------------*/

extern portBaseType xInitializeScheduler( void );

/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/

void prvSetupHardware( void )
{
	/* Initialise GPIO ports - enable board LEDs */
	vParTestInitialise( );
}
/*-------------------------------------------------------------------------*/

void vSafeRTOSDemoApplication( void )
{
portBaseType xStatus;

	/* Setup Board Hardware. */
	prvSetupHardware( );

	/* Initialise the Kernel Scheduler. */
	xStatus = xInitializeScheduler();

    /* Everything OK? */
	if( pdPASS == xStatus )
	{
	    /* Yes, try to create the demonstration tasks. */
		xStatus = xFullDemoCreate();
	}

	/* Everything OK? */
	if( pdPASS == xStatus )
	{
		/* Yes, try to start the Scheduler. */
		xStatus = xTaskStartScheduler( pdTRUE );
	}

	/* The Scheduler should now be running the Demonstration tasks.
	 * So this line should never be reached, if it is,
	 * then the Scheduler initialisation has failed! */
	vStartupAbort();
	for( ;; );
}
/*-------------------------------------------------------------------------*/

