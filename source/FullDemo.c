/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

//----------------------------------------------------------------------------//
// MODIFIED FULLDEMO.C 
//----------------------------------------------------------------------------//
//Added SafeRTOS Blinky Task for TI TMS570LC43xHDK
//NOTE: Disabled DEMO tasks. The SafeRTOS DEMO LED's conflict with blinky LED's  
//----------------------------------------------------------------------------//
//Thomas Latrofa
//Zoomlion Heavy Industries North America
//11-25-2019
//----------------------------------------------------------------------------//

/* Generic headers */
#include <stdlib.h>

/* SafeRTOS includes */
#include "SafeRTOS_API.h"

/* Demo application include files. These files are common to all demo applications. */
#include "death.h"
#include "flash.h"
#include "PollQ.h"
#include "BlockQ.h"
#include "dynamic.h"
#include "semtest.h"
#include "blocktim.h"
#include "countsem.h"
#include "TimerDemo.h"
#include "TaskNotify.h"
#include "MathsTest.h"
#include "comtest.h"
#include "TaskMutex.h"
//----------------------------BLINKY INCLUDE----------------------------------//
//----------------------------------------------------------------------------//
#include "blinky.h"
#include "blinkyq.h"
//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//
/* Demo application include files specific to this demo. */
#include "PortSpecifics.h"
#include "ParTest.h"
#include "FullDemo.h"
#include "aborts.h"


/*-----------------------------------------------------------------------------
 * Local constant definitions
 *---------------------------------------------------------------------------*/

/* Task priorities.
 * The lowest priority is always Zero ( 0 ), and the highest priority
 * is (configMAX_PRIORITIES - 1).
 * Any number of tasks can share the same priority.
 */
#define fulldemoCHECK_TASK_PRIORITY				( 4U )
#define fulldemoBLOCKING_QUEUE_TASKS_PRIORITY	( 1U )
#define fulldemoSUICIDAL_TASKS_PRIORITY			( 1U )
#define fulldemoFLASH_LED_TASKS_PRIORITY		( 1U )
#define fulldemoPOLLED_QUEUE_TASKS_PRIORITY		( 0U )
#define fulldemoSEMAPHORE_TASKS_PRIORITY		( 1U )
#define fulldemoCOM_TEST_TASK_PRIORITY			( 3U )
//----------------------------------------------------------------------------//
#define fulldemoBLINKY_QUEUE_TASKS_PRIORITY	( 1U )
/*-------------------------------------------------------------------------*/
/* Demonstration Task Executable Configuration */

/* If many demo tasks are disabled, set this to zero to prevent the check
 * task from displaying an error due to low CPU time. */
#define fulldemoCHECK_IDLE_TIMES			( 1 )

/* This block of demo tasks should execute on any SafeRTOS implementation
 * provided sufficient run time and RAM is available. */
/* Set to '1' to Enable or '0' to Disable */
#define fulldemoINCLUDE_FLASH_DEMO			( 0 )
#define fulldemoINCLUDE_POLL_Q_DEMO		        ( 0 )
#define fulldemoINCLUDE_BLOCK_Q_DEMO			( 0 )
#define fulldemoINCLUDE_DYNAMIC_DEMO			( 0 )
#define fulldemoINCLUDE_SEMA_COUNT_DEMO			( 0 )
#define fulldemoINCLUDE_SEMA_BINARY_DEMO		( 0 )
#define fulldemoINCLUDE_TIMER_DEMO		        ( 0 )
#define fulldemoINCLUDE_SUICIDE_DEMO			( 0 )
#define fulldemoINCLUDE_TASK_NOTIFY_DEMO		( 0 )
#define fulldemoINCLUDE_SVC_TEST_DEMO			( 0 )
#define fulldemoINCLUDE_SSI_TEST_DEMO			( 0 )
#define fulldemoINCLUDE_REC_MUTEX_DEMO			( 0 )

//----------------------------BLINKY INCLUDE----------------------------------//
//----------------------------------------------------------------------------//
#define fulldemoINCLUDE_BLINKY                          ( 0 )
#define fulldemoINCLUDE_BLINKY_Q                        ( 1 )
//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//

/* The block time demo is very processor intensive and can be disabled in
 * demo applications that use processors with lower clock speeds. */
#define fulldemoINCLUDE_BLOCK_TIME_DEMO			( 0 )

/* The Maths task demo is intended for processors that implement a floating
 * point unit (FPU). */
#define fulldemoINCLUDE_MATHS_DEMO			( 0 )

/* The com test demo requires a serial port driver that implements either an
 * external or internal loopback. */
#define fulldemoINCLUDE_COM_TEST_DEMO			( 0 )

/* Base periods of the timers used in the timer and task notify demo. */
#define fulldemoTIMER_TEST_PERIOD			( 50U )
#define fulldemoTASK_NOTIFY_PERIOD			( 50U )

/* Check task operational definitions. */
#define fulldemoCHECK_TASK_STACK_SIZE			( 512U )

#define fulldemoCHECK_TASK_NORMAL_CYCLE_RATE	( ( portTickType ) 5000U / configTICK_RATE_MS )
#define fulldemoCHECK_TASK_ERROR_CYCLE_RATE	( ( portTickType ) 1000U / configTICK_RATE_MS )

/* Check task error identifiers.
 * Used to keep track of errors that may have been detected. */
#define fulldemoCHECK_TASK_ERROR_MATHS_TASKS			( 0x00000001UL )
#define fulldemoCHECK_TASK_ERROR_BLOCKING_QUEUES		( 0x00000002UL )
#define fulldemoCHECK_TASK_ERROR_BLOCK_TIME_TEST		( 0x00000004UL )
#define fulldemoCHECK_TASK_ERROR_COM_TEST			( 0x00000008UL )
#define fulldemoCHECK_TASK_ERROR_COUNTING_SEMAPHORES	        ( 0x00000010UL )
#define fulldemoCHECK_TASK_ERROR_CREATE_TASKS			( 0x00000020UL )
#define fulldemoCHECK_TASK_ERROR_DYNAMIC_PRIORITIES		( 0x00000040UL )
#define fulldemoCHECK_TASK_ERROR_POLLING_QUEUES			( 0x00000080UL )
#define fulldemoCHECK_TASK_ERROR_BINARY_SEMAPHORES		( 0x00000100UL )
#define fulldemoCHECK_TASK_ERROR_TIMER_TASKS			( 0x00000200UL )
#define fulldemoCHECK_TASK_ERROR_TASK_NOTIFY			( 0x00000400UL )
#define fulldemoCHECK_TASK_ERROR_MUTEX				( 0x00000800UL )
#define fulldemoCHECK_TASK_ERROR_TICK_HOOK			( 0x00001000UL )
#define fulldemoCHECK_TASK_ERROR_IDLE_TASK			( 0x00002000UL )
#define fulldemoCHECK_TASK_ERROR_SVC_COUNT			( 0x00004000UL )
#define fulldemoCHECK_TASK_ERROR_SVC_RETURN                     ( 0x00010000UL )
#define fulldemoCHECK_TASK_ERROR_SSI_COUNT			( 0x00020000UL )

#define fulldemoCHECK_TASK_ERROR_INSUFFICIENT_IDLE		( 0x00040000UL )
#define fulldemoCHECK_TASK_ERROR_EXCESSIVE_IDLE			( 0x00080000UL )
#define fulldemoCHECK_TICK_HOOK_COUNT				( 0x00100000UL )

/* CPU usage limits for the idle task. */
#define fulldemoIDLE_TASK_MINIMUM_CPU_PERCENTAGE	( 1U )		/* 0.01% */
#define fulldemoIDLE_TASK_MAXIMUM_CPU_PERCENTAGE	( 200UL )	/* 2.00% */

#define fulldemoCHECK_TASK_PID_CHECK                    ( 0x00200000UL )

#define fulldemoSSIMASK						( 0x07UL )
#define fulldemoSSI1						( 0x01UL )
#define fulldemoSSI2						( 0x02UL )
#define fulldemoSSI3						( 0x03UL )
#define fulldemoSSI4						( 0x04UL )

#define fulldemoSYS_SSIR1_REG		( * ( ( volatile portUInt32Type * ) 0xFFFFFFB0UL ) )
#define fulldemoSYS_SSIR1_SSKEY		( 0x75014UL )
#define fulldemoSYS_SSIR2_REG		( * ( ( volatile portUInt32Type * ) 0xFFFFFFB4UL ) )
#define fulldemoSYS_SSIR2_SSKEY		( 0x8402UL )
#define fulldemoSYS_SSIR3_REG		( * ( ( volatile portUInt32Type * ) 0xFFFFFFB8UL ) )
#define fulldemoSYS_SSIR3_SSKEY		( 0x9304UL )
#define fulldemoSYS_SSIR4_REG		( * ( ( volatile portUInt32Type * ) 0xFFFFFFBCUL ) )
#define fulldemoSYS_SSIR4_SSKEY		( 0xA208UL )

/* LEDs */
#define fulldemoLED_CHECK_TASK		( partestLED_D3 )
#define fulldemoLED_COMTEST_TASK	( partestLED_LED1 )

/* Prefix macro for the Idle Hook data objects. */
#define portspecIDLE_HOOK_DATA_SECTION	__attribute__ ( ( section( "__idle_hook_data__" ) ) )

/* Stringify */
#define STRGFY(x) #x
#define STRINGIFY(x)  STRGFY(x)

/* How to access the SVC number used to invoke the SVC hook function. */
#define fulldemoGET_SVC_NUMBER()        portGET_PRIV_SVC_NUMBER_REGISTER()


/*-----------------------------------------------------------------------------
 * Local Prototypes
 *---------------------------------------------------------------------------*/

static void prvCheckTask( void *pvParameters );

#if ( fulldemoINCLUDE_SSI_TEST_DEMO == 1 )
static void prvDemoApplicationTriggerSSI( void );
#endif


/*-----------------------------------------------------------------------------
 * Local Variables
 *---------------------------------------------------------------------------*/

/* Accumulators and counter to test the SVC hook. */
static volatile portUnsignedBaseType uxSvcCounter = 0U;
static volatile portUnsignedBaseType uxSvcAccum = 0U;
static volatile portUInt64Type uxSvc64bitAccum = 0x1234567812345678ULL;

/* Counters to test the SSI hook. */
static volatile portUnsignedBaseType uxSSICounter1 = 0U;
static volatile portUnsignedBaseType uxSSICounter2 = 0U;
static volatile portUnsignedBaseType uxSSICounter3 = 0U;
static volatile portUnsignedBaseType uxSSICounter4 = 0U;

/* Incremented in the idle task hook so the check task
 * can ensure the idle task hook function is being called as expected. */
_Pragma("location=\"__idle_task_zero_data__\"")
static volatile portUInt32Type ulIdleHookCallCount = 0UL;

/* Incremented within the tick hook so the check task
 * can ensure the tick hook is being called. */
static volatile portUInt32Type ulTickHookCallCount = 0UL;

/* The check task needs to know the Idle task's handle,
 * so it can query its runtime statistics. */
_Pragma("location=\"__idle_task_zero_data__\"")
static portTaskHandleType xIdleTaskHandle = NULL;

_Pragma("location=\"__idle_task_data__\"")
static portBaseType xIdleTaskFirstExecution	= pdTRUE;

/* Declare task TCB:
 * Due to the use of the MPU background region, by default, all RAM can only
 * be accessed in privileged mode unless a specific MPU region has been setup
 * allowing unprivileged access. */
static xTCB xCheckTaskTCB = { 0 };

/* Declare task stacks :
 * These ARE protected by MPU regions so the alignment must follow the
 * MPU alignment rules, and basically be aligned to the same power of two
 * value as their length in bytes. */
#pragma data_alignment=fulldemoCHECK_TASK_STACK_SIZE
static portInt8Type acCheckTaskStack[ fulldemoCHECK_TASK_STACK_SIZE ] = { 0 };

/*-----------------------------------------------------------------------------
 * Public Function Definitions
 *---------------------------------------------------------------------------*/

portBaseType xFullDemoCreate( void )
{
portBaseType xCreateResult;

/* The structure passed to xTaskCreate() to create the check task. */
static const xTaskParameters xCheckTaskParams =
{
	&prvCheckTask,						/* The function that implements the task being created. */
	"Check",							/* The name of the task being created. The kernel does not use this itself, its just to assist debugging. */
	&xCheckTaskTCB,						/* The TCB for the check task. */
	acCheckTaskStack,					/* The buffer allocated for use as the task stack. */
	fulldemoCHECK_TASK_STACK_SIZE,		/* The size of the buffer allocated for use as the task stack - note this is in BYTES! */
	NULL,								/* The task parameter, not used in this case. */
	fulldemoCHECK_TASK_PRIORITY,		/* The priority to assigned to the task being created. */
	NULL,								/* Thread Local Storage not used. */
	pdFALSE,							/* Check task does NOT use the FPU. */
	{									/* MPU task parameters. */
		mpuPRIVILEGED_TASK,				/* Check task is privileged. */
		{
            { NULL, 0UL, 0UL, 0UL },	/* No additional region definitions are required. */
            { NULL, 0UL, 0UL, 0UL },
            { NULL, 0UL, 0UL, 0UL },
            { NULL, 0UL, 0UL, 0UL },
            { NULL, 0UL, 0UL, 0UL },
            { NULL, 0UL, 0UL, 0UL }
		}
	}
};

	/* Create the check task. */
	xCreateResult = xTaskCreate( &xCheckTaskParams,	/* The structure containing the task parameters created at the start of this function. */
								 NULL );			/* This parameter can be used to receive a handle to the created task, but is not used in this case. */
//----------------------------BLINKY INCLUDE----------------------------------//
//----------------------------------------------------------------------------//        
#if ( fulldemoINCLUDE_BLINKY == 1 )
	if( pdPASS == xCreateResult )
	{
		xCreateResult = xStartBlinky();
	}
#endif 
#if ( fulldemoINCLUDE_BLINKY_Q == 1 )
	if( pdPASS == xCreateResult )
	{
		xCreateResult = xStartBlinkyQueueTasks( fulldemoBLINKY_QUEUE_TASKS_PRIORITY );
	}
#endif        
//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//
/**************************************************************************
	 * The first block of standard demo tasks are dependent on the hardware
	 * capability of either the processor or target hardware and hence
	 * inclusion is decided by project level settings.
	 *************************************************************************/

#if ( fulldemoINCLUDE_COM_TEST_DEMO == 1 )
	if( pdPASS == xCreateResult )
	{
		xCreateResult = xAltStartComTestTasks( fulldemoCOM_TEST_TASK_PRIORITY,
												fulldemoLED_COMTEST_TASK );
	}
#endif

	/**************************************************************************
	 * The second block of standard demo tasks are not dependent on the
	 * hardware capability of either the processor or target hardware and
	 * therefore their inclusion is a matter of preference and controlled
	 * by settings in this file.
	 *************************************************************************/

#if ( fulldemoINCLUDE_FLASH_DEMO == 1 )
	if( pdPASS == xCreateResult )
	{
		xCreateResult = xStartLEDFlashTasks( fulldemoFLASH_LED_TASKS_PRIORITY );
	}
#endif

#if ( fulldemoINCLUDE_MATHS_DEMO == 1 )
	if( pdPASS == xCreateResult )
	{
		xCreateResult = xStartMathsTasks();
	}
#endif

#if ( fulldemoINCLUDE_BLOCK_Q_DEMO == 1 )
	if( pdPASS == xCreateResult )
	{
		xCreateResult = xStartBlockingQueueTasks( fulldemoBLOCKING_QUEUE_TASKS_PRIORITY );
	}
#endif

#if ( fulldemoINCLUDE_BLOCK_TIME_DEMO == 1 )
	if( pdPASS == xCreateResult )
	{
		xCreateResult = xCreateBlockTimeTasks();
	}
#endif

#if ( fulldemoINCLUDE_DYNAMIC_DEMO == 1 )
	if( pdPASS == xCreateResult )
	{
		xCreateResult = xStartDynamicPriorityTasks();
	}
#endif

#if ( fulldemoINCLUDE_POLL_Q_DEMO == 1 )
	if( pdPASS == xCreateResult )
	{
		xCreateResult = xStartPolledQueueTasks( fulldemoPOLLED_QUEUE_TASKS_PRIORITY );
	}
#endif

#if ( fulldemoINCLUDE_SEMA_BINARY_DEMO == 1 )
	if( pdPASS == xCreateResult )
	{
		xCreateResult = xStartSemaphoreTasks( fulldemoSEMAPHORE_TASKS_PRIORITY );
	}
#endif

#if ( fulldemoINCLUDE_SEMA_COUNT_DEMO == 1 )
	if( pdPASS == xCreateResult )
	{
		xCreateResult = xStartCountingSemaphoreTasks();
	}
#endif

#if ( fulldemoINCLUDE_TIMER_DEMO == 1 )
	if( pdPASS == xCreateResult )
	{
		xCreateResult = xStartTimerDemoTask( fulldemoTIMER_TEST_PERIOD,
											fulldemoTIMER_CMD_QUEUE_LEN,
											fulldemoTIMER_TASK_PRIORITY );
	}
#endif

#if ( fulldemoINCLUDE_TASK_NOTIFY_DEMO == 1 )
	if( pdPASS == xCreateResult )
	{
		xCreateResult = xStartTaskNotifyTask( fulldemoTASK_NOTIFY_PERIOD,
											fulldemoTIMER_TASK_PRIORITY );
	}
#endif

#if ( fulldemoINCLUDE_REC_MUTEX_DEMO == 1 )
	if( pdPASS == xCreateResult )
	{
		xCreateResult = xStartRecursiveMutexTasks();
	}
#endif

#if ( fulldemoINCLUDE_SUICIDE_DEMO == 1 )
	if( pdPASS == xCreateResult )
	{
		xCreateResult = xCreateSuicidalTasks( fulldemoSUICIDAL_TASKS_PRIORITY );
	}
#endif

	return xCreateResult;
}
/*---------------------------------------------------------------------------*/

void vDemoApplicationIdleHook( void )
{
	/* Is this the first time that the idle task hook has executed? */
	if( pdFALSE != xIdleTaskFirstExecution )
	{
		/* Yes, clear the flag. */
		xIdleTaskFirstExecution = pdFALSE;

		/* Now store a copy of our task handle so that the check task
		 * can examine the runtime statistics of the idle task. */
		xIdleTaskHandle = xTaskGetCurrentTaskHandle();
	}

	/* Increment a counter so the check task can see that the idle task is
	 * still running. */
	ulIdleHookCallCount++;
}
/*---------------------------------------------------------------------------*/

void vDemoApplicationTickHook( void )
{
portBaseType xHigherPriorityTaskWoken = pdFALSE;

	if( pdTRUE == xTaskIsSchedulerStartedFromISR() )
	{
#if ( fulldemoINCLUDE_TIMER_DEMO == 1 )
		/* Call the periodic timer test, which tests the timer API functions that
		 * can be called from an ISR. */
		if( xTimerPeriodicISRTests() != pdFALSE )
		{
			xHigherPriorityTaskWoken = pdTRUE;
		}
#endif

#if ( fulldemoINCLUDE_TASK_NOTIFY_DEMO == 1 )
		/* Call the periodic notify test, which tests the task notify API functions
		 * that can be called from an ISR. */
		if( xNotifyTaskFromISR() != pdFALSE )
		{
			xHigherPriorityTaskWoken = pdTRUE;
		}
#endif
	}

	/* Increment a counter so the check task can see that the tick hook is
	 * being called. */
	ulTickHookCallCount++;

	/* Context switch if needed. */
	taskYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
/*---------------------------------------------------------------------------*/

void vDemoApplicationTaskDeleteHook( portTaskHandleType xTaskBeingDeleted )
{
	/* The parameter is not used, this line prevents a compiler warning. */
	( void ) xTaskBeingDeleted;

	/* Increment a counter each time a task is deleted.
	 * The suicide tasks use this counter to ensure the expected number of
	 * tasks have been deleted. */
	uxTaskDeleteCallCount++;
}
/*---------------------------------------------------------------------------*/

void vDemoApplicationErrorHook( portTaskHandleType xHandleOfTaskWithError,
								const portCharType *pcErrorString,
								portBaseType xErrorCode )
{
	/* The parameters are not used, these lines prevent compiler warnings. */
	( void ) xHandleOfTaskWithError;
	( void ) pcErrorString;
	( void ) xErrorCode;

	/* Will only get here if an internal kernel error occurs. */
	vApplicationAbort();
}
/*---------------------------------------------------------------------------*/

/* SVC hook function
 *
 * This function is called with different prototypes, depending on the SWI_ALIAS
 * used to trigger the SVC.
 *
 * In this demo, there are three SWI_ALIAS pseudo-functions used to trigger an
 * SVC with three different SVC numbers, per the definitions near the top of
 * FullDemo.h. The prototypes of these three pseudo-functions are:
 *
 *   void vDemoApplicationSvcIncrValue( void );
 *   portUInt64Type uxDemoApplicationSvcAdd64Bit( portUInt64Type );
 *   portUInt32Type ulDemoApplicationSvcFourParams( portUInt32Type, portUInt32Type, portUInt32Type, portUInt32Type );
 *
 * The SVC hook function can be implemented either in C or in assembler, and examples
 * of both approaches are included. Select between them using fulldemoIMPLEMENT_SVC_HOOK_AS_ASM,
 * defined near the top of this file.
 *
 * If fulldemoIMPLEMENT_SVC_HOOK_AS_ASM is set to 1, the assembly language hook
 * function is used, otherwise the C language hook function is used.
 *
 * The port layer SVC handler preserves R2, R3, R4 and R5 across the SVC call, but it lets
 * the SVC hook function modify R0 and R1, in order to allow 64bit return values
 * from the pseudo-functions defined using SWI_ALIAS (SVC calls). It passes R0, R1, R2 and
 * R3 unchanged when calling the hook function, allowing arguments to be passed in to
 * the SVC hook using the SWI_ALIAS pseudo-function mechanism.
 *
 * The hook function prototype specifies a 64bit return value, said value being returned in
 * R0 and R1. Where the SWI_ALIAS functions have a smaller return value, or no return value,
 * we rely on the compiler's optimization honouring the volatility of R0 thru R3 per the ABI
 * to allow for the possibility that R0 and R1 may be changed by the hook function.
 */

/* Sub-handler functions for the SVC hook, prototyped to match the corresponding SWI_ALIAS pseudo-functions */
/* Pragmas are to prevent functions being eliminated if called only from assembler */
void vDemoApplicationSvcIncrValueFunc( void );
portUInt64Type uxDemoApplicationSvcAdd64BitFunc( portUInt64Type uxValue );
portUInt32Type ulDemoApplicationSvcFourParamsFunc( portUInt32Type ulArg1, portUInt32Type ulArg2, portUInt32Type ulArg3, portUInt32Type ulArg4  );

#if ( fulldemoIMPLEMENT_SVC_HOOK_IN_ASM == 0 )
#warning SVC hook function in C - ensure that SWI_ALIAS functions have no more than four parameters.
/*
* The ASM version of this function can be found in demoasm.s
*
* Alternative C language approach to SVC hook function.
*
* This function follows the generic prototype for the SVC hook function, returning
* a 64bit value and taking four 32 bit arguments. This allows most function prototypes
* to be accommodated by SVC calls via SWI_ALIAS pseudo-function calls.
*
* Here, its generic argument list and 64bit return value are coerced into the proper
* form for each alias prototype, which depends on knowing the endianness of the target
* and the way register pairs are used to represent 64bit values. The return has to be
* through this function if it's written in C, so it has to return a 64bit value, though
* this can be treated as 32bit or void by the invoking SWI_ALIAS pseudo-function.
*
* Obviously, "sub-handlers" needn't be written in separate functions, they could be
* implemented in the body of this hook function, but they're used here for simple
* comparison with the assembler hook function approach above.
*
* Because the compiler is free to use R4 in the function prologue etc., a C language
* implementation of the SVC hook can't depend on finding the SVC number in R4. However,
* the SVC handler also stores the invoking SVC number in one of the ARM core's CP15
* context registers, a register set aside for use as a privileged-only access PID register
* but unused by SafeRTOS (another PID register is used for the current task handle.)
*
* An access macro, portGET_PRIV_SVC_NUMBER_REGISTER(), retrieves and returns the SVC number,
* by using the __MRC compiler intrinsic.
*
* The SVC number isn't stored in the PID register if the SVC hook isn't called, i.e.
* it does not change when the SafeRTOS kernel SVC calls are taken.
*/

portUInt64Type uxDemoApplicationSvcHook( portUnsignedBaseType uxArg1, portUnsignedBaseType uxArg2, portUnsignedBaseType uxArg3, portUnsignedBaseType uxArg4 )
{
portUInt64Type uxRetVal = 0ull;
portUInt64Type uxArg64 = 0ull;
portUInt32Type ulTemp = 0UL;

	switch( fulldemoGET_SVC_NUMBER() )
	{
	case fulldemoSVC_INCREMENT_VALUE:
	    vDemoApplicationSvcIncrValueFunc();
	    break;

	case fulldemoSVC_ADD_64BIT:
	    /* Big endian, so MSB of 64bit arg is in arg1 (r0) and LSB in arg2 (r1). */
	    uxArg64 = ( ( portUInt64Type )uxArg1 << 32 ) + ( portUInt64Type )uxArg2;
	    uxRetVal = uxDemoApplicationSvcAdd64BitFunc( uxArg64 );
	    break;

	case fulldemoSVC_FOUR_PARAMS:
	    ulTemp = ulDemoApplicationSvcFourParamsFunc( uxArg1, uxArg2, uxArg3, uxArg4 );
	    /* Big endian, so shift result to top of the 64 bit return word, to get 32bit result in r0 */
	    uxRetVal = ( portUInt64Type ) ulTemp << 32;
	    break;

    default:
        break;
	}

	return uxRetVal;
}
/*---------------------------------------------------------------------------*/
#endif

/* Sub-handler called via SVC hook for the SWI_ALIAS vDemoApplicationSvcIncrValue()
 * pseudo-function. Void with no parameters. */
void vDemoApplicationSvcIncrValueFunc( void )
{
    uxSvcCounter++;
}
/*---------------------------------------------------------------------------*/

/* Sub-handler called via SVC hook for the SWI_ALIAS uxDemoApplicationSvcAdd64Bit()
 * pseudo-function. Returns a 64bit value, one 64bit parameter. */
portUInt64Type uxDemoApplicationSvcAdd64BitFunc( portUInt64Type uxValue )
{
    /* Add a 64bit arg, and current count, to 64 bit accumulator. */
    uxSvc64bitAccum += uxValue;
    uxSvc64bitAccum += uxSvcCounter;
    uxSvcCounter++;

    return uxSvc64bitAccum;
}
/*---------------------------------------------------------------------------*/

/* Sub-handler called via SVC hook for the SWI_ALIAS ulDemoApplicationSvcFourParams()
 * pseudo-function. Returns a 32bit value. Four 32bit parameters. */
portUInt32Type ulDemoApplicationSvcFourParamsFunc( portUInt32Type ulArg1, portUInt32Type ulArg2, portUInt32Type ulArg3, portUInt32Type ulArg4  )
{
    /* Do some arbitrary stuff with the four arguments. */
    ulArg1 += uxSvcCounter;
    uxSvcAccum += ulArg1;
    uxSvcAccum *= ulArg2;
    uxSvcAccum &= ulArg3;
    uxSvcAccum *= ulArg4;
    uxSvcCounter++;

    return uxSvcAccum;
}
/*---------------------------------------------------------------------------*/

#if ( fulldemoINCLUDE_SSI_TEST_DEMO == 1 )
static void prvDemoApplicationTriggerSSI( void )
{
static portBaseType xToggleTest = fulldemoSSI2;

    switch( xToggleTest )
    {
        case fulldemoSSI2:
            fulldemoSYS_SSIR2_REG = fulldemoSYS_SSIR2_SSKEY;
            break;    
    
        case fulldemoSSI3:
            fulldemoSYS_SSIR3_REG = fulldemoSYS_SSIR3_SSKEY;
            break;    
    
        case fulldemoSSI4:
            fulldemoSYS_SSIR4_REG = fulldemoSYS_SSIR4_SSKEY;
            break;     
      
        default:
            taskENTER_CRITICAL();
            {
                /* All three interrupts will be taken on exit from critical section. */
                fulldemoSYS_SSIR2_REG = fulldemoSYS_SSIR2_SSKEY;
                fulldemoSYS_SSIR3_REG = fulldemoSYS_SSIR3_SSKEY;
                fulldemoSYS_SSIR4_REG = fulldemoSYS_SSIR4_SSKEY;
            }
            taskEXIT_CRITICAL();
            xToggleTest = fulldemoSSI1;
            break;
    }
    xToggleTest++;

    /* For the simple test demo, the check task is checking for the effects
     * of the SSI interrupt immediately after this function exits. However,
     * the interrupt controller isn't closely enough coupled to guarantee that
     * the interrupt will have been taken by then. Some barriers here address
     * this. It would be more efficient to separate the call to this function
     * and the checking code, but doing it this way makes for simpler demo
     * code.
     */

    asm volatile ( " ISB " );
    asm volatile ( " DSB " );
}
#endif
/*---------------------------------------------------------------------------*/

void vDemoApplicationSSIHook( portUnsignedBaseType uxSSINumber )
{
	if( ( uxSSINumber & fulldemoSSIMASK ) == fulldemoSSI2 )
	{
        uxSSICounter2++;
	}
	if( ( uxSSINumber & fulldemoSSIMASK ) == fulldemoSSI3 )
	{
        uxSSICounter3++;
	}
	if( ( uxSSINumber & fulldemoSSIMASK ) == fulldemoSSI4 )
	{
        uxSSICounter4++;
	}
}
/*---------------------------------------------------------------------------*/

static void prvCheckTask( void *pvParameters )
{
portBaseType xCheckStatus;
portTickType xLastTime;
portTickType xCheckTaskCycleRate;
portUInt32Type ulErrorsDetected = 0UL;
portUInt32Type ulIdleHookLastCount = 0UL;
portUInt32Type ulTickHookLastCount = 0UL;
#if( fulldemoCHECK_IDLE_TIMES )
xPERCENTAGES xIdleTaskPercentages = { { 0UL, 0UL }, { 0UL, 0UL } };
#endif
#if ( fulldemoINCLUDE_SVC_TEST_DEMO == 1 )
portUnsignedBaseType uxCachedSvcCounter = 0U;
portUInt64Type uxSvcCheck64BitVal = 0ULL;
portUnsignedBaseType uxSvcCheck32bitVal = 0UL;
#endif
#if ( fulldemoINCLUDE_SSI_TEST_DEMO == 1 )
portUnsignedBaseType uxCachedSSICounter2 = 0U;
portUnsignedBaseType uxCachedSSICounter3 = 0U;
portUnsignedBaseType uxCachedSSICounter4 = 0U;
#endif

	/* The parameters are not used in this task. This just prevents a compiler
	 * warning. */
	( void ) pvParameters;

	/* Show the program is running. */
	vParTestSetLED( fulldemoLED_CHECK_TASK, pdTRUE );

	/* No errors have been detected yet. */
	xCheckTaskCycleRate = fulldemoCHECK_TASK_NORMAL_CYCLE_RATE;

	/* Initialise the variable used in calls to xTaskDelayUntil() to control
	 * the tasks execution period to the current time. */
	xLastTime = xTaskGetTickCount();

	/* Forever loop... */
	for( ;; )
	{
		/* Delay until the next check time. */
		( void ) xTaskDelayUntil( &xLastTime, xCheckTaskCycleRate );

		/**********************************************************************
		 * Check Demo Test operations.
		 **********************************************************************
		 * Ask each set of demo tasks to report their status.
		 **********************************************************************
		 * This set of demo tasks reported an error.
		 * Send an error message to the display task and latch that an error
		 * has occurred during this loop.
		 * Note that only a pointer to the error message is	actually queued so
		 * this assumes the string is in const, non-stack memory.
		 * This assumption is fine for a simple demo, but not for a high
		 * integrity application!
		 *********************************************************************/

		/**********************************************************************
		 * The first block of standard demo tasks are dependent on the hardware
		 * capability of either the processor or target hardware and hence
		 * inclusion is decided by project level settings.
		 *********************************************************************/

#if ( fulldemoINCLUDE_COM_TEST_DEMO == 1 )
		/* Check COM Test tasks. */
		xCheckStatus = xAreComTestTasksStillRunning();
		if( pdTRUE != xCheckStatus )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_COM_TEST;
		}
#endif

#if ( fulldemoINCLUDE_SVC_TEST_DEMO == 1 )

		/* SVC call with no arguments or return value, via an SWI_ALIAS */
		vDemoApplicationSvcIncrValue();

		if( uxCachedSvcCounter == uxSvcCounter )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_SVC_COUNT;
		}
		else
		{
			uxCachedSvcCounter = uxSvcCounter;
		}

		/* SVC call with a 64bit arg and 64bit return, via an SWI_ALIAS */
        uxSvcCheck64BitVal = uxDemoApplicationSvcAdd64Bit( 0x0000432100009876ULL );

        if( uxSvcCheck64BitVal != uxSvc64bitAccum )
        {
            ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_SVC_RETURN;
        }

        if( uxCachedSvcCounter == uxSvcCounter )
        {
            ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_SVC_COUNT;
        }
        else
        {
            uxCachedSvcCounter = uxSvcCounter;
        }

        /* SVC with four 32bit args and a 32bit return value, via SWI_ALIAS. */
        uxSvcCheck32bitVal = uxDemoApplicationSvcFourParams( 123UL, 99UL, 0x5555AAAAUL, 78UL );

        if( uxSvcCheck32bitVal != uxSvcAccum )
        {
            ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_SVC_RETURN;
        }

        if( uxCachedSvcCounter == uxSvcCounter )
        {
            ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_SVC_COUNT;
        }
        else
        {
            uxCachedSvcCounter = uxSvcCounter;
        }

#endif
#if ( fulldemoINCLUDE_SSI_TEST_DEMO == 1 )
		/* Check SSI demo.*/
		prvDemoApplicationTriggerSSI();

		if( 0UL != uxSSICounter1 )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_SSI_COUNT;
		}

		if( ( uxCachedSSICounter2 == uxSSICounter2 ) &&
			( uxCachedSSICounter3 == uxSSICounter3 ) &&
			( uxCachedSSICounter4 == uxSSICounter4 ) )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_SSI_COUNT;
		}
		else
		{
			uxCachedSSICounter2 = uxSSICounter2;
			uxCachedSSICounter3 = uxSSICounter3;
			uxCachedSSICounter4 = uxSSICounter4;
		}
#endif
		/**********************************************************************
		 * The second block of standard demo tasks are not dependent on the
		 * hardware capability of either the processor or target hardware and
		 * therefore their inclusion is a matter of preference and controlled
		 * by settings in this file.
		 *********************************************************************/

#if ( fulldemoINCLUDE_MATHS_DEMO == 1 )
		/* Check Math Test tasks. */
		xCheckStatus = xAreMathsTasksStillRunning();
		if( pdTRUE != xCheckStatus )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_MATHS_TASKS;
		}
#endif

#if ( fulldemoINCLUDE_BLOCK_Q_DEMO == 1 )
		/* Check Blocking Queue tasks. */
		xCheckStatus = xAreBlockingQueuesStillRunning();
		if( pdTRUE != xCheckStatus )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_BLOCKING_QUEUES;
		}
#endif

#if ( fulldemoINCLUDE_BLOCK_TIME_DEMO == 1 )
		/* Check Blocking Time tasks. */
		xCheckStatus = xAreBlockTimeTestTasksStillRunning();
		if( pdTRUE != xCheckStatus )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_BLOCK_TIME_TEST;
		}
#endif

#if ( fulldemoINCLUDE_DYNAMIC_DEMO == 1 )
		/* Check Dynamic tasks. */
		xCheckStatus = xAreDynamicPriorityTasksStillRunning();
		if( pdTRUE != xCheckStatus )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_DYNAMIC_PRIORITIES;
		}
#endif

#if ( fulldemoINCLUDE_POLL_Q_DEMO == 1 )
		/* Check Polled Queue tasks. */
		xCheckStatus = xArePollingQueuesStillRunning();
		if( pdTRUE != xCheckStatus )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_POLLING_QUEUES;
		}
#endif

#if ( fulldemoINCLUDE_SEMA_BINARY_DEMO == 1 )
		/* Check Binary Semaphore tasks. */
		xCheckStatus = xAreSemaphoreTasksStillRunning();
		if( pdTRUE != xCheckStatus )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_BINARY_SEMAPHORES;
		}
#endif

#if ( fulldemoINCLUDE_SEMA_COUNT_DEMO == 1 )
		/* Check Counting Semaphore tasks. */
		xCheckStatus = xAreCountingSemaphoreTasksStillRunning();
		if( pdTRUE != xCheckStatus )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_COUNTING_SEMAPHORES;
		}
#endif

#if ( fulldemoINCLUDE_TIMER_DEMO == 1 )
		xCheckStatus = xAreTimerDemoTasksStillRunning( xCheckTaskCycleRate,
													fulldemoTIMER_CMD_QUEUE_LEN );
		if( pdTRUE != xCheckStatus )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_TIMER_TASKS;
		}
#endif

#if ( fulldemoINCLUDE_TASK_NOTIFY_DEMO == 1 )
		xCheckStatus = xAreTaskNotificationTasksStillRunning();
		if( pdTRUE != xCheckStatus )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_TASK_NOTIFY;
		}
#endif

#if ( fulldemoINCLUDE_SUICIDE_DEMO == 1 )
		/* Check Suicide tasks. */
		xCheckStatus = xIsCreateTaskStillRunning();
		if( pdTRUE != xCheckStatus )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_CREATE_TASKS;
		}
#endif

#if ( fulldemoINCLUDE_REC_MUTEX_DEMO == 1 )
		xCheckStatus = xAreRecursiveMutexTasksStillRunning();
		if( pdTRUE != xCheckStatus )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_MUTEX;
		}
#endif

		/**********************************************************************
		 * Check SafeRTOS operating parameters.
		 *********************************************************************/

		/* Check that the Tick Hook is being called. */
		if( ulTickHookCallCount == ulTickHookLastCount )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_TICK_HOOK;
		}
		ulTickHookLastCount = ulTickHookCallCount;

		/* Finally, check that the idle task is getting called. */
		if( ulIdleHookCallCount == ulIdleHookLastCount )
		{
			ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_IDLE_TASK;
		}
		ulIdleHookLastCount = ulIdleHookCallCount;

#if( fulldemoCHECK_IDLE_TIMES == 1 )
		/* Retrieve the runtime statistics associated with the idle task. */
		if( NULL != xIdleTaskHandle )
		{
			xCheckStatus = xCalculateCPUUsage( xIdleTaskHandle, &xIdleTaskPercentages );

			if( pdPASS == xCheckStatus )
			{
				/* It is now possible to inspect the statistics to ensure that
				 * the expected percentage of CPU time is being spent in the
				 * Idle task - this will be different for each application. */

				if( xIdleTaskPercentages.xPeriod.ulCurrent < fulldemoIDLE_TASK_MINIMUM_CPU_PERCENTAGE )
				{
					ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_INSUFFICIENT_IDLE;
				}

				if( xIdleTaskPercentages.xOverall.ulMax > fulldemoIDLE_TASK_MAXIMUM_CPU_PERCENTAGE )
				{
					ulErrorsDetected |= fulldemoCHECK_TASK_ERROR_EXCESSIVE_IDLE;
				}
			}
		}
#endif /* ( fulldemoCHECK_IDLE_TIMES == 1 ) */

		/* Check that the tick count and the tick hook call count match. */
		if( xTaskGetTickCount() != ulTickHookCallCount )
		{
			ulErrorsDetected |= fulldemoCHECK_TICK_HOOK_COUNT;
		}

		if( xTaskGetCurrentTaskHandle() != (portTaskHandleType)portGET_CURRENT_TASK_HANDLE_REGISTER() )
		{
		    ulErrorsDetected |= fulldemoCHECK_TASK_PID_CHECK;
		}

		/* If an error has been detected... toggle check LED faster
		 * to give a visible indication of the error detection. */
		if( 0UL != ulErrorsDetected )
		{
			xCheckTaskCycleRate = fulldemoCHECK_TASK_ERROR_CYCLE_RATE;
		}

		/* Toggle the check task LED. */
		vParTestToggleLED( fulldemoLED_CHECK_TASK );
	}
}
/*---------------------------------------------------------------------------*/
