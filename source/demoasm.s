/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

#include "FullDemo.h" 

/*-----------------------------------------------------------------------------
 * Public Functions
 *---------------------------------------------------------------------------*/

	RSEG    CODE:CODE(2)
	ARM

	EXTERN vDemoApplicationSvcIncrValueFunc
	EXTERN uxDemoApplicationSvcAdd64BitFunc
	EXTERN ulDemoApplicationSvcFourParamsFunc
	
#if ( fulldemoIMPLEMENT_SVC_HOOK_IN_ASM == 1 )
	PUBLIC uxDemoApplicationSvcHook
	
/* This is an assembly language approach to the SVC hook - this code simply branches to
 * each "sub-handler" function, which is declared and defined with the same prototype
 * as the SWI_ALIAS function that leads to it. Although R4 is ultimately preserved
 * across the SVC call, when the SVC handler calls the SVC hook, the SVC number used
 * is available in R4, so the assembly language hook function can be fairly
 * straightforward, as in this simple example.
 *
 * The TI compiler manual states that the SWI_ALIAS should only be used to define
 * "functions" with up four registers' worth of parameters, i.e. passing arguments
 * is only really supported through the volatile registers. Note that if the SVC
 * function returns a struct, then one of the parameter registers will be used to
 * pass in an address to that struct. Any 64bit argument or return value will use
 * a pair of registers.
 *
 * If an SWI_ALIAS function is declared with more parameters than can be accommodated
 * by the volatile registers, the compiler will issue a warning that it passes some
 * parameters on the stack. The compiler doesn't attempt to use the supervisor mode
 * stack for this purpose. To access such parameters, the hook function would need to
 * obtain the caller's stack pointer.
 */
                 
	uxDemoApplicationSvcHook:
	     CMP     R4, #fulldemoSVC_INCREMENT_VALUE
	     BEQ     vDemoApplicationSvcIncrValueFunc             
	     CMP     R4, #fulldemoSVC_ADD_64BIT
	     BEQ     uxDemoApplicationSvcAdd64BitFunc
	     CMP     R4, #fulldemoSVC_FOUR_PARAMS
	     BEQ     ulDemoApplicationSvcFourParamsFunc
	     BX      LR
	     NOP

#endif

	END


