/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

#ifndef SAFERTOS_CONFIG_H
#define SAFERTOS_CONFIG_H

/* Hardware includes */
#include "HL_reg_rti.h"

#define configKERNEL_FUNC_DEF  			_Pragma("location=\"__kernel_functions__\"")
#define configKERNEL_DATA_DEF  			_Pragma("location=\"__kernel_data__\"")

/* Number of Task priorities. */
#define configMAX_PRIORITIES				( 10U )

/* Minimal size for Task's stacks. */
#define configMINIMAL_STACK_SIZE_WITH_FPU	( 512U )	/* Needs to be a power of two value. */
#define configMINIMAL_STACK_SIZE_NO_FPU		( 256U )	/* Needs to be a power of two value. */

/* System tick rate. */
#define configTICK_RATE_HZ					( 1000UL )

/* This macro calculates the number of ticks per millisecond.
 * NOTE: This integer calculation is only correct for values of
 * configTICK_RATE_HZ less than or equal to 1000 that are also divisors
 * of 1000.
 */
#define configTICK_RATE_MS			( ( portTickType ) 1000UL / configTICK_RATE_HZ )

/* This constant represents the RTI clock speed. For this demo application,
 * the PLL is configured to generate a CPU clock of 300MHz. HCLK source is CPU clock/2 (150MHz),
 * and RTI1CLK = ( HCLK / 2). For this demo this constant also represents the VCLKx's
 */
#define configCPU_CLOCK_HZ          ( 75000000UL )

/* If the queue registry files are included for use with the StateViewer plugin
 * then this defines the size of the queue registry. */
#define configQUEUE_REGISTRY_SIZE			( 0 )

/* Number of interrupt channels (0:126).
 * NOTE: from the TMS570 reference manual, "Channel 126 does not have a
 * dedicated vector and shall not be used". */
#define configVIM_NUM_INTERRUPTS	( 126U )

/* Macros used for run-time statistics. This is the interface with a
 * counter/timer, usually the SysTick timer, that has an accessible counter
 * that increments or decrements by a reasonable number of counts per tick
 * interrupt. Two macros are required, as follows:
 *
 *	configRTS_COUNTER_VALUE -	a macro expected to return an ASCENDING value.
 *
 *	configRTS_COUNTER_MAX -		a macro that is either a constant or an
 *								expression involving a dereferenced register
 *								pointer (e.g. a reload register or match
 *								register), that gives the maximum numeric value
 *								that configRTS_COUNTER_VALUE can reach before
 *								it wraps around to zero.
 */

#define configRTI_BLOCK_NUM					( 0U )
#define configRTI_COMPARE_NUM				( 0U )

/* This macro is used by the Real-Time Statistics functions to read a 32-bit
 * self-incrementing up-counter.
 * On this demo, the Real-Time Statistics functions use the RTI Block #0
 * counter, therefore configRTS_COUNTER_VALUE is simply a dereferenced
 * pointer to the RTI FRC0 register.
 */
#define configRTS_COUNTER_VALUE		( rtiREG1->CNT[ configRTI_BLOCK_NUM ].FRCx )

/* The TMS570 RTI counter has an ascending count that triggers an interrupt
 * when it matches a match register, whereupon the match register is advanced
 * by a reload count. So the counter itself doesn't reset to zero on every
 * interrupt, rather it free-runs and wraps at the max 32-bit value.
 * configRTS_COUNTER_MAX is thus 0xFFFFFFFF.
 */
#define configRTS_COUNTER_MAX		( 0xFFFFFFFFUL )

/*--------------------------------------------------------------------------*/


#endif /* SAFERTOS_CONFIG_H */
