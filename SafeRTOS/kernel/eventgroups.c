/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

#define EVENT_GROUPS_C

/* Scheduler includes. */
#define KERNEL_SOURCE_FILE
#include "SafeRTOS_API.h"

/* MCDC Test Point: PROTOTYPE */

/*-----------------------------------------------------------------------------
 * Local Type Definitions
 *---------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------
 * Local Constant Definitions
 *---------------------------------------------------------------------------*/

/* Constant representing the case where no event bits are set. */
#define evgrpNO_EVENT_BITS_SET		( ( eventBitsType ) 0 )

/* Constant that represents a zero wait time. */
#define evgrpNO_BLOCK_TIME			( ( portTickType ) 0 )


/*-----------------------------------------------------------------------------
 * Local Macro Definitions
 *---------------------------------------------------------------------------*/

#define prvIS_EVENT_GROUP_VALID( pxEventGroup )	( ( portBaseType )( ( portUnsignedBaseType )( pxEventGroup ) == ~( ( pxEventGroup )->uxEventMirror ) ) )


/*-----------------------------------------------------------------------------
 * Local Function Declarations
 *---------------------------------------------------------------------------*/
/* Test the bits set in xCurrentEventBits to see if the wait condition is met.
 * The wait condition is defined by xWaitForAllBits.  If xWaitForAllBits is
 * pdTRUE then the wait condition is met if all the bits set in xBitsToWaitFor
 * are also set in xCurrentEventBits.  If xWaitForAllBits is pdFALSE then the
 * wait condition is met if any of the bits set in xBitsToWait for are also set
 * in xCurrentEventBits. */
KERNEL_FUNCTION static portBaseType prvTestWaitCondition( const eventBitsType xCurrentEventBits,
														  const eventBitsType xBitsToWaitFor,
														  const portBaseType xWaitForAllBits );


/*-----------------------------------------------------------------------------
 * Local Variable Declarations
 *---------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------
 * Public Function Definitions
 *---------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xEventGroupCreate( eventGroupType *pxEventGroup,
												eventGroupHandleType *pxEventGroupHandle )
{
portBaseType xReturn = pdPASS;

	if( NULL == pxEventGroup )
	{
		/* A valid Event Group handle must be supplied. */
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_IF "xEventGroupCreate" */
		/* SAFERTOSTRACE EVENTGROUPCREATE_FAILED */
	}
	else if( NULL == pxEventGroupHandle )
	{
		/* A valid pointer must be supplied. */
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_ELSE_IF "xEventGroupCreate" */
		/* SAFERTOSTRACE EVENTGROUPCREATE_FAILED */
	}
	else
	{
		portENTER_CRITICAL_WITHIN_API();
		{
			if( prvIS_EVENT_GROUP_VALID( pxEventGroup ) )
			{
				/* Error if the provided event group handle is valid. An event group
				 * must be explicitly deleted before it can be recreated. */
				xReturn = errEVENT_GROUP_ALREADY_IN_USE;

				/* MCDC Test Point: STD_IF "xEventGroupCreate" */
			}
			else
			{
				/* Ensure all the event bits are reset. */
				pxEventGroup->xEventBits = evgrpNO_EVENT_BITS_SET;

				/* Initialize the list of tasks waiting for bits to be set. */
				vListInitialise( &( pxEventGroup->xTasksWaitingForBits ) );

				/* Finally, set the mirror value. */
				pxEventGroup->uxEventMirror = ~( ( portUnsignedBaseType ) pxEventGroup );

				/* Initialize the event group handle. */
				*pxEventGroupHandle = ( eventGroupHandleType ) pxEventGroup;

				/* MCDC Test Point: STD_ELSE "xEventGroupCreate" */
			}
		}
		portEXIT_CRITICAL_WITHIN_API();

		/* SAFERTOSTRACE EVENTGROUPCREATE */

		/* MCDC Test Point: STD_ELSE "xEventGroupCreate" */
	}

	return xReturn;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xEventGroupGetBits( eventGroupHandleType xEventGroupHandle,
												 eventBitsType *pxEventBitsSet )
{
portBaseType xReturn = pdPASS;
eventGroupType *pxEventGroup;

	if( NULL == xEventGroupHandle )
	{
		/* A valid Event Group handle must be supplied. */
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_IF "xEventGroupGetBits" */
	}
	else if( NULL == pxEventBitsSet )
	{
		/* A valid pointer must be supplied. */
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_ELSE_IF "xEventGroupGetBits" */
	}
	else
	{
		/* Obtain access to the Event Group structure. */
		pxEventGroup = ( eventGroupType * ) xEventGroupHandle;

		portENTER_CRITICAL_WITHIN_API();
		{
			/* Check the mirror within a critical section, since another task
			 * may delete the event group before we access xEventBits. */
			if( prvIS_EVENT_GROUP_VALID( pxEventGroup ) == pdFALSE )
			{
				/* A valid Event Group handle must be supplied. */
				xReturn = errINVALID_EVENT_GROUP_HANDLE;

				/* MCDC Test Point: STD_IF "xEventGroupGetBits" */
			}
			else
			{
				/* Retrieve the current setting of the event bits. */
				*pxEventBitsSet = pxEventGroup->xEventBits;

				/* MCDC Test Point: STD_ELSE "xEventGroupGetBits" */
			}
		}
		portEXIT_CRITICAL_WITHIN_API();

		/* MCDC Test Point: STD_ELSE "xEventGroupGetBits" */
	}

	return xReturn;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xEventGroupGetBitsFromISR( eventGroupHandleType xEventGroupHandle,
														eventBitsType *pxEventBitsSet )
{
portBaseType xReturn = pdPASS;
eventGroupType *pxEventGroup = ( eventGroupType * ) xEventGroupHandle;

	if( NULL == xEventGroupHandle )
	{
		/* A valid handle must be supplied. */
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_IF "xEventGroupGetBitsFromISR" */
	}
	else if( prvIS_EVENT_GROUP_VALID( pxEventGroup ) == pdFALSE )
	{
		/* Since event groups cannot be deleted from an ISR, we can check the
		 * mirror while interrupts are enabled. */

		/* A valid Event Group handle must be supplied. */
		xReturn = errINVALID_EVENT_GROUP_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xEventGroupGetBitsFromISR" */
	}
	else if( NULL == pxEventBitsSet )
	{
		/* A valid pointer must be supplied. */
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_ELSE_IF "xEventGroupGetBitsFromISR" */
	}
	else
	{
		/* Retrieve the current setting of the event bits. */
		*pxEventBitsSet = pxEventGroup->xEventBits;

		/* MCDC Test Point: STD_ELSE "xEventGroupGetBitsFromISR" */
	}

	return xReturn;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xEventGroupSetBits( eventGroupHandleType xEventGroupHandle,
												 const eventBitsType xBitsToSet )
{
portBaseType xReturn = pdPASS;
eventGroupType *pxEventGroup = ( eventGroupType * ) xEventGroupHandle;
xList *pxTaskList;
const xListItem *pxTaskListEnd;
xListItem *pxListItem;
xListItem *pxNextListItem;
eventBitsType xBitsWaitedFor;
eventBitsType xControlBits;
eventBitsType xBitsToClear = evgrpNO_EVENT_BITS_SET;
portBaseType xMatchFound = pdFALSE;
portBaseType xYieldRequired = pdFALSE;
portBaseType xAlreadyYielded;
portBaseType xTaskRemovedFromList;

	if( NULL == xEventGroupHandle )
	{
		/* A valid handle must be supplied. */
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_IF "xEventGroupSetBits" */
	}
	else if( evgrpNO_EVENT_BITS_SET != ( xBitsToSet & evgrpEVENT_BITS_CONTROL_BYTES ) )
	{
		/* Setting any bit in the control byte is not permitted. */
		xReturn = errINVALID_PARAMETERS;

		/* MCDC Test Point: STD_ELSE_IF "xEventGroupSetBits" */
	}
	else
	{
		/* Obtain the list of waiting tasks. */
		pxTaskList = &( pxEventGroup->xTasksWaitingForBits );

		/* Obtain the end of list marker for the list of waiting tasks. */
		pxTaskListEnd = listGET_END_MARKER( pxTaskList );

		/* Ensure no context switch occurs while the current task is accessing
		 * the Event Group. This does not disable interrupts. */
		vTaskSuspendScheduler();
		{
			/* Check the mirror with the scheduler disabled, since another task
			 * may delete the event group. */
			if( prvIS_EVENT_GROUP_VALID( pxEventGroup ) == pdFALSE )
			{
				/* A valid Event Group handle must be supplied. */
				xReturn = errINVALID_EVENT_GROUP_HANDLE;

				/* MCDC Test Point: STD_IF "xEventGroupSetBits" */
			}
			else
			{
				/* SAFERTOSTRACE EVENTGROUPSETBITS */

				/* Set the specified event bits. */
				pxEventGroup->xEventBits |= xBitsToSet;

				/* Obtain the head entry in the list of waiting tasks. */
				pxListItem = listGET_HEAD_ENTRY( pxTaskList );

				/* See if the new bit value should unblock any tasks. */
				/* MCDC Test Point: WHILE_EXTERNAL "xEventGroupSetBits" "( pxListItem != pxTaskListEnd )" */
				while( pxListItem != pxTaskListEnd )
				{
					pxNextListItem = listGET_NEXT( pxListItem );
					xBitsWaitedFor = listGET_LIST_ITEM_VALUE( pxListItem );
					xMatchFound = pdFALSE;

					/* Split the bits waited for from the control bits. */
					xControlBits = xBitsWaitedFor & evgrpEVENT_BITS_CONTROL_BYTES;
					xBitsWaitedFor &= ~evgrpEVENT_BITS_CONTROL_BYTES;

					if( evgrpNO_EVENT_BITS_SET == ( xControlBits & evgrpWAIT_FOR_ALL_BITS ) )
					{
						/* Just looking for single bit being set. */
						if( ( xBitsWaitedFor & pxEventGroup->xEventBits ) != evgrpNO_EVENT_BITS_SET )
						{
							xMatchFound = pdTRUE;

							/* MCDC Test Point: STD_IF "xEventGroupSetBits" */
						}
						/* MCDC Test Point: ADD_ELSE "xEventGroupSetBits" */
					}
					else if( ( xBitsWaitedFor & pxEventGroup->xEventBits ) == xBitsWaitedFor )
					{
						/* All bits are set. */
						xMatchFound = pdTRUE;

						/* MCDC Test Point: STD_ELSE_IF "xEventGroupSetBits" */
					}
					else
					{
						/* Need all bits to be set, but not all the bits were set. */

						/* MCDC Test Point: STD_ELSE "xEventGroupSetBits" */
					}

					if( pdFALSE != xMatchFound )
					{
						/* The bits match.  Should the bits be cleared on exit? */
						if( ( xControlBits & evgrpCLEAR_EVENTS_ON_EXIT_BIT ) != evgrpNO_EVENT_BITS_SET )
						{
							xBitsToClear |= xBitsWaitedFor;

							/* MCDC Test Point: STD_IF "xEventGroupSetBits" */
						}
						/* MCDC Test Point: ADD_ELSE "xEventGroupSetBits" */

						/* Store the actual event flag value in the task's event
						 * list item before removing the task from the event list.
						 * The evgrpUNBLOCKED_DUE_TO_BIT_SET bit is set so the task
						 * knows that is was unblocked due to its required bits
						 * matching, rather than because it timed out. */
						xTaskRemovedFromList = xTaskRemoveFromUnorderedEventList( pxListItem,
																				  ( pxEventGroup->xEventBits |
																					evgrpUNBLOCKED_DUE_TO_BIT_SET ) );
						if( pdFALSE != xTaskRemovedFromList )
						{
							xYieldRequired = pdTRUE;

							/* MCDC Test Point: STD_IF "xEventGroupSetBits" */
						}
						/* MCDC Test Point: ADD_ELSE "xEventGroupSetBits" */
					}
					/* MCDC Test Point: ADD_ELSE "xEventGroupSetBits" */

					/* Move onto the next list item.  Note pxListItem->pxNext is not
					 * used here as the list item may have been removed from the
					 * event list and inserted into the ready/pending reading
					 * list. */
					pxListItem = pxNextListItem;

					/* MCDC Test Point: WHILE_INTERNAL "xEventGroupSetBits" "( pxListItem != pxTaskListEnd )" */
				}

				/* Clear any bits that matched when the
				 * evgrpCLEAR_EVENTS_ON_EXIT_BIT bit was set in the control word. */
				pxEventGroup->xEventBits &= ~xBitsToClear;

				/* MCDC Test Point: STD_ELSE "xEventGroupSetBits" */
			}
		}
		xAlreadyYielded = xTaskResumeScheduler();

		/* If one or more higher priority tasks have been moved to the ready
		 * lists, ensure a context switch takes place. */
		/* MCDC Test Point: EXP_IF_AND "xEventGroupSetBits" "( pdFALSE != xYieldRequired )" "( pdFALSE == xAlreadyYielded )" */
		if( ( pdFALSE != xYieldRequired ) && ( pdFALSE == xAlreadyYielded ) )
		{
			taskYIELD_WITHIN_API();

			/* MCDC Test Point: NULL "xEventGroupSetBits" */
		}

		/* MCDC Test Point: STD_ELSE "xEventGroupSetBits" */
	}

	return xReturn;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xEventGroupSetBitsFromISR( eventGroupHandleType xEventGroupHandle,
														const eventBitsType xBitsToSet,
														portBaseType *pxHigherPriorityTaskWoken )
{
portBaseType xReturn;
const eventGroupType *pxEventGroup = ( const eventGroupType * ) xEventGroupHandle;

	if( NULL == xEventGroupHandle )
	{
		/* A valid Event Group handle must be supplied. */
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_IF "xEventGroupSetBitsFromISR" */
	}
	else if( prvIS_EVENT_GROUP_VALID( pxEventGroup ) == pdFALSE )
	{
		/* Since event groups cannot be deleted from an ISR, we can check the
		 * mirror with interrupts enabled. */

		/* A valid Event Group handle must be supplied. */
		xReturn = errINVALID_EVENT_GROUP_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xEventGroupSetBitsFromISR" */
	}
	else if( evgrpNO_EVENT_BITS_SET != ( xBitsToSet & evgrpEVENT_BITS_CONTROL_BYTES ) )
	{
		/* Setting any bit in the control byte is not permitted. */
		xReturn = errINVALID_PARAMETERS;

		/* MCDC Test Point: STD_ELSE_IF "xEventGroupSetBitsFromISR" */
	}
	else
	{
		/* SAFERTOSTRACE EVENTGROUPSETBITSISR */

		/* Send a message to the timer task requesting that xEventGroupSetBits()
		 * be executed from the timer task context. */
		xReturn = xTimerPendEventGroupSetBitsFromISR( xEventGroupHandle, xBitsToSet,
													  pxHigherPriorityTaskWoken );

		/* MCDC Test Point: STD_ELSE "xEventGroupSetBitsFromISR" */
	}

	return xReturn;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xEventGroupClearBits( eventGroupHandleType xEventGroupHandle,
												   const eventBitsType xBitsToClear )
{
portBaseType xReturn = pdPASS;
eventGroupType *pxEventGroup = ( eventGroupType * ) xEventGroupHandle;

	if( NULL == xEventGroupHandle )
	{
		/* A valid Event Group handle must be supplied. */
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_IF "xEventGroupClearBits" */
	}
	else if( evgrpNO_EVENT_BITS_SET != ( xBitsToClear & evgrpEVENT_BITS_CONTROL_BYTES ) )
	{
		/* Clearing any bit in the control byte is not permitted. */
		xReturn = errINVALID_PARAMETERS;

		/* MCDC Test Point: STD_ELSE_IF "xEventGroupClearBits" */
	}
	else
	{
		portENTER_CRITICAL_WITHIN_API();
		{
			/* Check the mirror within a critical section, since another task
			 * may delete the event group before we access xEventBits. */
			if( prvIS_EVENT_GROUP_VALID( pxEventGroup ) == pdFALSE )
			{
				/* A valid Event Group handle must be supplied. */
				xReturn = errINVALID_EVENT_GROUP_HANDLE;

				/* MCDC Test Point: STD_IF "xEventGroupClearBits" */
			}
			else
			{
				/* SAFERTOSTRACE EVENTGROUPCLEARBITS */

				/* Clear the specified bits. */
				pxEventGroup->xEventBits &= ~xBitsToClear;

				/* MCDC Test Point: STD_ELSE "xEventGroupClearBits" */
			}
		}
		portEXIT_CRITICAL_WITHIN_API();

		/* MCDC Test Point: STD_ELSE "xEventGroupClearBits" */
	}

	return xReturn;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xEventGroupClearBitsFromISR( eventGroupHandleType xEventGroupHandle,
														  const eventBitsType xBitsToClear,
														  portBaseType *pxHigherPriorityTaskWoken )
{
portBaseType xReturn;
const eventGroupType *pxEventGroup = ( const eventGroupType * ) xEventGroupHandle;

	if( NULL == xEventGroupHandle )
	{
		/* A valid Event Group handle must be supplied. */
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_IF "xEventGroupClearBitsFromISR" */
	}
	else if( prvIS_EVENT_GROUP_VALID( pxEventGroup ) == pdFALSE )
	{
		/* Since event groups cannot be deleted from an ISR, we can check the
		 * mirror with interrupts enabled. */

		/* A valid Event Group handle must be supplied. */
		xReturn = errINVALID_EVENT_GROUP_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xEventGroupClearBitsFromISR" */
	}
	else if( evgrpNO_EVENT_BITS_SET != ( xBitsToClear & evgrpEVENT_BITS_CONTROL_BYTES ) )
	{
		/* Clearing any bit in the control byte is not permitted. */
		xReturn = errINVALID_PARAMETERS;

		/* MCDC Test Point: STD_ELSE_IF "xEventGroupClearBitsFromISR" */
	}
	else
	{
		/* SAFERTOSTRACE EVENTGROUPCLEARBITSISR */

		/* Send a message to the timer task requesting that xEventGroupClearBits()
		 * be executed from the timer task context. */
		xReturn = xTimerPendEventGroupClearBitsFromISR( xEventGroupHandle, xBitsToClear,
														pxHigherPriorityTaskWoken );

		/* MCDC Test Point: STD_ELSE "xEventGroupClearBitsFromISR" */
	}

	return xReturn;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xEventGroupWaitBits( eventGroupHandleType xEventGroupHandle,
												  const eventBitsType xBitsToWaitFor,
												  const portBaseType xClearOnExit,
												  const portBaseType xWaitForAllBits,
												  eventBitsType *pxEventBitsSet,
												  portTickType xTicksToWait )
{
portBaseType xReturn = pdPASS;
eventGroupType *pxEventGroup = ( eventGroupType * ) xEventGroupHandle;
eventBitsType xCurrentEventBits;
eventBitsType xEventBitsSet = evgrpNO_EVENT_BITS_SET;
eventBitsType xControlBits = evgrpNO_EVENT_BITS_SET;
portBaseType xWaitConditionMet;
portBaseType xAlreadyYielded;

	/* Ensure the scheduler is not suspended. */
	if( xTaskIsSchedulerSuspended() != pdFALSE )
	{
		xReturn = errSCHEDULER_IS_SUSPENDED;

		/* MCDC Test Point: STD_IF "xEventGroupWaitBits" */
	}
	else if( NULL == xEventGroupHandle )
	{
		/* A valid Event Group handle must be supplied. */
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_ELSE_IF "xEventGroupWaitBits" */
	}
	else if( evgrpNO_EVENT_BITS_SET != ( xBitsToWaitFor & evgrpEVENT_BITS_CONTROL_BYTES ) )
	{
		/* Waiting for any bit in the control byte is not permitted. */
		xReturn = errINVALID_PARAMETERS;

		/* MCDC Test Point: STD_ELSE_IF "xEventGroupWaitBits" */
	}
	else if( evgrpNO_EVENT_BITS_SET == xBitsToWaitFor )
	{
		/* At least one wait bit must be specified. */
		xReturn = errINVALID_PARAMETERS;

		/* MCDC Test Point: STD_ELSE_IF "xEventGroupWaitBits" */
	}
	else
	{
		/* Ensure no context switch occurs while the current task is accessing
		 * the Event Group. This does not disable interrupts. */
		vTaskSuspendScheduler();
		{
			if( prvIS_EVENT_GROUP_VALID( pxEventGroup ) == pdFALSE )
			{
				/* A valid Event Group handle must be supplied. */
				xReturn = errINVALID_EVENT_GROUP_HANDLE;

				/* MCDC Test Point: STD_IF "xEventGroupWaitBits" */
			}
			else
			{
				/* Retrieve the current setting of the event bits. */
				xCurrentEventBits = pxEventGroup->xEventBits;

				/* Check to see if the wait condition is already met or not. */
				xWaitConditionMet = prvTestWaitCondition( xCurrentEventBits,
														  xBitsToWaitFor,
														  xWaitForAllBits );

				if( pdFALSE != xWaitConditionMet )
				{
					/* The wait condition has already been met so there is no
					 * need to block. */
					xEventBitsSet = xCurrentEventBits;
					xTicksToWait = evgrpNO_BLOCK_TIME;

					/* Clear the wait bits if requested to do so. */
					if( pdFALSE != xClearOnExit )
					{
						pxEventGroup->xEventBits &= ~xBitsToWaitFor;

						/* MCDC Test Point: STD_IF "xEventGroupWaitBits" */
					}
					/* MCDC Test Point: ADD_ELSE "xEventGroupWaitBits" */
				}
				else if( evgrpNO_BLOCK_TIME == xTicksToWait )
				{
					/* The wait condition has not been met, but no block time
					 * was specified. Ensure the current setting of the event
					 * bits is available if the application wants them. */
					xEventBitsSet = xCurrentEventBits;

					/* Let the caller know. */
					xReturn = errEVENT_GROUP_BITS_NOT_SET;

					/* MCDC Test Point: STD_ELSE_IF "xEventGroupWaitBits" */
				}
				else
				{
					/* The task is going to block to wait for its required bits
					 * to be set.  xControlBits are used to remember the
					 * specified behaviour of this call to xEventGroupWaitBits()
					 * - for use when the event bits unblock the task. */
					if( pdFALSE != xClearOnExit )
					{
						xControlBits |= evgrpCLEAR_EVENTS_ON_EXIT_BIT;

						/* MCDC Test Point: STD_IF "xEventGroupWaitBits" */
					}
					/* MCDC Test Point: ADD_ELSE "xEventGroupWaitBits" */

					if( pdFALSE != xWaitForAllBits )
					{
						xControlBits |= evgrpWAIT_FOR_ALL_BITS;

						/* MCDC Test Point: STD_IF "xEventGroupWaitBits" */
					}
					/* MCDC Test Point: ADD_ELSE "xEventGroupWaitBits" */

					/* Store the bits that the calling task is waiting for in
					 * the task's event list item so the kernel knows when a
					 * match is found. Then enter the blocked state. */
					vTaskPlaceOnUnorderedEventList( &( pxEventGroup->xTasksWaitingForBits ),
													( xBitsToWaitFor | xControlBits ),
													xTicksToWait );

					/* This is obsolete as it will get set after the task
					 * unblocks, but some compilers mistakenly generate a
					 * warning about the variable being returned without being
					 * set if it is not done. */
					xEventBitsSet = evgrpNO_EVENT_BITS_SET;

					/* SAFERTOSTRACE EVENTGROUPWAITBITS_BLOCK */

					/* MCDC Test Point: STD_ELSE "xEventGroupWaitBits" */
				}

				/* MCDC Test Point: STD_ELSE "xEventGroupWaitBits" */
			}
		}
		xAlreadyYielded = xTaskResumeScheduler();

		/* If there is no block time, there is nothing else to do. */
		if( evgrpNO_BLOCK_TIME != xTicksToWait )
		{
			/* Force a reschedule if xTaskResumeScheduler() has not already done
			 * so, in case this task is now in the blocked state. */
			if( pdFALSE == xAlreadyYielded )
			{
				taskYIELD_WITHIN_API();

				/* MCDC Test Point: STD_IF "xEventGroupWaitBits" */
			}
			/* MCDC Test Point: ADD_ELSE "xEventGroupWaitBits" */

			/* The task blocked to wait for its required bits to be set - at
			 * this point, either the required bits were set or the block time
			 * expired.  If the required bits were set, they will have been
			 * stored in the task's event list item, and they should now be
			 * retrieved then cleared. */
			xEventBitsSet = xTaskResetEventItemValue();

			if( evgrpNO_EVENT_BITS_SET == ( xEventBitsSet & evgrpUNBLOCKED_DUE_TO_BIT_SET ) )
			{
				if( evgrpNO_EVENT_BITS_SET != ( xEventBitsSet & evgrpUNBLOCKED_DUE_TO_DELETION ) )
				{
					/* The event group has been deleted.
					 * If the event bits must be returned to the caller,
					 * make sure that no bits are set. */
					xEventBitsSet = 0U;

					/* Let the caller know. */
					xReturn = errEVENT_GROUP_DELETED;

					/* MCDC Test Point: STD_IF "xEventGroupWaitBits" */
				}
				else
				{
					/* The task timed out, just return the current event bit
					 * value. */
					xEventBitsSet = pxEventGroup->xEventBits;

					/* It is possible that the event bits were updated between
					 * this task leaving the Blocked state and running again. */
					if( prvTestWaitCondition( xEventBitsSet, xBitsToWaitFor, xWaitForAllBits ) == pdFALSE )
					{
						/* Let the caller know that the wait condition is not
						 * satisfied. */
						xReturn = errEVENT_GROUP_BITS_NOT_SET;

						/* MCDC Test Point: STD_IF "xEventGroupWaitBits" */
					}
					else
					{
						/* The condition has become satisfied after the timeout,
						 * so we treat this as a success. However, since the
						 * task was no longer in the event list when the bits
						 * were updated, they may still need clearing. */
						if( pdFALSE != xClearOnExit )
						{
							portENTER_CRITICAL_WITHIN_API();
							{
								pxEventGroup->xEventBits &= ~xBitsToWaitFor;
							}
							portEXIT_CRITICAL_WITHIN_API();

							/* MCDC Test Point: STD_IF "xEventGroupWaitBits" */
						}
						/* MCDC Test Point: ADD_ELSE "xEventGroupWaitBits" */

						/* MCDC Test Point: STD_ELSE "xEventGroupWaitBits" */
					}

					/* MCDC Test Point: STD_ELSE "xEventGroupWaitBits" */
				}
			}
			else
			{
				/* The task unblocked because the bits were set. */

				/* MCDC Test Point: STD_ELSE "xEventGroupWaitBits" */
			}
		}
		/* MCDC Test Point: ADD_ELSE "xEventGroupWaitBits" */

		/* SAFERTOSTRACE EVENTGROUPWAITBITS_END */
		
		/* Does the application want to know the setting of the event bits? */
		if( NULL != pxEventBitsSet )
		{
			/* Ensure no control bits are reported to the application. */
			*pxEventBitsSet = ( xEventBitsSet & ~evgrpEVENT_BITS_CONTROL_BYTES );

			/* MCDC Test Point: STD_IF "xEventGroupWaitBits" */
		}
		/* MCDC Test Point: ADD_ELSE "xEventGroupWaitBits" */

		/* MCDC Test Point: STD_ELSE "xEventGroupWaitBits" */
	}

	

	return xReturn;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xEventGroupDelete( eventGroupHandleType xEventGroupHandle )
{
eventGroupType *pxEventGroup = ( eventGroupType * ) xEventGroupHandle;
const xList *pxTasksWaitingForBits;
portBaseType xReturn = pdPASS;
portBaseType xYieldRequired = pdFALSE;
portBaseType xAlreadyYielded;
portBaseType xTaskRemovedFromList;

	if( NULL == xEventGroupHandle )
	{
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_IF "xEventGroupDelete" */
	}
	else
	{
		vTaskSuspendScheduler();
		{
			if( prvIS_EVENT_GROUP_VALID( pxEventGroup ) == pdFALSE )
			{
				/* A valid Event Group handle must be supplied. */
				xReturn = errINVALID_EVENT_GROUP_HANDLE;

				/* MCDC Test Point: STD_IF "xEventGroupDelete" */
			}
			else
			{
				/* SAFERTOSTRACE EVENTGROUPDELETE */

				/* Invalidate mirror, so the event group will be detected as
				 * invalid. */
				pxEventGroup->uxEventMirror = 0U;

				/* Obtain access to the event list. */
				pxTasksWaitingForBits = &( pxEventGroup->xTasksWaitingForBits );

				/* MCDC Test Point: WHILE_EXTERNAL "xEventGroupDelete" "( listLIST_IS_EMPTY( pxTasksWaitingForBits ) == pdFALSE )" */
				while( listLIST_IS_EMPTY( pxTasksWaitingForBits ) == pdFALSE )
				{
					/* Unblock the task, returning 0 as the event list is being
					 * deleted and cannot therefore have any bits set. */
					xTaskRemovedFromList = xTaskRemoveFromUnorderedEventList( pxTasksWaitingForBits->xListEnd.pxNext,
																			  evgrpUNBLOCKED_DUE_TO_DELETION );
					if( pdFALSE != xTaskRemovedFromList )
					{
						xYieldRequired = pdTRUE;

						/* MCDC Test Point: STD_IF "xEventGroupDelete" */
					}
					/* MCDC Test Point: ADD_ELSE "xEventGroupDelete" */

					/* MCDC Test Point: WHILE_INTERNAL "xEventGroupDelete" "( listLIST_IS_EMPTY( pxTasksWaitingForBits ) == pdFALSE )" */
				}

				/* MCDC Test Point: STD_ELSE "xEventGroupDelete" */
			}
		}
		xAlreadyYielded = xTaskResumeScheduler();

		/* If one or more higher priority tasks have been moved to the ready
		 * lists, ensure a context switch takes place. */
		/* MCDC Test Point: EXP_IF_AND "xEventGroupDelete" "( pdFALSE != xYieldRequired )" "( pdFALSE == xAlreadyYielded )" */
		if( ( pdFALSE != xYieldRequired ) && ( pdFALSE == xAlreadyYielded ) )
		{
			taskYIELD_WITHIN_API();

			/* MCDC Test Point: NULL "xEventGroupDelete" */
		}

		/* MCDC Test Point: STD_ELSE "xEventGroupDelete" */
	}

	return xReturn;
}
/*---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 * Local Function Definitions
 *---------------------------------------------------------------------------*/

KERNEL_FUNCTION static portBaseType prvTestWaitCondition( const eventBitsType xCurrentEventBits,
														  const eventBitsType xBitsToWaitFor,
														  const portBaseType xWaitForAllBits )
{
portBaseType xWaitConditionMet = pdFALSE;

	if( pdFALSE == xWaitForAllBits )
	{
		/* The task only has to wait for one bit within xBitsToWaitFor to be
		 * set.  Is one already set? */
		if( ( xCurrentEventBits & xBitsToWaitFor ) != evgrpNO_EVENT_BITS_SET )
		{
			xWaitConditionMet = pdTRUE;

			/* MCDC Test Point: STD_IF "prvTestWaitCondition" */
		}
		/* MCDC Test Point: ADD_ELSE "prvTestWaitCondition" */
	}
	else
	{
		/* The task has to wait for all the bits in xBitsToWaitFor to be set.
		 * Are they set already? */
		if( ( xCurrentEventBits & xBitsToWaitFor ) == xBitsToWaitFor )
		{
			xWaitConditionMet = pdTRUE;

			/* MCDC Test Point: STD_IF "prvTestWaitCondition" */
		}
		/* MCDC Test Point: ADD_ELSE "prvTestWaitCondition" */

		/* MCDC Test Point: STD_ELSE "prvTestWaitCondition" */
	}

	return xWaitConditionMet;
}
/*---------------------------------------------------------------------------*/

/* SAFERTOSTRACE UXGETEVENTGROUPNUMBER */

#ifdef SAFERTOS_MODULE_TEST
	#include "eventGroupsTestHeaders.h"
	#include "eventGroupsTest.h"
#endif
