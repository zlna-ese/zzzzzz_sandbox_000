/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number 
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/


#define MPU_C

#define KERNEL_SOURCE_FILE

/* Scheduler includes */
#include "SafeRTOS.h"
#include "task.h"


/*--------------------------------------------------------------------------*/
/* Local Structures											 				*/
/*--------------------------------------------------------------------------*/

/* Defines MPU region configuration structure */
typedef struct mpuRegionSysConfig
{
	portUInt32Type ulRegionIdentifier;
	portUInt32Type ulRegionBeginAddress;
	portUInt32Type ulRegionEndAddress;
	portUInt32Type ulRegionAccess;
	portUInt32Type ulSubRegionDisable;
} mpuRegionSysConfigType;

/* MCDC Test Point: PROTOTYPE */

/*--------------------------------------------------------------------------*/

KERNEL_FUNCTION void vMPUSetupMPU( void )
{
portUInt32Type ulRegionIdx;
portUInt32Type ulRegionSize;
portUInt32Type ulNumberOfRegions;

const mpuRegionSysConfigType *pxRegionEntry;

mpuRegionSysConfigType axMPURegionSysConfigs[ ] =
{
	portmpuREGION_SYS_CONFIGURATION
};

	/* Calculate number of MPU system regions */
	ulNumberOfRegions = sizeof( axMPURegionSysConfigs ) /
						sizeof( mpuRegionSysConfigType );

	/* For each MPU system region... */
	for( ulRegionIdx = 0U; ulRegionIdx < ulNumberOfRegions; ulRegionIdx++ )
	{
		/* Locate MPU System region configuration settings */
		pxRegionEntry = &axMPURegionSysConfigs[ ulRegionIdx ];

		/* Calculate MPU system region size from address boundaries */
		ulRegionSize = pxRegionEntry->ulRegionEndAddress - pxRegionEntry->ulRegionBeginAddress;

		/* Configure MPU System regions */
		vPortMPUConfigureSystemRegion( pxRegionEntry->ulRegionIdentifier,
									   pxRegionEntry->ulRegionBeginAddress,
									   pxRegionEntry->ulRegionAccess,
									   ulRegionSize,
									   pxRegionEntry->ulSubRegionDisable );
		
		/* MCDC Test Point: STD "vMPUSetupMPU" */
	}

	/* Enable MPU operations */
	vPortMPUEnable( );
}
/*--------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xMPUConfigureGlobalRegion( portUInt32Type ulRegionNumber,
														portUInt32Type ulRegionBeginAddress,
														portUInt32Type ulRegionAccess,
														portUInt32Type ulRegionSize,
														portUInt32Type ulSubRegionDisable )
{
portUInt32Type ulCheckValidSize;
portUInt32Type ulCheckRegionAddr;
portBaseType xResult = pdFAIL;

	/* Global regions can only be defined before the scheduler has been started. */
	if( pdFALSE == xTaskIsSchedulerStarted() )
	{
		/* Check that the requested region is allowable. */
		/* MCDC Test Point: EXP_IF_AND "xMPUConfigureGlobalRegion" "( ulRegionNumber >= portmpuGLOBAL_CONFIGURABLE_REGION_FIRST )" "( ulRegionNumber <= portmpuGLOBAL_CONFIGURABLE_REGION_LAST )" "Deviate: FF" */
		if( ( ulRegionNumber >= portmpuGLOBAL_CONFIGURABLE_REGION_FIRST ) &&
			( ulRegionNumber <= portmpuGLOBAL_CONFIGURABLE_REGION_LAST ) )
		{
			/* No, check region address & size parameters */
			/* Reset check size to smallest valid size */
			ulCheckValidSize = portmpuSMALLEST_REGION_SIZE_ACTUAL;

			/* While region size is at least 32 bytes and a power of 2... */
			/* MCDC Test Point: EXP_WHILE_EXTERNAL_AND "xMPUConfigureGlobalRegion" "( ulCheckValidSize <  ulRegionSize )" "( portmpuLARGEST_REGION_SIZE_ACTUAL != ulCheckValidSize )" "Deviate: TF" "Deviate: FF" */
			while( ( ulCheckValidSize <  ulRegionSize ) &&
			       ( portmpuLARGEST_REGION_SIZE_ACTUAL != ulCheckValidSize ) )
			{
				ulCheckValidSize <<= 1U;

				/* MCDC Test Point: EXP_WHILE_INTERNAL_AND "xMPUConfigureGlobalRegion" "( ulCheckValidSize <  ulRegionSize )" "( ulCheckValidSize != portmpuLARGEST_REGION_SIZE_ACTUAL )" */
			}

			/* Is region size valid? */
			if( ulCheckValidSize != ulRegionSize )
			{
				/* No, set error result */
				xResult = errINVALID_PARAMETERS;
				/* MCDC Test Point: STD_IF "xMPUConfigureGlobalRegion" */
			}
			else
			{
				/* Yes, Now Check region Base Address */
				ulCheckRegionAddr = ( ulRegionBeginAddress % ulRegionSize );

				/* Is region Address Correctly Aligned? */
				if( 0U != ulCheckRegionAddr )
				{
					/* No, Set Error Result */
					xResult = errINVALID_PARAMETERS;

					/* MCDC Test Point: STD_IF "xMPUConfigureGlobalRegion" */
				}
				else
				{
					/* Load MPU System region parameters into MPU Sub-Processor.
					 * The allowable values for access permissions etc are port
					 * specific, therefore masks are applied in the portable
					 * layer to ensure illegal values are not loaded into the
					 * MPU registers. */
					vPortMPUConfigureSystemRegion( ulRegionNumber,
												   ulRegionBeginAddress,
												   ulRegionAccess,
												   ulRegionSize,
												   ulSubRegionDisable );

					xResult = pdPASS;
					/* MCDC Test Point: STD_ELSE "xMPUConfigureGlobalRegion" */
				}
				/* MCDC Test Point: STD_ELSE "xMPUConfigureGlobalRegion" */
			}
		}
		else
		{
			xResult = errINVALID_PARAMETERS;
			/* MCDC Test Point: STD_ELSE "xMPUConfigureGlobalRegion" */
		}
	}
	else
	{
		xResult = errSCHEDULER_ALREADY_RUNNING;
		/* MCDC Test Point: STD_ELSE "xMPUConfigureGlobalRegion" */
	}

	return xResult;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xMPUSetTaskRegions( portTaskHandleType pxSetTaskToModify,
												 const mpuTaskParamType * const pxSetTaskMpuParams )
{
portBaseType xSetResult = pdPASS;

/* Obtain the TCB pointer from the task handle (which could be NULL). */
xTCB *pxSetTCB = taskGET_TCB_FROM_HANDLE( pxSetTaskToModify );

	/* Ensure we don't attempt to access a NULL pointer. */
	if( NULL == pxSetTaskMpuParams )
	{
		xSetResult = errNULL_PARAMETER_SUPPLIED;
		
		/* MCDC Test Point: STD_IF "xMPUSetTaskRegions" */
	}
	else
	{
		portENTER_CRITICAL_WITHIN_API();
		{
			/* Check that the TCB is valid. */
			if( xPortIsTaskHandleValid( pxSetTCB ) == pdFALSE )
			{
				xSetResult = errINVALID_TASK_HANDLE;

				/* MCDC Test Point: STD_IF "xMPUSetTaskRegions" */
			}
			/* MCDC Test Point: ADD_ELSE "xMPUSetTaskRegions" */
		}
		portEXIT_CRITICAL_WITHIN_API();
		
		if( pdPASS == xSetResult )
		{
			/* Ensure the new region configuration is valid. */
			if( xMPUCheckTaskRegions( pxSetTaskMpuParams->axRegions ) == pdFAIL )
			{
				xSetResult = errINVALID_MPU_REGION_CONFIGURATION;
				
				/* MCDC Test Point: STD_IF "xMPUSetTaskRegions" */

			}
			else
			{
				/* Update the task's TCB with the new region configuration. */
				vMPUStoreTaskRegions( pxSetTCB, pxSetTaskMpuParams );

				/* If the task being modified is the task that is currently
				 * running, force a yield to ensure that the new regions are
				 * in place before the task continues running. */
				if( pxSetTCB == pxCurrentTCB )
				{
					taskYIELD_WITHIN_API();

					/* MCDC Test Point: STD_IF "xMPUSetTaskRegions" */
				}
				/* MCDC Test Point: ADD_ELSE "xMPUSetTaskRegions" */
				/* MCDC Test Point: STD_ELSE "xMPUSetTaskRegions" */
			}
		}
		/* MCDC Test Point: ADD_ELSE "xMPUSetTaskRegions" */
		/* MCDC Test Point: STD_ELSE "xMPUSetTaskRegions" */
	}

	return xSetResult;
}
/*--------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xMPUCheckTaskRegions( const mpuRegionConfigType *pxCheckTaskRegions )
{
portUInt32Type ulCheckRegionIdx;
portUInt32Type ulCheckValidSize;
portUInt32Type ulCheckRegionAddr;
portUInt32Type ulCheckRegionSize;

portBaseType xCheckResult = pdPASS;

	/*
	 * The input parameter pxCheckTaskRegions points to an array of axRegions
	 * - the validity of the pxCheckTaskRegions pointer is assured since this
	 * function is only ever called from within kernel code.
	 */

	/* For each region... Check region parameters are valid */
	for( ulCheckRegionIdx = 0U; ulCheckRegionIdx < portmpuCONFIGURABLE_REGION_NUM; ulCheckRegionIdx++ )
	{
		/* What is the size of this region? */
		ulCheckRegionSize = pxCheckTaskRegions[ ulCheckRegionIdx ].ulLengthInBytes;

		/* Null region size? */
		if( 0U != ulCheckRegionSize )
		{
			/* No, check region address & size parameters */
			/* Reset check size to smallest valid size */
			ulCheckValidSize = portmpuSMALLEST_REGION_SIZE_ACTUAL;

			/* While region size is at least 32 bytes and a power of 2... */
			/* MCDC Test Point: EXP_WHILE_EXTERNAL_AND "xMPUCheckTaskRegions" "( ulCheckValidSize <  ulCheckRegionSize )" "( portmpuLARGEST_REGION_SIZE_ACTUAL != ulCheckValidSize )" "Deviate: TF" "Deviate: FF" */
			while( ( ulCheckValidSize <  ulCheckRegionSize ) &&
			       ( portmpuLARGEST_REGION_SIZE_ACTUAL != ulCheckValidSize ) )
			{
				ulCheckValidSize <<= 1U;

				/* MCDC Test Point: EXP_WHILE_INTERNAL_AND "xMPUCheckTaskRegions" "( ulCheckValidSize <  ulCheckRegionSize )" "( ulCheckValidSize != portmpuLARGEST_REGION_SIZE_ACTUAL )" */
			}

			/* Is region size valid? */
			if( ulCheckValidSize != ulCheckRegionSize )
			{
				/* No, set error result */
				xCheckResult = pdFAIL;
				/* MCDC Test Point: STD_IF "xMPUCheckTaskRegions" */
			}
			else
			{
				/* Yes, Now Check region Base Address */
				ulCheckRegionAddr = ( portUInt32Type ) pxCheckTaskRegions[ ulCheckRegionIdx ].pvBaseAddress;
				ulCheckRegionAddr = ( ulCheckRegionAddr % ulCheckRegionSize );

				/* Is region Address Correctly Aligned? */
				if( 0U != ulCheckRegionAddr )
				{
					/* No, Set Error Result */
					xCheckResult = pdFAIL;
					
					/* MCDC Test Point: STD_IF "xMPUCheckTaskRegions" */
				}
				/* MCDC Test Point: ADD_ELSE "xMPUCheckTaskRegions" */
				/* MCDC Test Point: STD_ELSE "xMPUCheckTaskRegions" */
			}
		}
		/* MCDC Test Point: ADD_ELSE "xMPUCheckTaskRegions" */
	}

	return xCheckResult;
}
/*--------------------------------------------------------------------------*/

KERNEL_FUNCTION void vMPUStoreTaskRegions( portTaskHandleType pxStoreTaskToModify,
										   const mpuTaskParamType * const pxStoreTaskMpuRegions )
{
portUInt32Type ulStoreRegionIdx;
portUInt32Type ulStoreRegionSize;
xTCB* pxStoreTCB = ( xTCB * ) pxStoreTaskToModify;
const mpuRegionConfigType *pxStoreMpuRegion;

	/*
	 * The MPU region data in the TCB is updated during a context switch,
	 * so a	context switch must not occur while the TCB is being updated.
	 */
	portENTER_CRITICAL_WITHIN_API();
	{
		/* For each MPU region... Store task TCB MPU region values */
		for( ulStoreRegionIdx = 0U; ulStoreRegionIdx < portmpuCONFIGURABLE_REGION_NUM; ulStoreRegionIdx++ )
		{
			/* Get MPU region memory entry parameters */
			pxStoreMpuRegion = &( pxStoreTaskMpuRegions->axRegions[ ulStoreRegionIdx ] );

			/* Have We a valid MPU region memory entry? */
			if( pxStoreMpuRegion->ulLengthInBytes > 0U )
			{
				/* Yes, setup task MPU region memory entry */
				vPortMPUEncodeTCBRegion( &pxStoreTCB->axMPURegions[ ulStoreRegionIdx ],
										 ( portmpuCONFIGURABLE_REGION_FIRST + ulStoreRegionIdx ),
										 ( portUInt32Type ) pxStoreMpuRegion->pvBaseAddress,
										 pxStoreMpuRegion->ulAccessPermissions,
										 pxStoreMpuRegion->ulLengthInBytes,
										 pxStoreMpuRegion->ulSubRegionControl );

				/* MCDC Test Point: STD_IF "vMPUStoreTaskRegions" */
			}
			else
			{
				/* No, setup default task MPU region memory entry */
				vPortMPUEncodeTCBRegion( &pxStoreTCB->axMPURegions[ ulStoreRegionIdx ],
										 ( portmpuCONFIGURABLE_REGION_FIRST + ulStoreRegionIdx ),
										 0U, 0U, 0U, 0U );

				/* MCDC Test Point: STD_ELSE "vMPUStoreTaskRegions" */
			}

			/* Now setup task MPU mirror region memory entries */
			vPortMPUCreateTCBMirrorRegion( &pxStoreTCB->axMPURegions[ ulStoreRegionIdx ],
										   &pxStoreTCB->axMPURegionsMirror[ ulStoreRegionIdx ] );
		}

		/* Calculate task stack MPU region length */
		ulStoreRegionSize  = ( portUInt32Type ) pxStoreTCB->pxStackInUseMarker;
		ulStoreRegionSize -= ( portUInt32Type ) pxStoreTCB->pcStackBaseAddress;
		ulStoreRegionSize += sizeof( portStackType );

		/* Setup task stack MPU region memory entries */
		vPortMPUEncodeTCBRegion( &pxStoreTCB->axMPURegions[ portmpuCONFIGURABLE_REGION_NUM ],
								 portmpuSTACK_REGION,
								 ( portUInt32Type ) pxStoreTCB->pcStackBaseAddress,
								 portmpuTASK_STACK_ATTRIBUTES,
								 ulStoreRegionSize,
								 0U );

		/* Setup task stack MPU mirror region memory entries */
		vPortMPUCreateTCBMirrorRegion( &pxStoreTCB->axMPURegions[ portmpuCONFIGURABLE_REGION_NUM ],
									   &pxStoreTCB->axMPURegionsMirror[ portmpuCONFIGURABLE_REGION_NUM ] );
	}
	portEXIT_CRITICAL_WITHIN_API();
}
/*--------------------------------------------------------------------------*/


#ifdef SAFERTOS_MODULE_TEST
	#include "MpuCTestHeaders.h"
	#include "MpuCTest.h"
#endif

