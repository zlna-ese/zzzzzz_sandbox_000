/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

#define MUTEX_C

#define KERNEL_SOURCE_FILE

#include "SafeRTOS.h"
#include "queue.h"
#include "mutex.h"

/*-----------------------------------------------------------*/
/* Constant Definitions.									 */
/*-----------------------------------------------------------*/

/*-----------------------------------------------------------*/
/* Macros													 */
/*-----------------------------------------------------------*/

/* A mutex is a special type of queue. */
#define mutexQUEUE_LENGTH			( ( portUnsignedBaseType ) 1 )
#define mutexQUEUE_ITEM_LENGTH		( ( portUnsignedBaseType ) 0 )


/*-----------------------------------------------------------*/
/* Type Definitions.	 								     */
/*-----------------------------------------------------------*/

/*-----------------------------------------------------------*/
/* Variables.												 */
/*-----------------------------------------------------------*/

/*-----------------------------------------------------------*/
/* Function prototypes.		     						     */
/*-----------------------------------------------------------*/

/* MCDC Test Point: PROTOTYPE */

/*-----------------------------------------------------------*/
/* External declarations      								 */
/*-----------------------------------------------------------*/

/*-----------------------------------------------------------*/
/* Public API functions		     							 */
/*-----------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xMutexCreate( portInt8Type *pcMutexBuffer, xMutexHandleType *pxMutex )
{
portBaseType xReturn = errNULL_PARAMETER_SUPPLIED;
xMutexHandleType xCreatedMutex = NULL;

	/* A critical section is not required as the mutex cannot be used until the
	 * handle is returned to the calling task. */

	/* Check the supplied mutex handle. */
	if( NULL != pxMutex )
	{
		xReturn = xQueueCreate( pcMutexBuffer,
								portQUEUE_OVERHEAD_BYTES,
								mutexQUEUE_LENGTH,
								mutexQUEUE_ITEM_LENGTH,
								&xCreatedMutex );

		if( pdPASS == xReturn )
		{
			/* Initialise the queue elements that are specific to mutex type operation. */
			vQueueInitialiseMutex( xCreatedMutex );

			/* Set the returned mutex handle. */
			*pxMutex = xCreatedMutex;

			/* MCDC Test Point: STD_IF "xMutexCreate" */
		}
		/* MCDC Test Point: ADD_ELSE "xMutexCreate" */
	}
	/* MCDC Test Point: ADD_ELSE "xMutexCreate" */

	return xReturn;
}

#ifdef SAFERTOS_MODULE_TEST
	#include "MutexCTestHeaders.h"
	#include "MutexCTest.h"
#endif /* SAFERTOS_MODULE_TEST */



