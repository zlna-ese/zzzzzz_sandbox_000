/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

#define MPUAPI_C

#define KERNEL_SOURCE_FILE

/* Scheduler Includes */
#include "SafeRTOS_API.h"


/*-----------------------------------------------------------------------------
 * Global Functions
 *---------------------------------------------------------------------------*/

/* Wrapper Functions */
portTickType MPU_xTaskGetTickCount( void );
portTaskHandleType MPU_xTaskGetCurrentTaskHandle( void );

portBaseType MPU_xMPUSetTaskRegions( portTaskHandleType pxSetTaskToModify,
									 const mpuTaskParamType * const pxSetTaskMpuParams );

portBaseType MPU_xMPUConfigureGlobalRegion( portUInt32Type ulRegionNumber,
											portUInt32Type ulRegionBeginAddress,
											portUInt32Type ulRegionAccess,
											portUInt32Type ulRegionSize,
											portUInt32Type ulSubRegionDisable );

portBaseType MPU_xTaskCreate( const xTaskParameters * const pxTaskParameters,
							  portTaskHandleType *pxCreatedTask );

portBaseType MPU_xTaskDelete( portTaskHandleType pxTaskToDelete );
portBaseType MPU_xTaskDelay( portTickType xTicksToDelay );

portBaseType MPU_xTaskDelayUntil( portTickType *pxPreviousWakeTime,
								  portTickType xTimeIncrement );

portBaseType MPU_xTaskPriorityGet( portTaskHandleType pxTask,
								   portUnsignedBaseType *puxPriority );

portBaseType MPU_xTaskPrioritySet( portTaskHandleType pxTask,
								   portUnsignedBaseType uxNewPriority );

portBaseType MPU_xTaskSuspend( portTaskHandleType pxTaskToSuspend );
portBaseType MPU_xTaskResume( portTaskHandleType pxTaskToResume );

portBaseType MPU_xTaskStartScheduler( portBaseType xUseKernelConfigurationChecks );
void		 MPU_vTaskSuspendScheduler( void );
portBaseType MPU_xTaskResumeScheduler( void );
portBaseType MPU_xTaskInitializeScheduler( const xPORT_INIT_PARAMETERS * const pxPortInitParameters );
portBaseType MPU_xTaskIsSchedulerStarted( void );

portBaseType MPU_xTaskNotifyWait( portUnsignedBaseType uxBitsToClearOnEntry,
								  portUnsignedBaseType uxBitsToClearOnExit,
								  portUnsignedBaseType *puxNotificationValue,
								  portTickType xTicksToWait );

portBaseType MPU_xTaskNotifySend( portTaskHandleType xTaskToNotify,
								  portBaseType xAction,
								  portUnsignedBaseType uxValue );

void *MPU_pvTaskTLSObjectGet( void );

portBaseType MPU_xQueueCreate( portInt8Type *pcQueueMemory,
							   portUnsignedBaseType uxBufferLength,
							   portUnsignedBaseType uxQueueLength,
							   portUnsignedBaseType uxItemSize,
							   xQueueHandle *pxQueue );

portBaseType MPU_xQueueSend( xQueueHandle xQueue,
							 const void * const pvItemToQueue,
							 portTickType xTicksToWait );

portBaseType MPU_xQueueSendToFront( xQueueHandle xQueue,
									const void * const pvItemToQueue,
									portTickType xTicksToWait );

portBaseType MPU_xQueueReceive( xQueueHandle xQueue,
								void * const pvBuffer,
								portTickType xTicksToWait );

portBaseType MPU_xQueuePeek( xQueueHandle xQueue,
							 void * const pvBuffer,
							 portTickType xTicksToWait );

portBaseType MPU_xQueueMessagesWaiting( const xQueueHandle xQueue,
										portUnsignedBaseType *puxMessagesWaiting );

portBaseType MPU_xQueueGiveMutex( xQueueHandle xMutex );

portBaseType MPU_xQueueTakeMutex( xQueueHandle xMutex,
								  portTickType xTicksToWait );

portBaseType MPU_xCalculateCPUUsage( portTaskHandleType xHandle,
									 xPERCENTAGES* const pxPercentages );

portBaseType MPU_xSemaphoreCreateBinary( portInt8Type *pcSemaphoreBuffer,
										 xSemaphoreHandle *pxSemaphore );

portBaseType MPU_xSemaphoreCreateCounting( portUnsignedBaseType uxMaxCount,
										   portUnsignedBaseType uxInitialCount,
										   portInt8Type *pcSemaphoreBuffer,
										   xSemaphoreHandle *pxSemaphore );

portBaseType MPU_xMutexCreate( portInt8Type *pcMutexBuffer, xMutexHandleType *pxMutex );

portBaseType MPU_xTimerCreate( const timerInitParametersType * const pxTimerParameters, timerHandleType *pxCreatedTimer );

portBaseType MPU_xTimerStart( timerHandleType xTimer, portTickType xBlockTime );
portBaseType MPU_xTimerReset( timerHandleType xTimer, portTickType xBlockTime );
portBaseType MPU_xTimerStop( timerHandleType xTimer, portTickType xBlockTime );
portBaseType MPU_xTimerChangePeriod( timerHandleType xTimer,
									 portTickType xNewPeriodInTicks,
									 portTickType xBlockTime );
portBaseType MPU_xTimerDelete( timerHandleType xTimer, portTickType xBlockTime );

portBaseType MPU_xTimerGetTimerID( timerHandleType xTimer,
								   portBaseType *pxTimerID );

portBaseType MPU_xTimerIsTimerActive( timerHandleType xTimer );

void *MPU_pvTimerTLSObjectGet( timerHandleType xTimer );

portBaseType MPU_xEventGroupCreate( eventGroupType *pxEventGroup,
									eventGroupHandleType *pxEventGroupHandle );
portBaseType MPU_xEventGroupGetBits( eventGroupHandleType xEventGroupHandle,
									 eventBitsType *pxEventBitsSet );
portBaseType MPU_xEventGroupSetBits( eventGroupHandleType xEventGroupHandle,
									 const eventBitsType xBitsToSet );
portBaseType MPU_xEventGroupClearBits( eventGroupHandleType xEventGroupHandle,
									   const eventBitsType xBitsToClear );
portBaseType MPU_xEventGroupWaitBits( eventGroupHandleType xEventGroupHandle,
									  const eventBitsType xBitsToWaitFor,
									  const portBaseType xClearOnExit,
									  const portBaseType xWaitForAllBits,
									  eventBitsType *pxEventBitsSet,
									  portTickType xTicksToWait );
portBaseType MPU_xEventGroupDelete( eventGroupHandleType xEventGroupHandle );

/* MCDC Test Point: PROTOTYPE */

/*---------------------------------------------------------------------------*/

portBaseType MPU_xMPUSetTaskRegions( portTaskHandleType pxSetTaskToModify,
									 const mpuTaskParamType * const pxSetTaskMpuParams )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xMPUSetTaskRegions( pxSetTaskToModify, pxSetTaskMpuParams );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xMPUSetTaskRegions" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xMPUConfigureGlobalRegion( portUInt32Type ulRegionNumber,
											portUInt32Type ulRegionBeginAddress,
											portUInt32Type ulRegionAccess,
											portUInt32Type ulRegionSize,
											portUInt32Type ulSubRegionDisable )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xMPUConfigureGlobalRegion( ulRegionNumber, ulRegionBeginAddress,
										 ulRegionAccess, ulRegionSize, ulSubRegionDisable );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xMPUConfigureGlobalRegion" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTaskCreate( const xTaskParameters * const pxTaskParameters,
							  portTaskHandleType *pxCreatedTask )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTaskCreate( pxTaskParameters, pxCreatedTask );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTaskCreate" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTaskDelete( portTaskHandleType pxTaskToDelete )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTaskDelete( pxTaskToDelete );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTaskDelete" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTaskDelay( portTickType xTicksToDelay )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTaskDelay( xTicksToDelay );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTaskDelay" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTaskDelayUntil( portTickType *pxPreviousWakeTime,
								  portTickType xTimeIncrement )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTaskDelayUntil( pxPreviousWakeTime, xTimeIncrement );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTaskDelayUntil" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTaskPriorityGet( portTaskHandleType pxTask,
								   portUnsignedBaseType *puxPriority )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTaskPriorityGet( pxTask, puxPriority );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTaskPriorityGet" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTaskPrioritySet( portTaskHandleType pxTask,
								   portUnsignedBaseType uxNewPriority )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTaskPrioritySet( pxTask, uxNewPriority );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTaskPrioritySet" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTaskSuspend( portTaskHandleType pxTaskToSuspend )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTaskSuspend( pxTaskToSuspend );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTaskSuspend" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTaskResume( portTaskHandleType pxTaskToResume )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTaskResume( pxTaskToResume );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTaskResume" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTaskStartScheduler( portBaseType xUseKernelConfigurationChecks )
{
portBaseType xReturn;

	/* xTaskStartScheduler() should only be called from Privilege Mode. */
	if( pdFALSE == xPortIsPrivilegedMode() )
	{
		/* MCDC Test Point: STD_IF "MPU_xTaskStartScheduler" */

		xReturn = errEXECUTING_IN_UNPRIVILEGED_MODE;
	}
	else
	{
		/* MCDC Test Point: STD_ELSE "MPU_xTaskStartScheduler" */

		xReturn = xTaskStartScheduler( xUseKernelConfigurationChecks );
	}

	return xReturn;
}
/*---------------------------------------------------------------------------*/

void MPU_vTaskSuspendScheduler( void )
{
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	vTaskSuspendScheduler();

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_vTaskSuspendScheduler" */
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTaskResumeScheduler( void )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTaskResumeScheduler();

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTaskResumeScheduler" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portTickType MPU_xTaskGetTickCount( void )
{
portBaseType xRunningPrivileged;
portTickType xReturn;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTaskGetTickCount();

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTaskGetTickCount" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTaskInitializeScheduler( const xPORT_INIT_PARAMETERS* const pxPortInitParameters )
{
portBaseType xReturn;

	/* xTaskInitializeScheduler() should only be called from Privilege Mode. */
	if( pdFALSE == xPortIsPrivilegedMode() )
	{
		/* MCDC Test Point: STD_IF "MPU_xTaskInitializeScheduler" */

		xReturn = errEXECUTING_IN_UNPRIVILEGED_MODE;
	}
	else
	{
		/* MCDC Test Point: STD_ELSE "MPU_xTaskInitializeScheduler" */

		xReturn = xTaskInitializeScheduler( pxPortInitParameters );
	}

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTaskNotifyWait( portUnsignedBaseType uxBitsToClearOnEntry,
								portUnsignedBaseType uxBitsToClearOnExit,
								portUnsignedBaseType *puxNotificationValue,
								portTickType xTicksToWait )
{
portBaseType xRunningPrivileged;
portBaseType xReturn;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTaskNotifyWait( uxBitsToClearOnEntry, uxBitsToClearOnExit, puxNotificationValue, xTicksToWait );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTaskNotifyWait" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTaskNotifySend( portTaskHandleType xTaskToNotify,
								  portBaseType xAction,
								  portUnsignedBaseType uxValue )
{
portBaseType xRunningPrivileged;
portBaseType xReturn;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTaskNotifySend( xTaskToNotify, xAction, uxValue );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTaskNotifySend" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portTaskHandleType MPU_xTaskGetCurrentTaskHandle( void )
{
portBaseType xRunningPrivileged;
portTaskHandleType xReturn;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTaskGetCurrentTaskHandle();

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTaskGetCurrentTaskHandle" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTaskIsSchedulerStarted( void )
{
portBaseType xRunningPrivileged;
portBaseType xReturn;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTaskIsSchedulerStarted();

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTaskIsSchedulerStarted" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

void *MPU_pvTaskTLSObjectGet( void )
{
void *pvReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	pvReturn = pvTaskTLSObjectGet( );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_pvTaskTLSObjectGet" */

	return pvReturn;
}

/*---------------------------------------------------------------------------*/

portBaseType MPU_xQueueCreate( portInt8Type *pcQueueMemory,
							   portUnsignedBaseType uxBufferLength,
							   portUnsignedBaseType uxQueueLength,
							   portUnsignedBaseType uxItemSize,
							   xQueueHandle *pxQueue )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xQueueCreate( pcQueueMemory, uxBufferLength,
							uxQueueLength, uxItemSize, pxQueue );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xQueueCreate" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xQueueSend( xQueueHandle xQueue,
							 const void * const pvItemToQueue,
							 portTickType xTicksToWait )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xQueueSend( xQueue, pvItemToQueue, xTicksToWait );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xQueueSend" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xQueueSendToFront( xQueueHandle xQueue,
									const void * const pvItemToQueue,
									portTickType xTicksToWait )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xQueueSendToFront( xQueue, pvItemToQueue, xTicksToWait );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xQueueSendToFront" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xQueueReceive( xQueueHandle xQueue,
								void * const pvBuffer,
								portTickType xTicksToWait )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xQueueReceive( xQueue, pvBuffer, xTicksToWait );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xQueueReceive" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xQueuePeek( xQueueHandle xQueue,
							 void * const pvBuffer,
							 portTickType xTicksToWait )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xQueuePeek( xQueue, pvBuffer, xTicksToWait );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xQueuePeek" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xQueueMessagesWaiting( const xQueueHandle xQueue,
										portUnsignedBaseType *puxMessagesWaiting )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xQueueMessagesWaiting( xQueue, puxMessagesWaiting );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xQueueMessagesWaiting" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xQueueGiveMutex( xQueueHandle xMutex )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xQueueGiveMutex( xMutex );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xQueueGiveMutex" */

	return xReturn;
}
/*--------------------------------------------------------------------------*/

portBaseType MPU_xQueueTakeMutex( xQueueHandle xMutex,
								  portTickType xTicksToWait )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xQueueTakeMutex( xMutex, xTicksToWait );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xQueueTakeMutex" */

	return xReturn;
}
/*--------------------------------------------------------------------------*/

portBaseType MPU_xCalculateCPUUsage( portTaskHandleType xHandle,
									 xPERCENTAGES * const pxPercentages )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xCalculateCPUUsage( xHandle, pxPercentages );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xCalculateCPUUsage" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xSemaphoreCreateBinary( portInt8Type *pcSemaphoreBuffer,
										 xSemaphoreHandle *pxSemaphore )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xSemaphoreCreateBinary( pcSemaphoreBuffer, pxSemaphore );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xSemaphoreCreateBinary" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xSemaphoreCreateCounting( portUnsignedBaseType uxMaxCount,
										   portUnsignedBaseType uxInitialCount,
										   portInt8Type *pcSemaphoreBuffer,
										   xSemaphoreHandle *pxSemaphore )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xSemaphoreCreateCounting( uxMaxCount, uxInitialCount,
										pcSemaphoreBuffer, pxSemaphore );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xSemaphoreCreateCounting" */

	return xReturn;
}
/*--------------------------------------------------------------------------*/

portBaseType MPU_xMutexCreate( portInt8Type *pcMutexBuffer, xMutexHandleType *pxMutex )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xMutexCreate( pcMutexBuffer, pxMutex );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xMutexCreate" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTimerCreate( const timerInitParametersType * const pxTimerParameters, timerHandleType *pxCreatedTimer )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTimerCreate( pxTimerParameters, pxCreatedTimer );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTimerCreate" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTimerStart( timerHandleType xTimer, portTickType xBlockTime )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTimerStart( xTimer, xBlockTime );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTimerStart" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTimerReset( timerHandleType xTimer, portTickType xBlockTime )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTimerReset( xTimer, xBlockTime );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTimerReset" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTimerStop( timerHandleType xTimer, portTickType xBlockTime )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTimerStop( xTimer, xBlockTime );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTimerStop" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTimerChangePeriod( timerHandleType xTimer,
									 portTickType xNewPeriodInTicks,
									 portTickType xBlockTime )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTimerChangePeriod( xTimer, xNewPeriodInTicks, xBlockTime );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTimerChangePeriod" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/


portBaseType MPU_xTimerDelete( timerHandleType xTimer, portTickType xBlockTime )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTimerDelete( xTimer, xBlockTime );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTimerDelete" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTimerIsTimerActive( timerHandleType xTimer )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTimerIsTimerActive( xTimer );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTimerIsTimerActive" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xTimerGetTimerID( timerHandleType xTimer,
								   portBaseType *pxTimerID )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xTimerGetTimerID( xTimer, pxTimerID );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xTimerGetTimerID" */

	return xReturn;
}

/*---------------------------------------------------------------------------*/

void *MPU_pvTimerTLSObjectGet( timerHandleType xTimer )
{
void *pvReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	pvReturn = pvTimerTLSObjectGet( xTimer );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_pvTimerTLSObjectGet" */

	return pvReturn;
}

/*---------------------------------------------------------------------------*/

portBaseType MPU_xEventGroupCreate( eventGroupType *pxEventGroup,
									eventGroupHandleType *pxEventGroupHandle )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xEventGroupCreate( pxEventGroup, pxEventGroupHandle );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xEventGroupCreate" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xEventGroupGetBits( eventGroupHandleType xEventGroupHandle,
									 eventBitsType *pxEventBitsSet )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xEventGroupGetBits( xEventGroupHandle, pxEventBitsSet );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xEventGroupGetBits" */

	return xReturn;
}

/*---------------------------------------------------------------------------*/

portBaseType MPU_xEventGroupSetBits( eventGroupHandleType xEventGroupHandle,
									 const eventBitsType xBitsToSet )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xEventGroupSetBits( xEventGroupHandle, xBitsToSet );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xEventGroupSetBits" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xEventGroupClearBits( eventGroupHandleType xEventGroupHandle,
									   const eventBitsType xBitsToClear )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xEventGroupClearBits( xEventGroupHandle, xBitsToClear );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xEventGroupClearBits" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xEventGroupWaitBits( eventGroupHandleType xEventGroupHandle,
									  const eventBitsType xBitsToWaitFor,
									  const portBaseType xClearOnExit,
									  const portBaseType xWaitForAllBits,
									  eventBitsType *pxEventBitsSet,
									  portTickType xTicksToWait )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xEventGroupWaitBits( xEventGroupHandle, xBitsToWaitFor,
								   xClearOnExit, xWaitForAllBits,
								   pxEventBitsSet, xTicksToWait );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xEventGroupWaitBits" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

portBaseType MPU_xEventGroupDelete( eventGroupHandleType xEventGroupHandle )
{
portBaseType xReturn;
portBaseType xRunningPrivileged;

	xRunningPrivileged = portRAISE_PRIVILEGE( );

	xReturn = xEventGroupDelete( xEventGroupHandle );

	/* Restore original privilege level. */
	portRESTORE_PRIVILEGE( xRunningPrivileged );

	/* MCDC Test Point: STD "MPU_xEventGroupDelete" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

void vMPUTaskExecuteInUnprivilegedMode( void )
{
	/* MCDC Test Point: STD "vMPUTaskExecuteInUnprivilegedMode" */

	/* Lower privilege level. */
	portLOWER_PRIVILEGE();
}
/*---------------------------------------------------------------------------*/


#ifdef SAFERTOS_MODULE_TEST
	#include "MpuAPICTestHeaders.h"
	#include "MpuAPICTest.h"
#endif
