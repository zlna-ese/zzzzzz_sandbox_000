/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

#define KERNEL_SOURCE_FILE

/* Scheduler includes */
#include "SafeRTOS.h"
#include "task.h"


/*-----------------------------------------------------------------------------
 * Local Constants
 *---------------------------------------------------------------------------*/

/* Region Enable definition. */
#define portmpuREGION_ENABLE				( 0x01UL )

/* MPU Region size configuration */
#define portmpuSMALLEST_REGION_SIZE_SETTING	( 4UL )
#define portmpuLARGEST_REGION_SIZE_SETTING	( 31UL )

/* Sub-Region Disable mask definition. */
#define portmpuREGION_DISABLE_MASK	(		\
	portmpuREGION_1ST_SUB_REGION_DISABLE |	\
	portmpuREGION_2ND_SUB_REGION_DISABLE |	\
	portmpuREGION_3RD_SUB_REGION_DISABLE |	\
	portmpuREGION_4TH_SUB_REGION_DISABLE |	\
	portmpuREGION_5TH_SUB_REGION_DISABLE |	\
	portmpuREGION_6TH_SUB_REGION_DISABLE |	\
	portmpuREGION_7TH_SUB_REGION_DISABLE |	\
	portmpuREGION_8TH_SUB_REGION_DISABLE )

/* Masks used to ensure that only allowable bits in the MPU registers are
 * written to. */
#define portmpuMPU_REG_ADDRESS_MASK		( 0xFFFFFFE0UL )
#define portmpuMPU_REG_ACCESS_MASK		( 0x0000173FUL )
#define portmpuMPU_REG_SIZE_MASK		( 0x0000FF3FUL )


/*-----------------------------------------------------------------------------
 * External Prototypes
 *---------------------------------------------------------------------------*/

/* Load given MPU region settings - Implemented in portable Assembler file. */
extern void vPortMPULoadRegion( portUInt32Type ulRegionIdent,
								portUInt32Type ulRegionAddr,
								portUInt32Type ulRegionAttr,
								portUInt32Type ulRegionSize );


/*-----------------------------------------------------------------------------
 * Local Prototypes
 *---------------------------------------------------------------------------*/

KERNEL_FUNCTION static portUInt32Type prvGetRegionSizeSetting( portUInt32Type ulRegionSizeBytes );

/* MCDC Test Point: PROTOTYPE */

/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION void vPortMPUConfigureSystemRegion( portUInt32Type ulRegionIdentifier,
													portUInt32Type ulRegionBeginAddress,
													portUInt32Type ulRegionAccess,
													portUInt32Type ulRegionSize,
													portUInt32Type ulSubRegionDisable)
{
portUInt32Type ulSystemRegionSize;

portUnsignedBaseType uxNumberOfMPURegions = uxPortMPUGetNumberRegions();
portUInt32Type ulGlobalRegionNumberLast = 0UL;		/* use a local to avoid lint warning about side effects. */
portUInt32Type ulRegionNumber = 0UL;
portBaseType xValidRegion;

	/* Only configure a region if the number of regions is valid */
	if( uxNumberOfMPURegions >= portmpuMINIMUM_SUPPORTED_REGIONS )
	{
		/* Use a local to avoid lint warning about side effects. */
		ulGlobalRegionNumberLast = portmpuGLOBAL_CONFIGURABLE_REGION_LAST;

		/* The unprivileged Flash region is always the lowest priority */
		if( portmpuUNPRIVILEGED_FLASH_REGION_LOOKUP == ulRegionIdentifier )
		{
			ulRegionNumber = portmpuUNPRIVILEGED_FLASH_REGION;
			xValidRegion = pdTRUE;
			/* MCDC Test Point: STD_IF "vPortMPUConfigureSystemRegion" */
		}
		/* The kernel Flash region is always the second highest priority */
		else if( portmpuKERNEL_FLASH_REGION_LOOKUP == ulRegionIdentifier )
		{
			ulRegionNumber = uxNumberOfMPURegions - 2UL;
			xValidRegion = pdTRUE;
			/* MCDC Test Point: STD_ELSE_IF "vPortMPUConfigureSystemRegion" */
		}
		/* The kernel RAM region is always the highest priority */
		else if( portmpuKERNEL_RAM_REGION_LOOKUP == ulRegionIdentifier )
		{
			ulRegionNumber = uxNumberOfMPURegions - 1UL;
			xValidRegion = pdTRUE;
			/* MCDC Test Point: STD_ELSE_IF "vPortMPUConfigureSystemRegion" */
		}
		/* If its a global region */
		else if( ( ulRegionIdentifier >= portmpuGLOBAL_CONFIGURABLE_REGION_FIRST ) && 
				 ( ulRegionIdentifier <= ulGlobalRegionNumberLast  ) )
		{
			ulRegionNumber = ulRegionIdentifier; 
			xValidRegion = pdTRUE;
			/* MCDC Test Point: NULL "vPortMPUConfigureSystemRegion" */
		}
		else
		{
			xValidRegion = pdFALSE;
			/* MCDC Test Point: STD_ELSE "vPortMPUConfigureSystemRegion" */
		}
		
		/* MCDC Test Point: EXP_IF_AND "vPortMPUConfigureSystemRegion" "( ulRegionIdentifier >= portmpuGLOBAL_CONFIGURABLE_REGION_FIRST )" "( ulRegionIdentifier <= portmpuGLOBAL_CONFIGURABLE_REGION_LAST )" "Deviate: FF" */

		if( pdFALSE != xValidRegion )
		{
			/* Compile MPU System region size parameter. */
			ulSystemRegionSize = prvGetRegionSizeSetting( ulRegionSize );
			ulSystemRegionSize |= ulSubRegionDisable;
			ulSystemRegionSize |= portmpuREGION_ENABLE;

			/* Load MPU System region parameters into MPU Sub-Processor. */
			vPortMPULoadRegion( ulRegionNumber,
								( ulRegionBeginAddress & portmpuMPU_REG_ADDRESS_MASK ),
								( ulRegionAccess & portmpuMPU_REG_ACCESS_MASK ),
								( ulSystemRegionSize & portmpuMPU_REG_SIZE_MASK ) );

			/* MCDC Test Point: STD_IF "vPortMPUConfigureSystemRegion" */
		}
		/* MCDC Test Point: ADD_ELSE "vPortMPUConfigureSystemRegion" */

	}
	/* MCDC Test Point: ADD_ELSE "vPortMPUConfigureSystemRegion" */
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION void vPortMPUEncodeTCBRegion( portmpuRegionRegistersType *pxPortMPURegion,
											portUInt32Type ulRegionIdentifier,
											portUInt32Type ulBaseAddress,
											portUInt32Type ulAccessPermissions,
											portUInt32Type ulLengthInBytes,
											portUInt32Type ulSubRegionDisable )
{
portUInt32Type ulTaskRegionSize = 0UL;

	/* ulRegionIdentifier is not used in this port. */
	( void ) ulRegionIdentifier;

	/* Task region NULL entry? */
	if( ulLengthInBytes > 0UL )
	{
		/* Calculate task region Size parameter. */
		ulTaskRegionSize = prvGetRegionSizeSetting( ulLengthInBytes );
		ulTaskRegionSize |= ( ulSubRegionDisable & portmpuREGION_DISABLE_MASK );
		ulTaskRegionSize |= portmpuREGION_ENABLE;

		/* MCDC Test Point: STD_IF "vPortMPUEncodeTCBRegion" */
	}
	/* MCDC Test Point: ADD_ELSE "vPortMPUEncodeTCBRegion" */

	/* Set task region parameter values. */
	pxPortMPURegion->ulRegionBaseAddress = ulBaseAddress & portmpuMPU_REG_ADDRESS_MASK;
	pxPortMPURegion->ulRegionAttribute = ulAccessPermissions & portmpuMPU_REG_ACCESS_MASK;
	pxPortMPURegion->ulRegionSize = ulTaskRegionSize & portmpuMPU_REG_SIZE_MASK;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION void vPortMPUCreateTCBMirrorRegion( const portmpuRegionRegistersType *pxPortMPURegion,
													portmpuRegionRegistersType *pxPortMPUMirror )
{
	pxPortMPUMirror->ulRegionBaseAddress = ~( pxPortMPURegion->ulRegionBaseAddress );
	pxPortMPUMirror->ulRegionAttribute = ~( pxPortMPURegion->ulRegionAttribute );
	pxPortMPUMirror->ulRegionSize = ~( pxPortMPURegion->ulRegionSize );

	/* MCDC Test Point: STD "vPortMPUCreateTCBMirrorRegion" */
}
/*---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 * Filescope Functions
 *---------------------------------------------------------------------------*/

KERNEL_FUNCTION static portUInt32Type prvGetRegionSizeSetting( portUInt32Type ulRegionSizeBytes )
{
portBaseType xSizeExec = pdTRUE;
portUInt32Type ulSizeValue = portmpuSMALLEST_REGION_SIZE_ACTUAL;
portUInt32Type ulSizeSetting = portmpuSMALLEST_REGION_SIZE_SETTING;

	/* Smallest valid MPU region size setting is 32. */
	/* Largest valid MPU region size setting is 1 shifted by 31. */

	/* While MPU region size setting is smaller than given byte size... */
	do
	{
		/* User region size setting reached? */
		if( ulRegionSizeBytes <= ulSizeValue )
		{
			/* Yes, break loop & exit */
			xSizeExec = pdFALSE;
			/* MCDC Test Point: STD_IF "prvGetRegionSizeSetting" */
		}
		else
		{
			/* No, shift byte size compare mask. */
			ulSizeValue <<= 1U;

			/* Now increment region size setting. */
			ulSizeSetting++;

			/* Maximum region size setting reached? */
			if( ulSizeSetting >= portmpuLARGEST_REGION_SIZE_SETTING )
			{
				/* Yes, break loop & exit. */
				xSizeExec = pdFALSE;
				/* MCDC Test Point: STD_IF "prvGetRegionSizeSetting" */
			}
			/* MCDC Test Point: ADD_ELSE "prvGetRegionSizeSetting" */

			/* MCDC Test Point: STD_ELSE "prvGetRegionSizeSetting" */
		}

		/* MCDC Test Point: WHILE_INTERNAL "prvGetRegionSizeSetting" "( pdFALSE != xSizeExec )" */
	} while( pdFALSE != xSizeExec );

	/* Shift the code by one before returning so it can be written directly
	 * into the the correct bit position of the attribute register. */
	ulSizeSetting <<= 1U;

	return ulSizeSetting;
}
/*---------------------------------------------------------------------------*/


#ifdef SAFERTOS_MODULE_TEST
	#include "PortMpuCTestHeaders.h"
	#include "PortMpuCTest.h"
#endif
