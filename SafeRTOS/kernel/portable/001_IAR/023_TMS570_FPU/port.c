/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

/*-----------------------------------------------------------------------------
 * Implementation of functions defined in portable.h
 * for the Cortex-R TMS570 FPU port.
 *---------------------------------------------------------------------------*/

#define KERNEL_SOURCE_FILE

/* Scheduler Includes */
#include "SafeRTOS.h"
#include "task.h"
#include "mpuAPI.h"

/*-----------------------------------------------------------------------------
 * Constant Definitions.
 *---------------------------------------------------------------------------*/

/* Allowed ranges for clock and tick rate. */
#define portCPU_CLOCK_HZ_MIN				( 1000000U )
#define portTICK_RATE_HZ_MAX				( 20000U )

/* Constants required to set up the initial stack. */
#define portINSTRUCTION_SIZE				( ( portStackType ) 0x04UL )
#define portNO_CRITICAL_SECTION_NESTING		( ( portStackType ) 0x00UL )
#define portTHUMB_MODE_BIT					( ( portStackType ) 0x20UL )

/* Use system mode for general privilege mode tasks. */
#define portPRIVILEGED_MODE_SETTING			( 0x1FUL )
#define portUNPRIVILEGED_MODE_SETTING		( 0x10UL )

/* CPSR needs Endianness bit set. Also start with the FIQ interrupt disabled.
 * The application can enable the FIQ as desired. */
#define portCPSR_INITIAL_VALUE				( 0x00000240UL )

/* SafeRTOS inspects the vector table to ensure
 * the necessary handlers have been installed. */
#define portVIM_TABLE_ADDRESS				( ( volatile portUInt32Type * ) 0xFFF82000UL )

/* Base addresses of VIM registers. */
#define portVIM_CHANCTRL_BASE				( ( volatile portUInt32Type * ) 0xFFFFFE80UL )
#define portVIM_REQENASET_BASE				( ( volatile portUInt32Type * ) 0xFFFFFE30UL )

/* Interrupt Request ID for the System Software Interrupt. */
#define portVIM_SYS_SOFTWARE_INTR_REQ		( 21U )


/*
 * RTI register and bit definitions, required by the timer setup code.
 */

#define portRTIGCTRL		( *( ( volatile portUInt32Type * ) 0xFFFFFC00UL ) )
#define portRTITBCTRL		( *( ( volatile portUInt32Type * ) 0xFFFFFC04UL ) )
#define portRTICOMPCTRL		( *( ( volatile portUInt32Type * ) 0xFFFFFC0CUL ) )
/* Free running counter */
#define portRTIFRC0			( *( ( volatile portUInt32Type * ) 0xFFFFFC10UL ) )
/* up counter */
#define portRTIUC0			( *( ( volatile portUInt32Type * ) 0xFFFFFC14UL ) )
/* Compare up counter */
#define portRTICPUC0		( *( ( volatile portUInt32Type * ) 0xFFFFFC18UL ) )
/* Compare 0 register */
#define portRTICOMP0		( *( ( volatile portUInt32Type * ) 0xFFFFFC50UL ) )
/* Update Compare 0 */
#define portRTIUDCP0		( *( ( volatile portUInt32Type * ) 0xFFFFFC54UL ) )
/* Set Interrupt Enable */
#define portRTISETINT		( *( ( volatile portUInt32Type * ) 0xFFFFFC80UL ) )
#define portRTIINTFLAG		( *( ( volatile portUInt32Type * ) 0xFFFFFC88UL ) )

#define portRTIGCTRL_ENABLE_BIT						( 0x00000001UL )
#define portRTITBCTRL_USE_INTERNAL_COUNTER			( 0x00000000UL )
#define portRTICPUC0_PRESCALE_DIV_2					( 1UL )
#define portRTISETINT_ENABLE_TIMER0_COMPARE_INT_0	( 0x00000001UL )
#define portRTICOMPCTRL_COMPSEL0_RTIFRC0			( 0x00000000UL )

/* VIM Interrupt Enable */
#define portVIM_REQMASKSET0		( *( ( volatile portUInt32Type * ) 0xFFFFFE30UL ) )

/*-----------------------------------------------------------------------------
 * Exception Handlers
 *---------------------------------------------------------------------------*/

KERNEL_FUNCTION __irq __arm void vSafeRTOSSysTickHandler( void );


/*-----------------------------------------------------------------------------
 * Functions accessed from portasm.asm
 *---------------------------------------------------------------------------*/

KERNEL_FUNCTION void vPortControlCheckFailed( void );
KERNEL_FUNCTION void vPortSvcHookCheckFailed( void );
KERNEL_FUNCTION void vPortSSIHookCheckFailed( void );

/*-----------------------------------------------------------------------------
 * External Functions
 *---------------------------------------------------------------------------*/

/* Enabling the VFP is performed in portasm.asm */
extern void vPortEnableVFP( void );

/* Enabling the Cache is performed in portasm.s */
extern void vPortCacheEnable( void );

extern void vSafeRTOSSysSoftwareHandler( void );

extern void vInitialiseReadOnlyUserPidRegister( void );

/*-----------------------------------------------------------------------------
 * Local Prototypes
 *---------------------------------------------------------------------------*/

/* Configures the TCB within the host application provided buffer. */
KERNEL_FUNCTION static xTCB *prvSetupTCB( const xTaskParameters * const pxTaskParameters );

/* Ensure that the parameters passed by the application into the port layer are valid. */
KERNEL_FUNCTION static portBaseType prvCheckKernelConfiguration( void );

/* Setup the timer to generate the tick interrupts. This is a separate function
 * so it can be tested in isolation. */
KERNEL_FUNCTION static void prvSetupTimerInterrupt( portUInt32Type ulClockHz, portUInt32Type ulRateHz );

/* Scan the VIM Control registers for a unique occurrence of a given interrupt
 * request, and return the channel ID. */
KERNEL_FUNCTION static portBaseType prvGetUniqueInterruptChannel( portUnsignedBaseType uxIntrRequest, portUnsignedBaseType *puxIntrChannel );

/* MCDC Test Point: PROTOTYPE */


/*-----------------------------------------------------------------------------
 * Variables
 *---------------------------------------------------------------------------*/

/* As this port is potentially able to be in ROM the user must pass in the
 * system timing parameters (normally constants from a config header file
 * are used. These are stored in the following two variables ready for when
 * the scheduler is started. */
KERNEL_DATA static portUInt32Type ulCPUClockHz = 0UL;
KERNEL_DATA static portUInt32Type ulTickRateHz = 0UL;

/* Each task maintains its own interrupt status in the critical nesting
 * variable. This will get set to zero when the first task starts.
 * Note that this is global scope because it is accessed from portasm.s. */
KERNEL_DATA volatile portUInt32Type ulCriticalNesting = 0UL;

/* Hook functions passed in by the application software.
 * uxSetupTickInterruptHookFunction, pxTaskDeleteHookFunction,
 * pxIdleHookFunction, pxTickHookFunction and pvSvcHookFunction can be passed
 * in as NULL with no adverse effect. */
KERNEL_DATA static portTASK_DELETE_HOOK pxTaskDeleteHookFunction = NULL;
KERNEL_DATA static portERROR_HOOK pxErrorHookFunction = NULL;
KERNEL_DATA static portIDLE_HOOK pxIdleHookFunction = NULL;
KERNEL_DATA static portTICK_HOOK pxTickHookFunction = NULL;
KERNEL_DATA static portSETUP_TICK_HOOK pxSetupTickInterruptHookFunction = NULL;
KERNEL_DATA portSVC_HOOK pxSvcHookFunction = NULL;
KERNEL_DATA portSSI_HOOK pxSSIHookFunction = NULL;

/* Hook Mirrors. */
KERNEL_DATA static portUnsignedBaseType uxTaskDeleteHookMirror = 0UL;
KERNEL_DATA static portUnsignedBaseType uxErrorHookMirror = 0UL;
KERNEL_DATA static portUnsignedBaseType uxIdleHookMirror = 0UL;
KERNEL_DATA static portUnsignedBaseType uxTickHookMirror = 0UL;
KERNEL_DATA static portUnsignedBaseType uxSetupTickInterruptHookMirror = 0UL;
KERNEL_DATA portUnsignedBaseType uxSvcHookMirror = 0UL;
KERNEL_DATA portUnsignedBaseType uxSSIHookMirror = 0UL;

/* Flag controlling the idle hook being executed in unprivileged mode */
KERNEL_DATA static portBaseType xIdleHookRunsPrivileged = pdFALSE;

/* Flag controlling if the processor cache should be enabled */
KERNEL_DATA static portBaseType xEnableProcessorCache = pdFALSE;

/* The application defines whether any additional space should be reserved on
 * each task stack. */
KERNEL_DATA static portUnsignedBaseType uxAdditionalStackCheckMarginBytes = 0UL;

/* Declare a TCB for the idle task. */
KERNEL_DATA static xTCB xIdleTaskTCB = { 0 };

/* The Portable Layer is responsible for the idle task parameters. */
KERNEL_DATA static xTaskParameters xIdleTaskParameters = { 0 };

/* Default parameters to be used for kernel tasks. */
static const mpuTaskParamType xKernelTaskMPUParams =
{
	mpuPRIVILEGED_TASK,
	{
		{ NULL, 0UL, 0UL, 0UL },
		{ NULL, 0UL, 0UL, 0UL },
		{ NULL, 0UL, 0UL, 0UL },
		{ NULL, 0UL, 0UL, 0UL },
		{ NULL, 0UL, 0UL, 0UL },
		{ NULL, 0UL, 0UL, 0UL }
	}
};


/*-----------------------------------------------------------------------------
 * Public function definitions
 *---------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xPortInitialize( const xPORT_INIT_PARAMETERS * const pxInitParameters )
{
portBaseType xReturn;
portUnsignedBaseType uxRegionIdx;

	/* Ensure a NULL pointer hasn't been supplied. */
	if( NULL == pxInitParameters )
	{
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_IF "xPortInitialize" */
	}
	else
	{
		xReturn = pdPASS;

		ulCPUClockHz = pxInitParameters->ulCPUClockHz;
		ulTickRateHz = pxInitParameters->ulTickRateHz;

		ulCriticalNesting = 0xAAAAAAAAUL;

		pxTaskDeleteHookFunction = pxInitParameters->pxTaskDeleteHookFunction;
		pxErrorHookFunction = pxInitParameters->pxErrorHookFunction;
		pxIdleHookFunction = pxInitParameters->pxIdleHookFunction;
		pxTickHookFunction = pxInitParameters->pxTickHookFunction;
		pxSetupTickInterruptHookFunction = pxInitParameters->pxSetupTickInterruptHookFunction;
		pxSvcHookFunction = pxInitParameters->pxSvcHookFunction;
		pxSSIHookFunction = pxInitParameters->pxSSIHookFunction;

		xEnableProcessorCache = pxInitParameters->xEnableCache;

		/* Calculate the mirrors. */
		uxTaskDeleteHookMirror = ~( ( portUnsignedBaseType ) pxInitParameters->pxTaskDeleteHookFunction );
		uxErrorHookMirror = ~( ( portUnsignedBaseType ) pxInitParameters->pxErrorHookFunction );
		uxIdleHookMirror = ~( ( portUnsignedBaseType ) pxInitParameters->pxIdleHookFunction );
		uxTickHookMirror = ~( ( portUnsignedBaseType ) pxInitParameters->pxTickHookFunction );
		uxSetupTickInterruptHookMirror = ~( ( portUnsignedBaseType ) pxInitParameters->pxSetupTickInterruptHookFunction );
		uxSvcHookMirror = ~( ( portUnsignedBaseType ) pxInitParameters->pxSvcHookFunction );
		uxSSIHookMirror = ~( ( portUnsignedBaseType ) pxInitParameters->pxSSIHookFunction );

		/* Each stack has a boundary. Prior to saving the context of a task to
		 * the task stack the kernel will ensure that the boundary is far
		 * enough away that the save process does not move the stack pointer
		 * past the stack boundary.
		 * The user can set uxAdditionalStackCheckMarginBytes to provide a
		 * larger margin - such that the task will will check that there is
		 * enough at least uxAdditionalStackCheckMarginBytes between the stack
		 * pointer and the stack boundary once the task context has been saved.
		 * If there is too little stack available then the error hook function
		 * is called. */
		uxAdditionalStackCheckMarginBytes = pxInitParameters->uxAdditionalStackCheckMarginBytes;

		/* Initialise the Idle task TCB. */
		vPortSetWordAlignedBuffer( &xIdleTaskTCB, 0U, sizeof( xTCB ) );

		/* Initialise Idle task default parameters. */
		xIdleTaskParameters.pvTaskCode = &vIdleTask;
		xIdleTaskParameters.pcTaskName = "IDLE";
		xIdleTaskParameters.pxTCB = &xIdleTaskTCB;
		xIdleTaskParameters.pvParameters = NULL;
		xIdleTaskParameters.uxPriority = taskIDLE_PRIORITY;
		xIdleTaskParameters.xMPUParameters.uxPrivilegeLevel = mpuPRIVILEGED_TASK;	/* The idle task is a privileged task. */

		/* Initialise Idle task configurable parameters. */
		xIdleTaskParameters.pcStackBuffer = pxInitParameters->pcIdleTaskStackBuffer;
		xIdleTaskParameters.uxStackDepthBytes = pxInitParameters->uxIdleTaskStackSizeBytes;
		xIdleTaskParameters.xUsingFPU = pxInitParameters->xIdleTaskUsingFPU;
		for( uxRegionIdx = 0U; uxRegionIdx < portmpuCONFIGURABLE_REGION_NUM; uxRegionIdx++ )
		{
			xIdleTaskParameters.xMPUParameters.axRegions[ uxRegionIdx ] = pxInitParameters->xIdleTaskMPUParameters.axRegions[ uxRegionIdx ];
			/* MCDC Test Point: STD "xPortInitialize" */
		}

		xIdleTaskParameters.pvObject = pxInitParameters->pvIdleTaskTLSObject;
		
		/* The idle task's privilege level selected by the application should
		 * only apply to the idle hook. */
		if( mpuPRIVILEGED_TASK == pxInitParameters->xIdleTaskMPUParameters.uxPrivilegeLevel )
		{
			xIdleHookRunsPrivileged = pdTRUE;
			/* MCDC Test Point: STD_IF "xPortInitialize" */
		}
		else
		{
			xIdleHookRunsPrivileged = pdFALSE;
			/* MCDC Test Point: STD_ELSE "xPortInitialize" */
		}

		/* Set read-only thread and process ID register to null. */
		vInitialiseReadOnlyUserPidRegister();

		/* MCDC Test Point: STD_ELSE "xPortInitialize" */
	}

	return xReturn;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xPortCheckTaskParameters( const xTaskParameters * const pxTaskParameters )
{
portUnsignedBaseType uxValidStackSize = portmpuSMALLEST_REGION_SIZE_ACTUAL;
portUnsignedBaseType uxTaskStackSize = pxTaskParameters->uxStackDepthBytes;
portBaseType xReturn = pdPASS;

	/* Check that the region definitions are correct. */
	if( pdPASS != xMPUCheckTaskRegions( pxTaskParameters->xMPUParameters.axRegions ) )
	{
		/* Set the error code to indicate an MPU configuration error. */
		xReturn = errINVALID_MPU_REGION_CONFIGURATION;
		/* MCDC Test Point: STD_IF "xPortCheckTaskParameters" */
	}
	/* MCDC Test Point: ADD_ELSE "xPortCheckTaskParameters" */

	/* Check the stack size. */
	if( pdPASS == xReturn )
	{
		/* The stack must be at least large enough to hold two lots of task context
		 * registers. */
		if( pdFALSE != pxTaskParameters->xUsingFPU )
		{
			if( uxTaskStackSize < ( uxAdditionalStackCheckMarginBytes + ( 2U * portCONTEXT_SIZE_BYTES_WITH_FPU ) ) )
			{
				xReturn = errSUPPLIED_BUFFER_TOO_SMALL;
				/* MCDC Test Point: STD_IF "xPortCheckTaskParameters" */
			}
			/* MCDC Test Point: ADD_ELSE "xPortCheckTaskParameters" */
		}
		else
		{
			if( uxTaskStackSize < ( uxAdditionalStackCheckMarginBytes + ( 2U * portCONTEXT_SIZE_BYTES_NO_FPU ) ) )
			{
				xReturn = errSUPPLIED_BUFFER_TOO_SMALL;
				/* MCDC Test Point: STD_IF "xPortCheckTaskParameters" */
			}
			/* MCDC Test Point: ADD_ELSE "xPortCheckTaskParameters" */

			/* MCDC Test Point: STD_ELSE "xPortCheckTaskParameters" */
		}
	}
	/* MCDC Test Point: ADD_ELSE "xPortCheckTaskParameters" */

	/* If the stack is large enough, check for alignment. */
	if( pdPASS == xReturn )
	{
		/* The task stack must be a power of 2. */
		do
		{
			uxValidStackSize <<= 1U;

			/* MCDC Test Point: EXP_WHILE_INTERNAL_AND "xPortCheckTaskParameters" "( uxValidStackSize < uxTaskStackSize )" "( portmpuLARGEST_REGION_SIZE_ACTUAL != uxValidStackSize )" */
		} while( ( uxValidStackSize < uxTaskStackSize ) &&
				 ( portmpuLARGEST_REGION_SIZE_ACTUAL != uxValidStackSize ) );

		/* Is the stack size valid? */
		if( uxValidStackSize != uxTaskStackSize )
		{
			xReturn = errINVALID_BUFFER_SIZE;
			/* MCDC Test Point: STD_IF "xPortCheckTaskParameters" */
		}
		else
		{
			/* The stack size is valid, now check the alignment. */
			if( 0UL != ( ( ( portUnsignedBaseType ) pxTaskParameters->pcStackBuffer ) % uxTaskStackSize ) )
			{
				/* The stack is not correctly aligned. */
				xReturn = errINVALID_BYTE_ALIGNMENT;
				/* MCDC Test Point: STD_IF "xPortCheckTaskParameters" */
			}
			/* MCDC Test Point: ADD_ELSE "xPortCheckTaskParameters" */

			/* MCDC Test Point: STD_ELSE "xPortCheckTaskParameters" */
		}
	}
	/* MCDC Test Point: ADD_ELSE "xPortCheckTaskParameters" */

	return xReturn;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION void vPortInitialiseTask( const xTaskParameters * const pxTaskParameters )
{
xTCB *pxTCB;
volatile portStackType *pxTopOfStack;

	/* Configure the TCB associated with the task. */
	pxTCB = prvSetupTCB( pxTaskParameters );

	/* Indicate that this task stack is in use. */
	*( pxTCB->pxStackInUseMarker ) = portSTACK_IN_USE;

	/* Setup the initial stack of the task. The stack is set exactly as
	 * expected by the portRESTORE_CONTEXT() macro. */
	pxTopOfStack = pxTCB->pxTopOfStack;

	/* First on the stack is the return address - which in this case is the
	 * start of the task. The offset is added to make the return address appear
	 * as it would within an IRQ ISR. */
	*pxTopOfStack = ( portStackType ) pxTaskParameters->pvTaskCode + portINSTRUCTION_SIZE;
	pxTopOfStack--;

	*pxTopOfStack = ( portStackType ) 0xAAAAAAAAUL;	/* R14 */
	pxTopOfStack--;
	*pxTopOfStack = ( portStackType ) pxTCB->pxTopOfStack; /* Stack used when task starts goes in R13. */
	pxTopOfStack--;
	*pxTopOfStack = ( portStackType ) 0x12121212UL;	/* R12 */
	pxTopOfStack--;
	*pxTopOfStack = ( portStackType ) 0x11111111UL;	/* R11 */
	pxTopOfStack--;
	*pxTopOfStack = ( portStackType ) 0x10101010UL;	/* R10 */
	pxTopOfStack--;
	*pxTopOfStack = ( portStackType ) 0x09090909UL;	/* R9 */
	pxTopOfStack--;
	*pxTopOfStack = ( portStackType ) 0x08080808UL;	/* R8 */
	pxTopOfStack--;
	*pxTopOfStack = ( portStackType ) 0x07070707UL;	/* R7 */
	pxTopOfStack--;
	*pxTopOfStack = ( portStackType ) 0x06060606UL;	/* R6 */
	pxTopOfStack--;
	*pxTopOfStack = ( portStackType ) 0x05050505UL;	/* R5 */
	pxTopOfStack--;
	*pxTopOfStack = ( portStackType ) 0x04040404UL;	/* R4 */
	pxTopOfStack--;
	*pxTopOfStack = ( portStackType ) 0x03030303UL;	/* R3 */
	pxTopOfStack--;
	*pxTopOfStack = ( portStackType ) 0x02020202UL;	/* R2 */
	pxTopOfStack--;
	*pxTopOfStack = ( portStackType ) 0x01010101UL;	/* R1 */
	pxTopOfStack--;

	/* When the task starts it will expect to find the function parameter in R0. */
	*pxTopOfStack = ( portStackType ) pxTaskParameters->pvParameters; /* R0 */
	pxTopOfStack--;

	/* The CPSR register contains the operating mode setting. During a context
	 * switch this is held in the Saved Program Status Register. */
	if( mpuPRIVILEGED_TASK == pxTaskParameters->xMPUParameters.uxPrivilegeLevel )
	{
		*pxTopOfStack = portCPSR_INITIAL_VALUE | portPRIVILEGED_MODE_SETTING;
		pxTCB->ulSystemModeSetting = portPRIVILEGED_MODE_SETTING;

		/* MCDC Test Point: STD_IF "vPortInitialiseTask" */
	}
	else
	{
		*pxTopOfStack = portCPSR_INITIAL_VALUE | portUNPRIVILEGED_MODE_SETTING;
		pxTCB->ulSystemModeSetting = portUNPRIVILEGED_MODE_SETTING;

		/* MCDC Test Point: STD_ELSE "vPortInitialiseTask" */
	}

	if( ( ( portUInt32Type ) pxTaskParameters->pvTaskCode & 0x01UL ) != 0x00UL )
	{
		/* We want the task to start in thumb mode. */
		*pxTopOfStack |= portTHUMB_MODE_BIT;

		/* MCDC Test Point: STD_IF "vPortInitialiseTask" */
	}
	/* MCDC Test Point: ADD_ELSE "vPortInitialiseTask" */

	pxTopOfStack--;
	*pxTopOfStack = portNO_CRITICAL_SECTION_NESTING;

	if( pdTRUE == pxTCB->xUsingFPU )
	{
		/* If this task will use the FPU, then there also needs to be initial
		 * values for each of the 16 64-bit general purpose registers and the
		 * FPSCR on the stack. For the general purpose registers, the initial
		 * values aren't important, but using known values assists the testing
		 * of this function. */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00001515UL;	/* D15L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x15150000UL;	/* D15H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00001414UL;	/* D14L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x14140000UL;	/* D14H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00001313UL;	/* D13L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x13130000UL;	/* D13H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00001212UL;	/* D12L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x12120000UL;	/* D12H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00001111UL;	/* D11L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x11110000UL;	/* D11H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00001010UL;	/* D10L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x10100000UL;	/* D10H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00000909UL;	/* D9L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x09090000UL;	/* D9H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00000808UL;	/* D8L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x08080000UL;	/* D8H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00000707UL;	/* D7L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x07070000UL;	/* D7H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00000606UL;	/* D6L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x06060000UL;	/* D6H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00000505UL;	/* D5L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x05050000UL;	/* D5H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00000404UL;	/* D4L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x04040000UL;	/* D4H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00000303UL;	/* D3L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x03030000UL;	/* D3H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00000202UL;	/* D2L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x02020000UL;	/* D2H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00000101UL;	/* D1L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x01010000UL;	/* D1H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00000000UL;	/* D0L */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00000000UL;	/* D0H */
		pxTopOfStack--;
		*pxTopOfStack = ( portStackType ) 0x00000000UL;	/* FPSCR */
		/* MCDC Test Point: STD_IF "vPortInitialiseTask" */
	}
	/* MCDC Test Point: ADD_ELSE "vPortInitialiseTask" */

	/* Update the Top of Stack and Top of Stack Mirror variables in the TCB
	 * after the initial context has been placed on the stack. */
	pxTCB->pxTopOfStack = pxTopOfStack;
	pxTCB->uxTopOfStackMirror = ~( ( portUnsignedBaseType ) pxTCB->pxTopOfStack );
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xPortStartScheduler( portBaseType xUseKernelConfigurationChecks )
{
portBaseType xReturn;

	if( pdFALSE != xUseKernelConfigurationChecks )
	{
		xReturn = prvCheckKernelConfiguration();

		/* MCDC Test Point: STD_IF "xPortStartScheduler" */
	}
	else
	{
		xReturn = pdPASS;

		/* MCDC Test Point: STD_ELSE "xPortStartScheduler" */
	}

	if( pdPASS == xReturn )
	{
		/* Create the idle task at the lowest priority. */
		xReturn = xTaskCreate( &xIdleTaskParameters, ( portTaskHandleType * ) NULL );

		/* MCDC Test Point: STD_IF "xPortStartScheduler" */
	}
	/* MCDC Test Point: ADD_ELSE "xPortStartScheduler" */

	/* If the idle task was created successfully, configure the system tick. */
	if( pdPASS == xReturn )
	{
		if( ( portUnsignedBaseType ) pxSetupTickInterruptHookFunction == ~uxSetupTickInterruptHookMirror )
		{
			/* Start the timer that generates the tick ISR. Interrupts are
			 * disabled here already. */
			if( NULL == pxSetupTickInterruptHookFunction )
			{
				/* No hook was provided, use the default method. */
				prvSetupTimerInterrupt( ulCPUClockHz, ulTickRateHz );

				/* MCDC Test Point: STD_IF "xPortStartScheduler" */
			}
			else
			{
				/* A hook was provided, call it now. */
				pxSetupTickInterruptHookFunction( ulCPUClockHz, ulTickRateHz );

				/* MCDC Test Point: STD_ELSE "xPortStartScheduler" */
			}
		}
		else
		{
			xReturn = errBAD_HOOK_FUNCTION_ADDRESS;
			/* MCDC Test Point: STD_ELSE "xPortStartScheduler" */
		}
	}
	/* MCDC Test Point: ADD_ELSE "xPortStartScheduler" */

	/* If the system tick was configured successfully, we can start. */
	if( pdPASS == xReturn )
	{
		/* Configure the regions in the MPU that are common to all tasks. */
		vMPUSetupMPU();

		/* Enable the processor cache if required */
		if ( pdFALSE != xEnableProcessorCache )
		{
			/* MCDC Test Point: STD_IF "xPortStartScheduler" */
			vPortCacheEnable();
		}
		/* MCDC Test Point: ADD_ELSE "xPortStartScheduler" */

		/* Ensure the VFP is enabled - it should be anyway. */
		vPortEnableVFP();

		/* MCDC Test Point: STD_IF "xPortStartScheduler" */

		/* Start the first task. */
		portSTART_FIRST_TASK();
	}
	/* MCDC Test Point: ADD_ELSE "xPortStartScheduler" */

	/* We will only reach here if prvCheckKernelConfiguration() fails or if the
	 * idle task isn't created, otherwise the scheduler will be running. */
	return xReturn;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xPortIsTaskHandleValid( portTaskHandleType xTaskToCheck )
{
portBaseType xReturn = pdTRUE;
xTCB *pxTCB;
portUnsignedBaseType uxRegionIndex;
const portmpuRegionRegistersType *pxMPURegions;
const portmpuRegionRegistersType *pxMPURegionsMirror;

	/* First ensure we've not been given a NULL pointer to check. */
	if( NULL == xTaskToCheck )
	{
		xReturn = pdFALSE;

		/* MCDC Test Point: STD_IF "xPortIsTaskHandleValid" */
	}
	else
	{
		/* Set a pointer to the TCB buffer. */
		pxTCB = ( xTCB * ) xTaskToCheck;

		if( ( portUnsignedBaseType )( pxTCB->pxStackLimit ) != ~( pxTCB->uxStackLimitMirror ) )
		{
			/* The stack limit mirror is not as expected. */
			xReturn = pdFALSE;
			/* MCDC Test Point: STD_IF "xPortIsTaskHandleValid" */
		}
		else if( ( portUnsignedBaseType )( pxTCB->pxTopOfStack ) != ~( pxTCB->uxTopOfStackMirror ) )
		{
			/* The top of stack mirror is not as expected. */
			xReturn = pdFALSE;
			/* MCDC Test Point: STD_ELSE_IF "xPortIsTaskHandleValid" */
		}
		else if( ( portUnsignedBaseType )( pxTCB->xUsingFPU ) != ~( pxTCB->uxUsingFPUMirror ) )
		{
			/* The 'Using FPU' flag mirror is not as expected. */
			xReturn = pdFALSE;
			/* MCDC Test Point: STD_ELSE_IF "xPortIsTaskHandleValid" */
		}
		else if( portSTACK_IN_USE != *( pxTCB->pxStackInUseMarker ) )
		{
			/* The pxStackInUseMarker is not as expected. */
			xReturn = pdFALSE;
			/* MCDC Test Point: STD_ELSE_IF "xPortIsTaskHandleValid" */
		}
		else
		{
			/* Are the MPU register settings as expected? */

			/* NOTE: we use pointers here (breaching MISRA rules) because
			 * updating two pointers is more efficient than using uxRegionIndex
			 * to reference each member of the axMPURegions[] and
			 * axMPURegionsMirror[] arrays.
			 * This function is called extensively by the kernel, especially
			 * during context switches, therefore its efficiency is critical.
			 */
			pxMPURegions = pxTCB->axMPURegions;
			pxMPURegionsMirror = pxTCB->axMPURegionsMirror;

			for( uxRegionIndex = 0UL; uxRegionIndex < portmpuTASK_REGION_NUM; uxRegionIndex++ )
			{
				if( pxMPURegions->ulRegionBaseAddress != ~( pxMPURegionsMirror->ulRegionBaseAddress ) )
				{
					xReturn = pdFALSE;
					/* MCDC Test Point: STD_IF "xPortIsTaskHandleValid" */
				}
				/* MCDC Test Point: ADD_ELSE "xPortIsTaskHandleValid" */

				if( pxMPURegions->ulRegionAttribute != ~( pxMPURegionsMirror->ulRegionAttribute ) )
				{
					xReturn = pdFALSE;
					/* MCDC Test Point: STD_IF "xPortIsTaskHandleValid" */
				}
				/* MCDC Test Point: ADD_ELSE "xPortIsTaskHandleValid" */

				if( pxMPURegions->ulRegionSize != ~( pxMPURegionsMirror->ulRegionSize ) )
				{
					xReturn = pdFALSE;
					/* MCDC Test Point: STD_IF "xPortIsTaskHandleValid" */
				}
				/* MCDC Test Point: ADD_ELSE "xPortIsTaskHandleValid" */

				/* Update pointer to next region. */
				pxMPURegions++;
				pxMPURegionsMirror++;
			}

			/* MCDC Test Point: STD_ELSE "xPortIsTaskHandleValid" */
		}

		/* MCDC Test Point: STD_ELSE "xPortIsTaskHandleValid" */
	}

	return xReturn;
}
/*---------------------------------------------------------------------------*/

/* Port specific helper function that will add any required port specific
 * declarations to a kernel task parameter structure. */
KERNEL_FUNCTION void vPortAddKernelTaskParams( xTaskParameters *pxKernelTaskParams )
{
	if( NULL != pxKernelTaskParams )
	{
		pxKernelTaskParams->xUsingFPU = pdFALSE;
		pxKernelTaskParams->xMPUParameters = xKernelTaskMPUParams;

		/* MCDC Test Point: STD_IF "vPortAddKernelTaskParams" */
	}
	/* MCDC Test Point: ADD_ELSE "vPortAddKernelTaskParams" */
}
/*---------------------------------------------------------------------------*/

void vPortRestoreKernelTaskPrivileges( void )
{
	/* Raise the privilege in case it has been reduced by application code. */
	( void ) portRAISE_PRIVILEGE();

	/* Apply the default MPU settings. */
	vMPUStoreTaskRegions( pxCurrentTCB, &xKernelTaskMPUParams );

	/* MCDC Test Point: STD "vPortRestoreKernelTaskPrivileges" */
}
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION void vPortEnterCritical( void )
{
	portSET_INTERRUPT_MASK();
	++ulCriticalNesting;

	/* MCDC Test Point: STD "vPortEnterCritical" */
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION void vPortExitCritical( void )
{
	if( 0U != ulCriticalNesting )
	{
		--ulCriticalNesting;
		if( 0U == ulCriticalNesting )
		{
			portCLEAR_INTERRUPT_MASK();
			/* MCDC Test Point: STD_IF "vPortExitCritical" */
		}
		/* MCDC Test Point: ADD_ELSE "vPortExitCritical" */
	}
	/* MCDC Test Point: ADD_ELSE "vPortExitCritical" */
}

/*-----------------------------------------------------------------------------
 * Exception Handlers
 *---------------------------------------------------------------------------*/

KERNEL_FUNCTION __irq __arm void vSafeRTOSSysTickHandler( void )
{
	/* Clear the interrupt */
	portRTIINTFLAG = 0x01UL;

	/* Call the handler routine. */
	vTaskProcessSystemTickFromISR();

	/* MCDC Test Point: STD "vSafeRTOSSysTickHandler" */
}
/*---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 * Hook Functions
 *---------------------------------------------------------------------------*/

KERNEL_FUNCTION void vPortErrorHook( portTaskHandleType xHandleOfTaskWithError,
									const portCharType *pcErrorString,
									portBaseType xErrorCode )
{
	if( ( portUnsignedBaseType ) pxErrorHookFunction == ~uxErrorHookMirror )
	{
		/* Execute Error Hook function. */
		pxErrorHookFunction( xHandleOfTaskWithError, pcErrorString, xErrorCode );
		/* MCDC Test Point: STD_IF "vPortErrorHook" */
	}
	else
	{
		taskENTER_CRITICAL();
		for( ;; )
		{
			/* MCDC Test Point: STD_ELSE_REPORT "vPortErrorHook" */
		}
	}
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION void vPortTaskDeleteHook( portTaskHandleType xTaskBeingDeleted )
{
	if( ( portUnsignedBaseType ) pxTaskDeleteHookFunction == ~uxTaskDeleteHookMirror )
	{
		if( NULL != pxTaskDeleteHookFunction )
		{
			/* Execute Task-Delete Hook function. */
			pxTaskDeleteHookFunction( xTaskBeingDeleted );
			/* MCDC Test Point: STD_IF "vPortTaskDeleteHook" */
		}
		/* MCDC Test Point: ADD_ELSE "vPortTaskDeleteHook" */
	}
	else
	{
		/* MCDC Test Point: STD_ELSE "vPortTaskDeleteHook" */
		vPortErrorHook( NULL,
						"Task-delete hook corrupted",
						errBAD_HOOK_FUNCTION_ADDRESS );
	}
}
/*---------------------------------------------------------------------------*/

void vPortIdleHook( void )
{
portIDLE_HOOK pxCachedIdleHookFunction;

	if( ( portUnsignedBaseType ) pxIdleHookFunction == ~uxIdleHookMirror )
	{
		if( NULL != pxIdleHookFunction )
		{
			/* Cache the hook function pointer, as it will become inaccessible
			 * once we have lowered the privilege level. */
			pxCachedIdleHookFunction = pxIdleHookFunction;

			/* Lower privilege level if needed. */
			if( pdFALSE == xIdleHookRunsPrivileged )
			{
				portLOWER_PRIVILEGE();
				/* MCDC Test Point: STD_IF "vPortIdleHook" */
			}
			/* MCDC Test Point: ADD_ELSE "vPortIdleHook" */

			/* Execute Idle Hook function. */
			pxCachedIdleHookFunction();

			/* Re-raise privilege level.
			 * NOTE: we need to do it regardless of the value of
			 * xIdleHookRunsPrivileged, since it could have been lowered within
			 * pxCachedIdleHookFunction(). */
			( void ) portRAISE_PRIVILEGE();
		}
		/* MCDC Test Point: ADD_ELSE "vPortIdleHook" */
	}
	else
	{
		/* MCDC Test Point: STD_ELSE "vPortIdleHook" */
		vPortErrorHook( NULL,
						"Idle hook corrupted",
						errBAD_HOOK_FUNCTION_ADDRESS );
	}
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION void vPortTickHook( void )
{
	if( ( portUnsignedBaseType ) pxTickHookFunction == ~uxTickHookMirror )
	{
		if( NULL != pxTickHookFunction )
		{
			/* Execute Tick Hook function. */
			pxTickHookFunction();
			/* MCDC Test Point: STD_IF "vPortTickHook" */
		}
		/* MCDC Test Point: ADD_ELSE "vPortTickHook" */
	}
	else
	{
		/* MCDC Test Point: STD_ELSE "vPortTickHook" */
		vPortErrorHook( NULL,
						"Tick hook corrupted",
						errBAD_HOOK_FUNCTION_ADDRESS );
	}
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION void vPortControlCheckFailed( void )
{
	/* MCDC Test Point: STD "vPortControlCheckFailed" */
	vPortErrorHook( pxCurrentTCB,
					"Either TCB or stack corrupted",
					errINVALID_TASK_SELECTED );
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION void vPortSvcHookCheckFailed( void )
{
	/* MCDC Test Point: STD "vPortSvcHookCheckFailed" */
	vPortErrorHook( NULL,
					"SVC hook corrupted",
					errBAD_HOOK_FUNCTION_ADDRESS );
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION void vPortSSIHookCheckFailed( void )
{
	/* MCDC Test Point: STD "vPortSSIHookCheckFailed" */
	vPortErrorHook( NULL,
					"SSI hook corrupted",
					errBAD_HOOK_FUNCTION_ADDRESS );
}
/*---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 * Filescope functions
 *---------------------------------------------------------------------------*/

KERNEL_FUNCTION static xTCB *prvSetupTCB( const xTaskParameters * const pxTaskParameters )
{
xTCB *pxNewTCB;
portInt8Type *pcTopOfStack;
portUnsignedBaseType uxStackDepthBytes;

	/* Set a pointer to the TCB buffer. */
	pxNewTCB = pxTaskParameters->pxTCB;

	/* Store the base address of the task's stack buffer. */
	pxNewTCB->pcStackBaseAddress = pxTaskParameters->pcStackBuffer;

	/* Calculate the top of stack address. */
	uxStackDepthBytes = pxTaskParameters->uxStackDepthBytes & ~portSTACK_ALIGNMENT_MASK;
	pcTopOfStack = pxTaskParameters->pcStackBuffer + ( uxStackDepthBytes - sizeof( portStackType ) );

	/* Set the 'StackInUse' and 'Top of Stack' pointers ensuring correct
	 * alignment. */
	pxNewTCB->pxStackInUseMarker = ( portStackType * ) pcTopOfStack;
	pxNewTCB->pxTopOfStack = ( portStackType * ) ( ( ( portUnsignedBaseType ) pcTopOfStack - sizeof( portStackType ) ) & ~portSTACK_ALIGNMENT_MASK );

	/* Calculate the stack limit - this is the minimum amount of space required
	 * on the task stack for the context switch. */
	if( pdFALSE != pxTaskParameters->xUsingFPU )
	{
		pxNewTCB->pxStackLimit = ( portStackType * ) ( pxTaskParameters->pcStackBuffer + ( portCONTEXT_SIZE_BYTES_WITH_FPU + uxAdditionalStackCheckMarginBytes ) );

		/* MCDC Test Point: STD_IF "prvSetupTCB" */
	}
	else
	{
		pxNewTCB->pxStackLimit = ( portStackType * ) ( pxTaskParameters->pcStackBuffer + ( portCONTEXT_SIZE_BYTES_NO_FPU + uxAdditionalStackCheckMarginBytes ) );

		/* MCDC Test Point: STD_ELSE "prvSetupTCB" */
	}

	/* Set uxStackLimitMirror to the bitwise inverse of pxStackLimit. */
	pxNewTCB->uxStackLimitMirror = ~( ( portUnsignedBaseType ) pxNewTCB->pxStackLimit );

	/* Setup the TCB. */
	pxNewTCB->uxPriority = pxTaskParameters->uxPriority;
	pxNewTCB->uxBasePriority = pxTaskParameters->uxPriority;
	pxNewTCB->pcNameOfTask = pxTaskParameters->pcTaskName;
	vListInitialiseItem( &( pxNewTCB->xStateListItem ) );
	vListInitialiseItem( &( pxNewTCB->xEventListItem ) );

	/* Set the pxTCB as a link back from the xListItem. This is so we can get
	 * back to the containing TCB from a generic item in a list. */
	listSET_LIST_ITEM_OWNER( &( pxNewTCB->xStateListItem ), pxNewTCB );

	/* Tasks are stored within event lists such that the list references tasks
	 * in priority order - high priority to low priority. The ItemValue is used
	 * to hold the priority in this case - but because ordered lists actually
	 * sort themselves so that low values come before high values, the priority
	 * assigned to the ItemValue has to be inverted. Thus high priority tasks
	 * get a low ItemValue value, and low priority tasks get a high ItemValue
	 * value. */
	listSET_LIST_ITEM_VALUE( &( pxNewTCB->xEventListItem ), ( portTickType ) ( configMAX_PRIORITIES - pxTaskParameters->uxPriority ) );
	listSET_LIST_ITEM_OWNER( &( pxNewTCB->xEventListItem ), pxNewTCB );

	/* Initialise notification parameters. */
	pxNewTCB->uxNotifiedValue = 0UL;
	pxNewTCB->xNotifyState = taskNOTIFICATION_NOT_WAITING;

	/* Initialise TLS Object. */
	pxNewTCB->pvObject = pxTaskParameters->pvObject;

	/* Initialise the mutex held list. */
	vListInitialise( &( pxNewTCB->xMutexesHeldList ) );

	/* The MPU region configuration needs to be stored in the task's TCB. */
	vMPUStoreTaskRegions( pxNewTCB, &( pxTaskParameters->xMPUParameters ) );

	/* Initialise the run-time statistics members of the TCB. */
	vInitialiseTaskRunTimeStatistics( pxNewTCB );

	/* Initialise the FPU members of the TCB. */
	pxNewTCB->xUsingFPU = pxTaskParameters->xUsingFPU;
	pxNewTCB->uxUsingFPUMirror = ~( ( portUnsignedBaseType ) pxNewTCB->xUsingFPU );

	return pxNewTCB;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION static portBaseType prvCheckKernelConfiguration( void )
{
portBaseType xReturn = pdFAIL;
const volatile portUInt32Type *pulVIMTable = portVIM_TABLE_ADDRESS;
portUnsignedBaseType uxSysSwVectorChannelId = 0U;
/* NOTE: the Flash start at 0, so we only check against the upper limit. */
const portUnsignedBaseType uxUsedFlashLimit = ( portUnsignedBaseType ) &__ICFEDIT_region_ROM_end__;

	/* Perform the required checks. */
	if( ulCPUClockHz < portCPU_CLOCK_HZ_MIN )
	{
		xReturn = errBAD_OR_NO_TICK_RATE_CONFIGURATION;
		/* MCDC Test Point: STD_IF "prvCheckKernelConfiguration" */
	}
	else if( 0UL == ulTickRateHz )
	{
		xReturn = errBAD_OR_NO_TICK_RATE_CONFIGURATION;
		/* MCDC Test Point: STD_ELSE_IF "prvCheckKernelConfiguration" */
	}
	else if( ulTickRateHz > portTICK_RATE_HZ_MAX )
	{
		xReturn = errBAD_OR_NO_TICK_RATE_CONFIGURATION;
		/* MCDC Test Point: STD_ELSE_IF "prvCheckKernelConfiguration" */
	}
	else if( ( portUnsignedBaseType ) pxTaskDeleteHookFunction >= uxUsedFlashLimit )
	{
		/* Note that the task delete hook is allowed to be NULL but, if defined,
		 * it must be within the Flash range. */
		xReturn = errBAD_HOOK_FUNCTION_ADDRESS;
		/* MCDC Test Point: STD_ELSE_IF "prvCheckKernelConfiguration" */
	}
	else if( ( portUnsignedBaseType ) pxSetupTickInterruptHookFunction >= uxUsedFlashLimit )
	{
		/* Note that the setup tick hook is allowed to be NULL but, if defined,
		 * it must be within the Flash range. */
		xReturn = errBAD_HOOK_FUNCTION_ADDRESS;
		/* MCDC Test Point: STD_ELSE_IF "prvCheckKernelConfiguration" */
	}
	else if( NULL == pxErrorHookFunction )
	{
		xReturn = errBAD_HOOK_FUNCTION_ADDRESS;
		/* MCDC Test Point: STD_ELSE_IF "prvCheckKernelConfiguration" */
	}
	else if( ( portUnsignedBaseType ) pxErrorHookFunction >= uxUsedFlashLimit )
	{
		xReturn = errBAD_HOOK_FUNCTION_ADDRESS;
		/* MCDC Test Point: STD_ELSE_IF "prvCheckKernelConfiguration" */
	}
	else if( ( portUnsignedBaseType ) pxIdleHookFunction >= uxUsedFlashLimit )
	{
		/* Note that the idle hook is allowed to be NULL but, if defined,
		 * it must be within the Flash range. */
		xReturn = errBAD_HOOK_FUNCTION_ADDRESS;
		/* MCDC Test Point: STD_ELSE_IF "prvCheckKernelConfiguration" */
	}
	else if( ( portUnsignedBaseType ) pxTickHookFunction >= uxUsedFlashLimit )
	{
		/* Note that the tick hook is allowed to be NULL but, if defined,
		 * it must be within the Flash range. */
		xReturn = errBAD_HOOK_FUNCTION_ADDRESS;
		/* MCDC Test Point: STD_ELSE_IF "prvCheckKernelConfiguration" */
	}
	else if( ( portUnsignedBaseType ) pxSvcHookFunction >= uxUsedFlashLimit )
	{
		/* Note that the SVC hook is allowed to be NULL but, if defined,
		 * it must be within the Flash range. */
		xReturn = errBAD_HOOK_FUNCTION_ADDRESS;
		/* MCDC Test Point: STD_ELSE_IF "prvCheckKernelConfiguration" */
	}
	else if( ( portUnsignedBaseType ) pxSSIHookFunction >= uxUsedFlashLimit )
	{
		/* Note that the SSI hook is allowed to be NULL but, if defined,
		 * it must be within the Flash range. */
		xReturn = errBAD_HOOK_FUNCTION_ADDRESS;
		/* MCDC Test Point: STD_ELSE_IF "prvCheckKernelConfiguration" */
	}
	else if( uxPortMPUGetNumberRegions() < portmpuMINIMUM_SUPPORTED_REGIONS )
	{
		xReturn = errNO_MPU_IN_DEVICE;
		/* MCDC Test Point: STD_ELSE_IF "prvCheckKernelConfiguration" */
	}
	else
	{
		/* Read priority of System Software Interrupt. */
		if( pdFALSE == prvGetUniqueInterruptChannel( portVIM_SYS_SOFTWARE_INTR_REQ, &uxSysSwVectorChannelId ) )
		{
			xReturn = errERROR_IN_VECTOR_TABLE;
			/* MCDC Test Point: STD_IF "prvCheckKernelConfiguration" */
		}
		else if( pulVIMTable[ uxSysSwVectorChannelId + 1U ] != ( portUInt32Type ) &vSafeRTOSSysSoftwareHandler )
		{
			xReturn = errERROR_IN_VECTOR_TABLE;
			/* MCDC Test Point: STD_ELSE_IF "prvCheckKernelConfiguration" */
		}
		else
		{
			xReturn = pdPASS;
			/* MCDC Test Point: STD_ELSE "prvCheckKernelConfiguration" */
		}

		/* MCDC Test Point: STD_ELSE "prvCheckKernelConfiguration" */
	}

	return xReturn;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION static portBaseType prvGetUniqueInterruptChannel( portUnsignedBaseType uxIntrRequest, portUnsignedBaseType *puxIntrChannel )
{
portBaseType xReturn = pdFALSE;
const volatile portUInt32Type *pulVIMChanCtrl = portVIM_CHANCTRL_BASE;
const volatile portUInt32Type *pulVIMReqEnaSet = portVIM_REQENASET_BASE;
portUnsignedBaseType uxNumChannelsFound = 0U;
portUnsignedBaseType uxIntrChannelIdx;
portUnsignedBaseType uxChanCtrlRegId;
portUnsignedBaseType uxChanMapRShift;
portUnsignedBaseType uxChanMapValue;
portUnsignedBaseType uxReqEnaRegId;
portUnsignedBaseType uxReqEnaBitNum;
portUnsignedBaseType uxReqEnaMask;

	/* Scan CHANCTRL registers for interrupts. */
	for( uxIntrChannelIdx = 0U; uxIntrChannelIdx < configVIM_NUM_INTERRUPTS; uxIntrChannelIdx++ )
	{
		/* Calculate CHANCTRLx register id and CHANMAPy shift. */
		uxChanCtrlRegId = uxIntrChannelIdx >> 2U;											/* divide by 4 */
		uxChanMapRShift = ( 3U - ( uxIntrChannelIdx - ( uxChanCtrlRegId << 2U ) ) ) << 3U;	/* ((3 - (channel % 4)) * 8) */

		/* Read the correct CHANMAPy field. */
		uxChanMapValue = ( pulVIMChanCtrl[ uxChanCtrlRegId ] >> uxChanMapRShift ) & 0xFFUL;

		/* Calculate REQMASK register id and bit number. */
		uxReqEnaRegId = uxIntrChannelIdx >> 5U;							/* divide by 32 */
		uxReqEnaBitNum = ( uxIntrChannelIdx - ( uxReqEnaRegId << 5 ) );	/* module 32 */

		/* Read the correct REQENASET bit mask. */
		uxReqEnaMask = pulVIMReqEnaSet[ uxReqEnaRegId ] & ( 1UL << uxReqEnaBitNum );

		/* Does CHANMAPy match the request id we are searching for? */
		/* MCDC Test Point: EXP_IF_AND "prvGetUniqueInterruptChannel" "( uxChanMapValue == uxIntrRequest )" "( 0UL != uxReqEnaMask )" */
		if( ( uxChanMapValue == uxIntrRequest ) &&
			( 0UL != uxReqEnaMask ) )
		{
			/* We found a match. */
			uxNumChannelsFound++;

			/* Is this the first match? */
			if( 1U == uxNumChannelsFound )
			{
				*puxIntrChannel = uxIntrChannelIdx;
				xReturn = pdTRUE;
				/* MCDC Test Point: STD_IF "prvGetUniqueInterruptChannel" */
			}
			else
			{
				/* The same request cannot be mapped on 2 channels. */
				xReturn = pdFALSE;
				/* MCDC Test Point: STD_ELSE "prvGetUniqueInterruptChannel" */
			}
		}
		/* MCDC Test Point: ADD_ELSE "prvGetUniqueInterruptChannel" */
	}

	return xReturn;
}
/*---------------------------------------------------------------------------*/

KERNEL_FUNCTION static void prvSetupTimerInterrupt( portUInt32Type ulClockHz, portUInt32Type ulRateHz )
{
portUInt32Type ulCompareValue;
portBaseType xReadbackTestFailed = pdFALSE;

	/* Calculate compare value. */
	ulCompareValue = ( ( ulClockHz / 2UL ) / ulRateHz );

	/* Disable the Timer0. */
	portRTIGCTRL &= ~portRTIGCTRL_ENABLE_BIT;

	/* Use the internal counter. */
	portRTITBCTRL = portRTITBCTRL_USE_INTERNAL_COUNTER;

	/* Initialise the counter registers. */
	portRTIUC0 = 0UL;
	portRTIFRC0 = 0UL;

	/* Setthe prescale counter register. */
	portRTICPUC0 = portRTICPUC0_PRESCALE_DIV_2;

	/* Set Prescaler for RTI clock. */
	portRTICOMP0 = ulCompareValue;
	portRTIUDCP0 = ulCompareValue;

	/* Enable Timer0 compare interrupt INT0. */
	portRTISETINT = portRTISETINT_ENABLE_TIMER0_COMPARE_INT_0;

	/* COMPSEL0 will use the RTIFRC0 counter. */
	portRTICOMPCTRL = portRTICOMPCTRL_COMPSEL0_RTIFRC0;

	/* Enable the interrupt channel in the VIM. */
	portVIM_REQMASKSET0 = 0x00000004UL;

	/* Test the RTI Values have been written correctly */
	if( portRTITBCTRL_USE_INTERNAL_COUNTER != portRTITBCTRL )
	{
		xReadbackTestFailed = pdTRUE;
		/* MCDC Test Point: STD_IF "prvSetupTimerInterrupt" */
	}
	/* MCDC Test Point: ADD_ELSE "prvSetupTimerInterrupt" */

	if( 0UL != portRTIFRC0 )
	{
		xReadbackTestFailed = pdTRUE;
		/* MCDC Test Point: STD_IF "prvSetupTimerInterrupt" */
	}
	/* MCDC Test Point: ADD_ELSE "prvSetupTimerInterrupt" */

	if( 0UL != portRTIUC0 )
	{
		xReadbackTestFailed = pdTRUE;
		/* MCDC Test Point: STD_IF "prvSetupTimerInterrupt" */
	}
	/* MCDC Test Point: ADD_ELSE "prvSetupTimerInterrupt" */

	if( portRTICPUC0_PRESCALE_DIV_2 != portRTICPUC0 )
	{
		xReadbackTestFailed = pdTRUE;
		/* MCDC Test Point: STD_IF "prvSetupTimerInterrupt" */
	}
	/* MCDC Test Point: ADD_ELSE "prvSetupTimerInterrupt" */

	if( portRTICOMP0 != ulCompareValue )
	{
		xReadbackTestFailed = pdTRUE;
		/* MCDC Test Point: STD_IF "prvSetupTimerInterrupt" */
	}
	/* MCDC Test Point: ADD_ELSE "prvSetupTimerInterrupt" */

	if( portRTIUDCP0 != ulCompareValue )
	{
		xReadbackTestFailed = pdTRUE;
		/* MCDC Test Point: STD_IF "prvSetupTimerInterrupt" */
	}
	/* MCDC Test Point: ADD_ELSE "prvSetupTimerInterrupt" */

	if( 0UL == ( portRTISETINT_ENABLE_TIMER0_COMPARE_INT_0 & portRTISETINT ) )
	{
		xReadbackTestFailed = pdTRUE;
		/* MCDC Test Point: STD_IF "prvSetupTimerInterrupt" */
	}
	/* MCDC Test Point: ADD_ELSE "prvSetupTimerInterrupt" */

	if( portRTICOMPCTRL_COMPSEL0_RTIFRC0 != portRTICOMPCTRL )
	{
		xReadbackTestFailed = pdTRUE;
		/* MCDC Test Point: STD_IF "prvSetupTimerInterrupt" */
	}
	/* MCDC Test Point: ADD_ELSE "prvSetupTimerInterrupt" */

	/* Enable the Timer0. */
	portRTIGCTRL |= portRTIGCTRL_ENABLE_BIT;

	/* Test the timer enable bit was set */
	if( ( portRTIGCTRL & portRTIGCTRL_ENABLE_BIT ) != portRTIGCTRL_ENABLE_BIT )
	{
		xReadbackTestFailed = pdTRUE;
		/* MCDC Test Point: STD_IF "prvSetupTimerInterrupt" */
	}
	/* MCDC Test Point: ADD_ELSE "prvSetupTimerInterrupt" */

	/* If any of the read backs have failed report an error */
	if( pdFALSE != xReadbackTestFailed )
	{
		/* MCDC Test Point: STD_IF "prvSetupTimerInterrupt" */
		vPortErrorHook( NULL, "Setup Timer Interrupt Failed", errBAD_OR_NO_TICK_RATE_CONFIGURATION );
	}
	/* MCDC Test Point: ADD_ELSE "prvSetupTimerInterrupt" */
}
/*---------------------------------------------------------------------------*/

#ifdef SAFERTOS_MODULE_TEST
	#include "PortCTestHeaders.h"
	#include "PortCTest.h"
#endif
