/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

#ifndef PORTMACRO_H
#define PORTMACRO_H

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
 * Portable layer version number
 *---------------------------------------------------------------------------*/

/* Portable layer major version number */
#define portPORT_MAJOR_VERSION	( 6 )

/* Portable layer minor version number */
#define portPORT_MINOR_VERSION	( 0 )

/*-----------------------------------------------------------------------------
 * Type definitions
 *---------------------------------------------------------------------------*/

typedef char					portCharType;

typedef signed char				portInt8Type;
typedef signed short			portInt16Type;
typedef signed long				portInt32Type;
typedef signed long long		portInt64Type;

typedef unsigned char			portUInt8Type;
typedef unsigned short			portUInt16Type;
typedef unsigned long			portUInt32Type;
typedef unsigned long long		portUInt64Type;

typedef float					portFloat32Type;
typedef double					portFloat64Type;

typedef unsigned long			portStackType;
typedef signed long				portBaseType;
typedef unsigned long			portUnsignedBaseType;
typedef unsigned long			portTickType;
typedef void *					portTaskHandleType;

#ifndef NULL
	#ifdef __cplusplus
		#define NULL	( 0 )
	#else
		#define NULL	( ( void * ) 0 )
	#endif
#endif


/*-----------------------------------------------------------------------------
 * Program Hook function pointers
 *---------------------------------------------------------------------------*/

typedef void ( *portTASK_DELETE_HOOK )( portTaskHandleType xTaskBeingDeleted );
typedef void ( *portERROR_HOOK )( portTaskHandleType xHandleOfTaskWithError,
								  const portCharType *pcErrorString,
								  portBaseType xErrorCode );
typedef void ( *portIDLE_HOOK )( void );
typedef void ( *portTICK_HOOK )( void );
typedef void ( *portSETUP_TICK_HOOK )( portUInt32Type ulClockHz, portUInt32Type ulRateHz );
typedef portUInt64Type ( *portSVC_HOOK )( portUnsignedBaseType uxArg1, portUnsignedBaseType uxArg2, portUnsignedBaseType uxArg3, portUnsignedBaseType uxArg4 );
typedef void ( *portSSI_HOOK )( portUnsignedBaseType uxSSINumber );


/*-----------------------------------------------------------------------------
 * Architecture specifics
 *---------------------------------------------------------------------------*/

#define portMAX_DELAY					( ( portTickType ) 0xFFFFFFFFU )
#define portMAX_LIST_ITEM_VALUE			( ( portTickType ) 0xFFFFFFFFU )
#define portWORD_ALIGNMENT				( 4U )
#define portWORD_ALIGNMENT_MASK			( ( portUInt32Type )( portWORD_ALIGNMENT - 1U ) )
#define portSTACK_ALIGNMENT				( 8U )
#define portSTACK_ALIGNMENT_MASK		( ( portUInt32Type )( portSTACK_ALIGNMENT - 1U ) )
/* SAFERTOSTRACE PORTQUEUEOVERHEADBYTES */
#define portQUEUE_OVERHEAD_BYTES		( 124U )
/* SAFERTOSTRACE PORTQUEUEOVERHEADBYTESENDIF */
#define portCONTEXT_SIZE_BYTES_NO_FPU	( ( portUInt32Type ) 18U * sizeof( portStackType ) )
#define portCONTEXT_SIZE_BYTES_WITH_FPU	( ( portUInt32Type ) 51U * sizeof( portStackType ) )

#define portSTACK_IN_USE				( ( portStackType ) 0xA5A5A5A5U )
#define portSTACK_NOT_IN_USE			( ( portStackType ) 0x5A5A5A5AU )

#define portTICK_COUNT_NUM_BITS			( sizeof( portTickType ) * 8U )

/* Prevent inlining of functions outside the kernel_func section. */
#define portNO_INLINE_FUNC_DEF			_Pragma( "optimize=no_inline" )

/*-----------------------------------------------------------------------------
 * Scheduler utilities
 *---------------------------------------------------------------------------*/

portBaseType xPortRaisePrivilege( void );
portBaseType xPortIsPrivilegedMode( void );

#define portRAISE_PRIVILEGE()			xPortRaisePrivilege()


#pragma swi_number=0
__swi
extern void portYIELD( void );

#pragma swi_number=1
__swi
extern void portENTER_CRITICAL( void );

#pragma swi_number=2
__swi
extern void portEXIT_CRITICAL( void );

#pragma swi_number=4
__swi
extern void portLOWER_PRIVILEGE( void );

#pragma swi_number=5
__swi
extern void portSTART_FIRST_TASK( void );

/* portRESTORE_PRIVILEGE( xRunningPrivileged ) - used in mpuAPI.c */
#define portRESTORE_PRIVILEGE( xRunningPrivileged )						\
{																		\
	if( pdFALSE == ( xRunningPrivileged ) )								\
	{																	\
		portLOWER_PRIVILEGE();											\
		/* MCDC Test Point: STD_IF_IN_MACRO "portRESTORE_PRIVILEGE" */	\
	}																	\
	/* MCDC Test Point: ADD_ELSE_IN_MACRO "portRESTORE_PRIVILEGE" */	\
}

/* Require this definition to make the below macros work without including intrinsics.h */
extern __ATTRIBUTES unsigned int __iar_builtin_MRC( unsigned __constrange(0,15) coproc, 
												    unsigned __constrange(0,8)  opcode_1, 
													unsigned __constrange(0,15) CRn, 
													unsigned __constrange(0,15) CRm, 
													unsigned __constrange(0,8)  opcode_2 );

/*
 * Macro using an intrinsic to access the user-read-only PID register,
 * which is set to the current task handle by the kernel. This port-
 * specific macro may be used as a lightweight alternative to 
 * xTaskGetCurrentTaskHandle().
 */
#define portGET_CURRENT_TASK_HANDLE_REGISTER()		__iar_builtin_MRC( 15, 0, 13, 0, 3 )

/*
 * Macro using an intrinsic to access the privileged-only PID register,
 * which is used to indicate the invoking SVC number if the SVC hook 
 * function is called by the SVC handler. This register can only be 
 * accessed in privileged mode, and only holds a valid SVC number 
 * until the SVC handler exits.
 */
#define portGET_PRIV_SVC_NUMBER_REGISTER()			__iar_builtin_MRC( 15, 0, 13, 0, 4 )
//__asm volatile {"MRC	p15, #0, R0, C13, C0, #4"} 

/*-----------------------------------------------------------------------------
 * Utilities required by the common task code
 *---------------------------------------------------------------------------*/

/* Define these away for this port as we do not support interrupt nesting. */
#define portSET_INTERRUPT_MASK_FROM_ISR()						( ( portUnsignedBaseType ) 0 )
#define portCLEAR_INTERRUPT_MASK_FROM_ISR( uxOriginalPriority )	( ( void )( uxOriginalPriority ) )

/* The routines to set and clear the interrupt mask are defined in portasm.s. */
void vPortSetInterruptMask( void );
void vPortClearInterruptMask( void );

#define portSET_INTERRUPT_MASK()	vPortSetInterruptMask()
#define portCLEAR_INTERRUPT_MASK()	vPortClearInterruptMask()

void vPortEnterCritical( void );
void vPortExitCritical( void );

/* Critical section management within API. */
#define portENTER_CRITICAL_WITHIN_API()		vPortEnterCritical()
#define portEXIT_CRITICAL_WITHIN_API()		vPortExitCritical()

/* Macro that is conventionally used within an ISR to perform a context
 * switch if required. */
#define portSYS_SSIR1_REG		( * ( ( volatile portUInt32Type * ) 0xFFFFFFB0UL ) )
#define portSYS_SSIR1_SSKEY		( 0x7500UL )

#define portYIELD_IMMEDIATE()									\
{																\
	portSYS_SSIR1_REG = portSYS_SSIR1_SSKEY;					\
	asm( " DSB " );												\
	asm( " ISB " );												\
	/* MCDC Test Point: STD_IN_MACRO "portYIELD_IMMEDIATE" */\
}
/*---------------------------------------------------------------------------*/

#define portYIELD_WITHIN_API()									\
{																\
	portYIELD_IMMEDIATE();										\
	portCLEAR_INTERRUPT_MASK();									\
	/* MCDC Test Point: STD_IN_MACRO "portYIELD_WITHIN_API" */	\
}
/*---------------------------------------------------------------------------*/

#define portYIELD_FROM_ISR( xSwitchRequired )						\
{																	\
	if( pdFALSE != ( xSwitchRequired ) )							\
	{																\
		portYIELD_IMMEDIATE();										\
		/* MCDC Test Point: STD_IF_IN_MACRO "portYIELD_FROM_ISR" */	\
	}																\
	/* MCDC Test Point: ADD_ELSE_IN_MACRO "portYIELD_FROM_ISR" */	\
}
/*---------------------------------------------------------------------------*/

/* These macros are defined away because portTickType is atomic. */
#define portTICK_TYPE_ENTER_CRITICAL()
#define portTICK_TYPE_EXIT_CRITICAL()
#define portTICK_TYPE_SET_INTERRUPT_MASK_FROM_ISR()							( 0U )
#define portTICK_TYPE_CLEAR_INTERRUPT_MASK_FROM_ISR( uxOriginalPriority )	( ( void ) ( uxOriginalPriority ) )
#define portTASK_HANDLE_ENTER_CRITICAL()
#define portTASK_HANDLE_EXIT_CRITICAL()


/*-----------------------------------------------------------------------------
 * Run-Time Statistics management
 *---------------------------------------------------------------------------*/

#define portGET_ELAPSED_CPU_TIME( pulTaskSwitchedInTime, pulElapsedTime )	\
{																			\
portUInt32Type ulTimeCount = configRTS_COUNTER_VALUE;						\
portUInt32Type ulTimeValue = *( pulTaskSwitchedInTime );					\
																			\
	if( ulTimeCount > ulTimeValue )											\
	{																		\
		/* Calculate elapsed time for normal case. */						\
		ulTimeValue = ( ulTimeCount - ulTimeValue );						\
		/* MCDC Test Point: STD_IF_IN_MACRO "portGET_ELAPSED_CPU_TIME" */	\
	}																		\
	else																	\
	{																		\
		/* Calculate elapsed time when the counter has wrapped. */			\
		ulTimeValue = ( configRTS_COUNTER_MAX - ulTimeValue );				\
		ulTimeValue += ( ulTimeCount + 1UL );								\
		/* MCDC Test Point: STD_ELSE_IN_MACRO "portGET_ELAPSED_CPU_TIME" */	\
	}																		\
																			\
	/* Set new time values. */												\
	*( pulElapsedTime ) = ulTimeValue;										\
	*( pulTaskSwitchedInTime ) = ulTimeCount;								\
}
/*---------------------------------------------------------------------------*/

/* This port uses the same timer for run time statistics as is used to generate
 * the tick interrupt, therefore no other timer setup is required. */
#define portRTS_TIMER_INITIALISATION()

/*---------------------------------------------------------------------------*/

/*
 * vPortSetWordAlignedBuffer() is used for zeroing out memory blocks.
 * We know the last parameter is a constant (sizeof) and the middle parameter
 * is 0.
 */
void vPortSetWordAlignedBuffer( void *pvDestination, portUInt32Type ulValue, portUnsignedBaseType uxLength );

/*
 * vPortCopyBytes copies where we do not know if bytes or words.
 * It will check if aligned and sized by words and copy by words, if so, as it
 * is faster.
 */
void vPortCopyBytes( void *pvDestination, const void *pvSource, portUnsignedBaseType uxLength );

/* MCDC Test Point: PROTOTYPE */

/*---------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* PORTMACRO_H */
