/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

/*-----------------------------------------------------------------------------
 * Kernel Functions
 *---------------------------------------------------------------------------*/

	/* Functions that are only used by the kernel are located with the other
	 * privileged functions. */
	RSEG	__kernel_functions__:CODE(2)
	ARM

	PUBLIC vPortMPUEnable
	PUBLIC vPortCacheEnable
	PUBLIC vPortEnableVFP
	PUBLIC vPortMPULoadRegion
	PUBLIC uxPortMPUGetNumberRegions

	PUBLIC vSafeRTOSSVCHandler
	PUBLIC vSafeRTOSSysSoftwareHandler

	PUBLIC vPortSetWordAlignedBuffer
	PUBLIC vPortCopyBytes
	PUBLIC vPortClearInterruptMask
	PUBLIC vPortSetInterruptMask
	PUBLIC vInitialiseReadOnlyUserPidRegister




/*------------------------------------------------------------------------------
 * Global Definitions
 *------------------------------------------------------------------------------*/



/* Declare references for 'C' functions called by this module. */
	EXTERN vTaskSelectNextTask
	EXTERN vTaskStackCheckFailed
	EXTERN vPortControlCheckFailed
	EXTERN vPortSvcHookCheckFailed
	EXTERN vPortSSIHookCheckFailed

/* Declare references for global data accessed by this module. */
	EXTERN	pxCurrentTCB
	EXTERN	ulCriticalNesting
	EXTERN	pxSvcHookFunction
	EXTERN	uxSvcHookMirror
	EXTERN	pxSSIHookFunction
	EXTERN	uxSSIHookMirror

/*-----------------------------------------------------------------------------
 * Constant definitions
 */

#define SW_INTERRUPT_FLAG_REGISTER			( 0xFFFFFFF4 )

#define HIGHEST_SVC							( 0x05 )

#define IRQ_DISABLE_BIT						( 0x80 )
#define ARM_MODE_MASK						( 0x1F )
#define SYSTEM_MODE							( 0x1F )
#define USER_MODE							( 0x10 )
#define INVERSE_OF_USER_MODE				( 0x0F )


#define SVC_OFFSET					( -1 )

#define DREGION_MASK				( 0x0000FF00 )

#define FPU_ACCESS_MASK				( 0xF00000 )
#define FPU_ENABLE_MASK				( 0x40000000 )

#define MPU_ENABLE_LSB_MASK			( 0x01 )
#define MPU_ENABLE_MSB_MASK			( 0x20000 )

/* Offset to calculate the index of the 1st task MPU region. NOTE: it must be equal to (portmpuTASK_REGION_NUM + 2) */
#define FIRST_TASK_REGION_OFFSET	( 9 )

#define L1_CACHE_AVAILABLE_MASK		( 0x30000000 ) /* bits b28(NO_DCACHE) and b29(NO_ICACHE) */

/* Constants used in vPortSetWordAlignedBuffer() and xPortCopyBytes(). */
#define WORD_LENGTH					( 4 )
#define BYTE_LENGTH					( 1 )

#define BIT_0_BIT_1_MASK			( 0x3 )

#define SSI_1_VALUE					( 0x1 )		/* Bits for SSI 1 */
#define SSI_NUM_MASK				( 0x7 )		/* The register will have 1, 2, 3 or 4 */

#define TRUE								( 1 )
#define FALSE								( 0 )
/* Declare offsets into the TCB structure for the elements that are accessed
 * directly by the save and restore context macros. */
#define STACK_LIMIT_TCB_OFFSET				( 4 )
#define USING_FPU_TCB_OFFSET				( 8 )
#define STACK_TOP_MIRROR_TCB_OFFSET			( 12 )
#define MPU_REGIONS_TCB_OFFSET				( 16 )
#define SYSTEM_MODE_SETTING_TCB_OFFSET		( 100 )

/* portRAISE_PRIVILEGE SVC number */
#define SVC_RAISE_PRIV						( 3 )

/*-----------------------------------------------------------------------------
 * portSAVE_CONTEXT macro definition
 */
portSAVE_CONTEXT MACRO
		/* Ensure that all memory accesses and instructions are complete before
		 * saving the context. */
		DSB

		/* Push R0 and R1 onto the IRQ/SVC stack so they can be used to check
		 * the task stack limit. */
		STMDB	SP!, {R0, R1}

		/* Set R0 to point to the task stack pointer. */
		STMDB	SP,{SP}^
		SUB		SP, SP, #4
		LDMIA	SP!,{R0}

		/* Load the stack limit into R1. */
		LDR		R1, =pxCurrentTCB
		LDR		R1, [R1]
		ADD		R1, R1, #STACK_LIMIT_TCB_OFFSET
		LDR		R1, [R1]

		/* Check that the stack is not over the limit. */
		CMP		R1, R0
		BHI		vTaskStackCheckFailed

		/* If we get here then there is enough task stack space to save the task
		 * context (or at least vTaskStackCheckFailed() returned). */

		/* Push the return address onto the stack. */
		STMDB	R0!, {LR}

		/* Now we have saved LR we can use it instead of R0. */
		MOV		LR, R0

		/* Pop R0 and R1 so we can save them onto the task stack. */
		LDMIA	SP!, {R0, R1}

		/* Push all the system mode registers onto the task stack. */
		STMDB	LR, {R0-LR}^
		SUB		LR, LR, #60

		/* Push the SPSR onto the task stack. */
		MRS		R0, SPSR
		STMDB	LR!, {R0}

		/* Push the critical nesting count onto the task stack. */
		LDR		R0, =ulCriticalNesting
		LDR		R0, [R0]
		STMDB	LR!, {R0}

		/* Load pxCurrentTCB. */
		LDR		R0, =pxCurrentTCB
		LDR		R0, [R0]

		/* Is this task using the FPU? */
		LDR		R1, [R0, #USING_FPU_TCB_OFFSET]
		CMP		R1, #FALSE

		/* If this task is using the FPU, push the relevant registers onto the stack. */
		VSTMDBNE	LR!, {D0-D15}
		VMRSNE		R1, FPSCR
		STMDBNE		LR!, {R1}

		/* Store the new top of stack for the task - R0 still points to the
		 * first member of the TCB (top of stack). */
		STR		LR, [R0]

		/* Calculate the bitwise inverse of the new top of stack and save the
		 * top of stack mirror into the TCB. */
		MOV		R1, LR
		MVNS	R1, R1
		ADD		R0, R0, #STACK_TOP_MIRROR_TCB_OFFSET
		STR		R1, [R0]

		ENDM

/*-----------------------------------------------------------------------------
 * portRESTORE_CONTEXT macro definition
 */
portRESTORE_CONTEXT MACRO
		/* Get the start of the new TCB in R0, keep the TCB address in R8 as we
		 * need it later. */
		LDR		R8, =pxCurrentTCB
		LDR		R8, [R8]

		/* Store current TCB to User read-only thread and process ID register. */
		MCR		p15, #0, R8, c13, c0, #3

		/* Stack pointer is the first entry in the TCB */
		LDR		LR, [R8]

		/* Get the MPU regions from the TCB and program the MPU */
		ADD		R0, R8, #MPU_REGIONS_TCB_OFFSET

		MOV		R7, #0						/* Use R7 to disable region before reprogramming. */

		/* Calculate first task region */
		MRC		p15, #0, R1, C0, C0, #4		/* Get the MPU Type Register. */
		AND		R1, R1, #DREGION_MASK		/* Mask off the DRegion field. */
		LSR		R1, R1, #8					/* Shift the DRegion field into the lowest 8 bits. */

		LDMIA	R0!, {R4-R6}				/* Read 1st set of MPU registers */
		SUB		R1, R1, #FIRST_TASK_REGION_OFFSET	/* First task region. */
		MCR		p15, #0, R1, C6, C2, #0		/* Set up region number */
		MCR		p15, #0, R7, C6, C1, #2		/* Disable region before programming */
		MCR		p15, #0, R4, C6, C1, #0		/* Set up base address */
		MCR		p15, #0, R5, C6, C1, #4		/* Set up access permissions */
		MCR		p15, #0, R6, C6, C1, #2		/* Set up size */

		LDMIA	R0!, {R4-R6}				/* Read 2nd set of MPU registers */
		ADD		R1, R1, #1					/* Next region */
		MCR		p15, #0, R1, C6, C2, #0		/* Set up region number */
		MCR		p15, #0, R7, C6, C1, #2		/* Disable region before programming */
		MCR		p15, #0, R4, C6, C1, #0		/* Set up base address */
		MCR		p15, #0, R5, C6, C1, #4		/* Set up access permissions */
		MCR		p15, #0, R6, C6, C1, #2		/* Set up size */

		LDMIA	R0!, {R4-R6}				/* Read 3rd set of MPU registers */
		ADD		R1, R1, #1					/* Next region */
		MCR		p15, #0, R1, C6, C2, #0		/* Set up region number */
		MCR		p15, #0, R7, C6, C1, #2		/* Disable region before programming */
		MCR		p15, #0, R4, C6, C1, #0		/* Set up base address */
		MCR		p15, #0, R5, C6, C1, #4		/* Set up access permissions */
		MCR		p15, #0, R6, C6, C1, #2		/* Set up size */

		LDMIA	R0!, {R4-R6}				/* Read 4th set of MPU registers */
		ADD		R1, R1, #1					/* Next region */
		MCR		p15, #0, R1, C6, C2, #0		/* Set up region number */
		MCR		p15, #0, R7, C6, C1, #2		/* Disable region before programming */
		MCR		p15, #0, R4, C6, C1, #0		/* Set up base address */
		MCR		p15, #0, R5, C6, C1, #4		/* Set up access permissions */
		MCR		p15, #0, R6, C6, C1, #2		/* Set up size */

		LDMIA	R0!, {R4-R6}				/* Read 5th set of MPU registers */
		ADD		R1, R1, #1					/* Next region */
		MCR		p15, #0, R1, C6, C2, #0		/* Set up region number */
		MCR		p15, #0, R7, C6, C1, #2		/* Disable region before programming */
		MCR		p15, #0, R4, C6, C1, #0		/* Set up base address */
		MCR		p15, #0, R5, C6, C1, #4		/* Set up access permissions */
		MCR		p15, #0, R6, C6, C1, #2		/* Set up size */

		LDMIA	R0!, {R4-R6}				/* Read 6th set of MPU registers */
		ADD		R1, R1, #1					/* Next region */
		MCR		p15, #0, R1, C6, C2, #0		/* Set up region number */
		MCR		p15, #0, R7, C6, C1, #2		/* Disable region before programming */
		MCR		p15, #0, R4, C6, C1, #0		/* Set up base address */
		MCR		p15, #0, R5, C6, C1, #4		/* Set up access permissions */
		MCR		p15, #0, R6, C6, C1, #2		/* Set up size */

		LDMIA	R0!, {R4-R6}				/* Read 7th set of MPU registers (task stack). */
		ADD		R1, R1, #1					/* Next region */
		MCR		p15, #0, R1, C6, C2, #0		/* Set up region number */
		MCR		p15, #0, R7, C6, C1, #2		/* Disable region before programming */
		MCR		p15, #0, R4, C6, C1, #0		/* Set up base address */
		MCR		p15, #0, R5, C6, C1, #4		/* Set up access permissions */
		MCR		p15, #0, R6, C6, C1, #2		/* Set up size */

		/* Is this task using the FPU? */
		LDR		R1, [R8, #USING_FPU_TCB_OFFSET]
		CMP		R1, #FALSE

		/* If this task is using the FPU, restore the FPU registers from the task stack. */
		LDMIANE		LR!, {R1}
		VMSRNE		FPSCR, R1
		VLDMIANE	LR!, {D0-D15}

		/* Load the critical nesting depth into the ulCriticalNesting variable. */
		LDR		R0, =ulCriticalNesting
		LDMFD	LR!, {R1}
		STR		R1, [R0]

		/* Get the SPSR from the stack. */
		LDMFD	LR!, {R0}

		/* Critical Nesting Depth > 0 ? */
		CMP		R1, #0

		/* If so, ensure SPSR interrupt disabled */
		ORRNE	R0, R0, #IRQ_DISABLE_BIT	/* Disable IRQ */

		MSR		SPSR_CXSF, R0				/* Write back modified SPSR value */

		/* Check that the restored processor mode matches that stored in the TCB. */
		LDR		R1, [R8, #SYSTEM_MODE_SETTING_TCB_OFFSET]
		AND		R0, R0, #SYSTEM_MODE
		CMP		R0, R1
		/* Call the error hook if the mode register does not have the expected value. */
		BNE		vPortControlCheckFailed

		/* Restore all system mode registers for the task. */
		LDMFD	LR, {R0-R14}^
		NOP

		/* Restore the return address. */
		LDR		LR, [LR, #+60]

		/* Ensure that all memory accesses and instructions are complete before
		 * returning to the restored task. */
		DSB

		/* Return - correcting the offset in the LR to obtain the correct address. */
		SUBS	PC, LR, #4

		ENDM

/*-----------------------------------------------------------------------------
 * SVC Handler
 *	Parameter:
 *  0 = yield
 *  1 = enter critical section
 *  2 = exit critical section
 *  3 = switch to priv mode
 *  4 = switch to user mode
 *  5 = start first task
 *  6+ = call SVC hook
 *---------------------------------------------------------------------------*/

vSafeRTOSSVCHandler:
		STMDB	SP!, {R4, R5, LR}			/* Push used registers (R4, R5 and LR). */
		LDRB	R4, [LR, #SVC_OFFSET]		/* Load the SVC-# into R4 */
		CMP		R4, #HIGHEST_SVC			/* Check for SVC out of Range */
		BHI		_vPortSVC_call_hook			/* Exit Immediately via SVC hook if SVC out of Range */

		LDR		PC, [ PC, R4, LSL#2 ]		/* Dispatch Instruction */
		NOP									/* Exactly 1 Word Between Disp. Instr and Table */
		DC32	_vPortSVC_yield				/* SVC 0x00 - Dispatch Table begins here */
		DC32	_vPortSVC_enter_crit		/* SVC 0x01 */
		DC32	_vPortSVC_exit_crit			/* SVC 0x02 */
		DC32	_vPortSVC_priv_mode			/* SVC 0x03 */
		DC32	_vPortSVC_user_mode			/* SVC 0x04 */
		DC32	_vPortSVC_start_first		/* SVC 0x05 */

_vPortSVC_yield:
		LDMFD	SP!, {R4, R5, LR}			/* Pop saved registers so that the real context can be saved */
		ADD		LR, LR, #4					/* increment return address to mimic ISR operation */
		portSAVE_CONTEXT					/* Save the current task context to the task stack */

		BL		vTaskSelectNextTask			/* Select the next task to execute. */

		portRESTORE_CONTEXT					/* Restore the context of the new task and return */

_vPortSVC_enter_crit:
		MRS		R4, SPSR					/* Get CPSR. */
		ORR		R4, R4, #IRQ_DISABLE_BIT	/* Disable IRQ. */
		MSR		SPSR_CF, R4					/* Write back modified value. */
		LDR		R5, =ulCriticalNesting		/* increment critical nesting */
		LDR		R4, [R5]
		ADD		R4, R4, #1
		STR		R4, [R5]

		B		_vPortSVC_exit

_vPortSVC_exit_crit:
		LDR		R5, =ulCriticalNesting		/* Load critical nesting address */
		LDR		R4, [R5]					/* Load critical nesting */
		CMP		R4, #0						/* If already 0 then exit */
		BEQ		_vPortSVC_exit

		SUB		R4, R4, #1					/* Decrement critical nesting */
		STR		R4, [R5]					/* Write back */
		CMP		R4, #0						/* If 0 then enable ints */
		BNE		_vPortSVC_exit

		MRS		R4, SPSR					/* Get CPSR. */
		BIC		R4, R4, #IRQ_DISABLE_BIT	/* Enable IRQ. */
		MSR		SPSR_CF, R4					/* Write back modified value. */

		B		_vPortSVC_exit

_vPortSVC_priv_mode:
		MRS		R4, SPSR									/* Get CPSR. */
		ORR		R4, R4, #SYSTEM_MODE						/* Set the privilege mode bits. */
		MSR		SPSR_CF, R4									/* Write back modified value. */

		AND		R4, R4, #ARM_MODE_MASK
		LDR		R5, =pxCurrentTCB							/* Get the current TCB */
		LDR		R5, [R5]
		STR		R4, [R5, #SYSTEM_MODE_SETTING_TCB_OFFSET]	/* Store the priv mode setting */

		B		_vPortSVC_exit

_vPortSVC_user_mode:
		MRS		R4, SPSR									/* Get CPSR. */
		BIC		R4, R4, #INVERSE_OF_USER_MODE				/* Clear bits to put us in user mode. */
		MSR		SPSR_CF, R4									/* Write back modified value. */

		AND		R4, R4, #ARM_MODE_MASK
		LDR		R5, =pxCurrentTCB							/* Get the current TCB */
		LDR		R5, [R5]
		STR		R4, [R5, #SYSTEM_MODE_SETTING_TCB_OFFSET]	/* Store the priv mode setting */

		B		_vPortSVC_exit

_vPortSVC_start_first:
		LDMFD	SP!, {R4, R5, LR}			/* Pop registers to keep stack pointer correct. */
		portRESTORE_CONTEXT

_vPortSVC_call_hook:
		/* SVC hook. r0-r3 and r12 will not be preserved across the call.
		 *
		 * A total of four arguments can be passed in r0-r3. A struct return type will consume
		 * one of these four, and each 64bit argument will use a register pair. R0 and R1
		 * can pass scalar return types up to 64bits.
		 *
		 * All other registers will be preserved across the SVC call overall, but r4 will
		 * contain the SVC number, and r5 will contain the hook function address at the
		 * point when the hook function is called. SVC number is also saved into CP15 c0 #4
		 */
		LDR		LR,  =pxSvcHookFunction		/* Get SVC hook address. */
		LDR		LR,  [LR]
		LDR		R5,  =uxSvcHookMirror		/* Get SVC hook mirror. */
		LDR		R5,  [R5]
		MVN		R5,  R5						/* Bitwise 1-complement of the mirror. */
		CMP		LR,  R5						/* Compare hook address with the bitwise 1-complement of its mirror. */
		BNE		vPortSvcHookCheckFailed		/* Call the error hook if they don't match. */

		ADD		SP, SP, #-4 				/* SP should be 8-byte aligned, but we stacked 3 words entry, so adjust SP. */
		CMP		LR, #0						/* If the SVC hook is NULL, we don't need to do anything else. */
		MCRNE   p15, #0, R4, c13, c0, #4    /* Save SVC number to privileged-only PID register (otherwise unused) */
		BLXNE	LR							/* Execute SVC hook, if not NULL. SVC number will be in R4. */
		MOV		R5, #0						/* Use R5 to zero the privileged PID register after use. */
		MCR		p15, #0, R5, c13, c0, #4    /* Clear the privileged PID register (to avoid leaking information across tasks) */
		ADD		SP, SP, #4					/* Restore SP to top of saved R4, R5 and LR. */


_vPortSVC_exit:
		LDMFD	SP!, { R4, R5, PC }^		/* Pop saved R4 and R5, and the LR into the PC to return */


/*-----------------------------------------------------------------------------
 * vSafeRTOSSysSoftwareHandler
 */

vSafeRTOSSysSoftwareHandler

        STMDB	SP!, {R0, R1, LR}			                /* Push used registers (R1, R2 and LR). */
		
		LDR		R0, =SW_INTERRUPT_FLAG_REGISTER             /* Read System Software Interrupt flags. */
		LDR     R0, [R0]
        
        AND     R1, R0, #SSI_NUM_MASK 
        CMP     R1, #SSI_1_VALUE                            /* Check to see if hook should be invoked. */
        BEQ     _vSafeRTOSSysSoftwareHandler_CtxtSw         /* No - do context switch, no need to check the other mask */

        LDR     R1, =pxSSIHookFunction                      /* Test the hook function against its mirror */
        LDR     R1,[R1, #+0]
        LDR     LR, =uxSSIHookMirror
        LDR     LR,[LR, #+0]
        MVNS    LR, LR
        CMP     LR, R1
        BNE      _vSafeRTOSSysSoftwareHandler_ErrorHook     /* Bad hook function = error hook */
        
        CMP      R1,#+0                                     /* Test the hook function against NULL */
        BEQ      _vSafeRTOSSysSoftwareHandler_Exit          /* NULL, dont call the hook. */
        STMDB	 SP!, {R2, R3, R12}							/* Push scratch registers. */
        BLX      R1                                         /* Call SSI hook. Flags are already in R0 */
		LDMFD	 SP!, {R2, R3, R12}							/* Pop scratch registers. */
        
_vSafeRTOSSysSoftwareHandler_Exit
		LDMFD	SP!, {R0, R1, LR}			                /* Pop R0, R1 and the LR into the PC */
		SUBS	PC, LR, #4                                  /* Return - correcting the offset in the LR to obtain the correct address. */

_vSafeRTOSSysSoftwareHandler_ErrorHook    
        BL       vPortSSIHookCheckFailed                    /* Trigger the error hook as bad SSI hook func detected. */

_vSafeRTOSSysSoftwareHandler_CtxtSw
		LDMFD	SP!, {R0, R1, LR}			                /* Restore temp registers. Pop R0, R1 and LR */
        
		portSAVE_CONTEXT                                    /* Save the context of the current task. */

		BL		vTaskSelectNextTask                         /* Select the next task to execute. */

		portRESTORE_CONTEXT                                 /* Restore the context of the task selected to execute. */



/*-----------------------------------------------------------------------------
 * void vPortCopyBytes( void *pvDestination, const void *pvSource, portUnsignedBaseType uxLength )
 */
vPortCopyBytes
		CMP		R2, #0							/* If uxLength is zero we should not attempt to copy any bytes. Semaphores are implemented with queues of data size 0. */
		BEQ		prvCopyBytesFinished
		ORR		R3, R0, R1						/* OR together to see if bottom two bits are 0. */
		ORR		R3, R3, R2						/* OR in length. */
		TST		R3, #BIT_0_BIT_1_MASK			/* Now see if bottom two bits are 0. */
		BNE		prvCopyByte						/* No, copy by bytes. */

prvCopyWord										/* We copy back to front as its simpler. */
		SUBS	R2, R2, #WORD_LENGTH			/* Pre-dec uxLength so last entry. */
		LDR		R3, [R1, R2]					/* Read from pvSource + (uxLength -= 4). */
		STR		R3, [R0, R2]					/* Write pvDestination + uxLength. */
		BNE		prvCopyWord						/* Stop when writing to pvDestination ( uxLength == 0 ). */
		BX		LR								/* Done. */

prvCopyByte										/* Bytes: We copy back to front as its simpler. */
		SUBS	R2, R2, #BYTE_LENGTH			/* Pre-dec uxLength so last entry. */
		LDRB	R3, [R1, R2]					/* Read from pvSource + ( uxLength -= 1 ). */
		STRB	R3, [R0, R2]					/* Write to pvDestination + uxLength. */
		BNE		prvCopyByte						/* Stop when writing to pvDestination ( uxLength == 0 ). */

prvCopyBytesFinished
		BX		LR

/*-----------------------------------------------------------------------------
 * void vPortClearInterruptMask( void )
 */
vPortClearInterruptMask
		MRS		R0, CPSR					/* Get CPSR. */
		BIC		R0, R0, #IRQ_DISABLE_BIT	/* Enable IRQ. */
		MSR		CPSR_CXSF, R0				/* Write back modified value. */

		BX		LR

/*-----------------------------------------------------------------------------
 * void vPortSetInterruptMask( void )
 */
vPortSetInterruptMask
		MRS		R0, CPSR					/* Get CPSR. */
		ORR		R0, R0, #IRQ_DISABLE_BIT	/* Disable IRQ. */
		MSR		CPSR_CXSF, R0				/* Write back modified value. */

		BX		LR


/*-------------------------------------------------------------------------------
 * void vInitialiseReadOnlyUserPidRegister()
 */


vInitialiseReadOnlyUserPidRegister
		MOV		R0, #0
		MCR		p15, #0, R0, c13, c0, #3
		BX		LR
	
/*------------------------------------------------------------------------------
 * void vPortSetWordAlignedBuffer( void *pvDestination, portUInt32Type ulValue, portUnsignedBaseType uxLength )
 */
vPortSetWordAlignedBuffer
prvLoop:									/* We 0 back to front as it's simpler. */
		SUBS	R2, R2, #WORD_LENGTH		/* Pre-dec uxLength so last entry. */
		STR		R1, [R0, R2]				/* Write 0 to pvDestination +( uxLength -= 4 ). */
		BNE		prvLoop						/* Stop when writing to pvDestination ( uxLength == 0 ). */
		BX		LR

/*------------------------------------------------------------------------------
 * vPortMPUEnable
 */
vPortMPUEnable
		MRC		p15, #0, r1, c1, c0, #0
		ORR		r1, r1, #MPU_ENABLE_LSB_MASK		/* Assembler cannot cope with 0x20001 */
		ORR		r1, r1, #MPU_ENABLE_MSB_MASK		/* as an immediate value!! */

		DSB
		MCR		p15, #0, r1, c1, c0, #0
		ISB

		MOV		PC, LR

/*------------------------------------------------------------------------------
 * void vPortCacheEnable( void )
 */
vPortCacheEnable
		/* Check to see if cache is available before enabling */
		MRC		p15, #0, r0, c15, c2, #1			/* Read Build Options 2 Register in to R0 */
		AND		r0, r0, #L1_CACHE_AVAILABLE_MASK
		CMP		r0, #0								/* Is L1 Data and Instruction cache present on this device */
		BNE		vPortCacheEnable_exit				/* If cache is not present then do not attempt to enable it */

		MOV		r0,#0

		MRC		p15, #0, R1, c1, c0, #0				/* Read System Control Register configuration data */
		ORR		R1, R1, #0x1 <<12					/* instruction cache enable */
		ORR		R1, R1, #0x1 <<2					/* data cache enable */
		DSB
		MCR		p15, #0, r0, c15, c5, #0			/* Invalidate entire data cache */
		DSB											/* delay is required, manually added */
		MCR		p15, #0, r0, c7, c5, #0				/* Invalidate entire instruction cache */
		DSB											/* delay is required, manually added */
		MCR		p15, #0, R1, c1, c0, #0				/* enabled cache RAMs */
		ISB
vPortCacheEnable_exit
		 BX		LR

/*------------------------------------------------------------------------------
 * vPortEnableVFP
 */
vPortEnableVFP
		MRC		p15, #0, r0, c1, c0, #2		/* Get the MPU Control Register. */
		ORR		r0, r0, #FPU_ACCESS_MASK	/* Set FPU Access */
		MCR		p15, #0, r0, c1, c0, #2

		MOV		r0, #FPU_ENABLE_MASK		/* Set FPU Enable Operations */
		FMXR	FPEXC, r0

		BX		LR
	
/*------------------------------------------------------------------------------
 * void vPortMPULoadRegion
 * (
 *	R0 = portUInt32Type ulRegion
 *	R1 = portUInt32Type ulBaseAddr
 *	R2 = portUInt32Type ulAccess
 *	R3 = portUInt32Type ulSize
 * )
 */
vPortMPULoadRegion
		MCR		p15, #0, r0, c6, c2, #0
		MCR		p15, #0, r1, c6, c1, #0
		MCR		p15, #0, r2, c6, c1, #4
		MCR		p15, #0, r3, c6, c1, #2

		MOV		PC, LR
		
/*------------------------------------------------------------------------------
 * portUnsignedBaseType uxPortMPUGetNumberRegions( void )
 */
uxPortMPUGetNumberRegions
		MRC		p15, #0, r0, c0, c0, #4		/* Get the MPU Type Register. */
		AND		R0, R0, #DREGION_MASK		/* Mask off the DRegion field. */
		LSR		R0, R0, #8					/* Shift the DRegion field into the lowest 8 bits. */
		BX		LR

/*------------------------------------------------------------------------------
 * Following Functions Have Global Access
 */
	RSEG CODE:CODE(2)
	ARM

	PUBLIC xPortRaisePrivilege
	PUBLIC xPortIsPrivilegedMode
/*-----------------------------------------------------------------------------
 * portBaseType xPortRaisePrivilege( void )
 */
xPortRaisePrivilege
		MRS		R0, CPSR
		AND		R0, R0, #ARM_MODE_MASK
		CMP		R0, #SYSTEM_MODE		/* Is the task running privileged? */

		MOVNE	R0, #FALSE				/* return false. */
		SVCNE	#SVC_RAISE_PRIV			/* switch to privileged. */
		MOVEQ	R0, #TRUE				/* return true. */

		BX		LR

/*-----------------------------------------------------------------------------
 * portBaseType xPortIsPrivilegedMode( void )
 */
xPortIsPrivilegedMode
		MRS		R0, CPSR
		AND		R0, R0, #ARM_MODE_MASK
		CMP		R0, #SYSTEM_MODE		/* Is the task running privileged? */
		MOVNE	R0, #FALSE				/* Return false. */
		MOVEQ	R0, #TRUE				/* Return true. */

		BX		LR

/*---------------------------------------------------------------------------
 * void vPortCheckWriteBytes( void *pvDestination, portUnsignedBaseType uxLength )
 */

vPortCheckWriteBytes
		CMP		R1, #0						/* If uxLength is zero we should not attempt to copy any bytes. Semaphores are implemented with queues of data size 0. */
		BEQ		prvPortCheckWriteBytesFinished
		MOV		R3, #0
		ORR		R2, R0, R1					/* OR in length. */
		TST		R2, #BIT_0_BIT_1_MASK		/* Now see if bottom two bits are 0. */
		BNE		prvCheckWriteByte			/* No, copy by bytes. */

prvCheckWriteWord							/* Words: we write back to front as it's simpler. */
		SUBS	R1, R1, #WORD_LENGTH		/* Pre-dec uxLength so last entry. */
		STR		R3, [R0, R1]				/* Write to pvDestination + uxLength. */
		BNE		prvCheckWriteWord			/* Stop when writing to pvDestination (uxLength == 0). */

		BX		LR							/* Done. */

prvCheckWriteByte							/* Bytes: we write back to front as it's simpler. */
		SUBS	R1, R1, #BYTE_LENGTH		/* Pre-dec uxLength so last entry. */
		STRB	R3, [R0, R1]				/* Write to pvDestination + uxLength. */
		BNE		prvCheckWriteByte			/* Stop when writing to pvDestination (uxLength == 0). */

prvPortCheckWriteBytesFinished
		BX		LR

/*------------------------------------------------------------------------------
 * void vPortCheckReadBytes( const void *pvSource, portUnsignedBaseType uxLength )
 */
vPortCheckReadBytes
		CMP		R1, #0						/* If uxLength is zero we should not attempt to copy any bytes. Semaphores are implemented with queues of data size 0. */
		BEQ		prvPortCheckReadBytesFinished
		ORR		R2, R0, R1					/* OR in length. */
		TST		R2, #BIT_0_BIT_1_MASK		/* Now see if bottom two bits are 0. */
		BNE		prvCheckReadByte			/* No, copy by bytes. */

prvCheckReadWord							/* Words: we read back to front as it's simpler. */
		SUBS	R1, R1, #WORD_LENGTH		/* Pre-dec uxLength so last entry. */
		LDR		R3, [R0, R1]				/* Read from pvSource + uxLength. */
		BNE		prvCheckReadWord			/* Stop when reading from pvSource (uxLength == 0). */

		BX		LR							/* Done. */

prvCheckReadByte							/* Bytes: We read back to front as its simpler. */
		SUBS	R1, R1, #BYTE_LENGTH		/* Pre-dec uxLength so last entry. */
		LDRB	R3, [R0, R1]				/* Read from pvSource + uxLength. */
		BNE		prvCheckReadByte			/* Stop when reading from pvSource (uxLength == 0). */

prvPortCheckReadBytesFinished
		BX		LR

	END

