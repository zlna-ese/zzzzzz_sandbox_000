/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

#ifndef PORTMPU_H
#define PORTMPU_H

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
 * Public Port MPU Macros
 *---------------------------------------------------------------------------*/

/* Kernel Function area definitions ARE required for this Port. */
#define portmpuKERNEL_FUNC_DEF			_Pragma("location=\"__kernel_functions__\"")
#define portmpuKERNEL_DATA_DEF			_Pragma("location=\"__kernel_data__\"")


/*-----------------------------------------------------------------------------
 * Public Port MPU Constants
 *---------------------------------------------------------------------------*/

/* MPU region actual byte size limits. */
#define portmpuSMALLEST_REGION_SIZE_ACTUAL		( 32U )
#define portmpuLARGEST_REGION_SIZE_ACTUAL		( 0x80000000U )

/* MPU configurable system region identifiers - These are the actual region numbers */
#define portmpuUNPRIVILEGED_FLASH_REGION		( 0U )

/* Indexes used by the common mpuArm code.
 * NOTE: they are needed for compatibility with the common files, but their
 * value has no effect. */
#define portmpuCONFIGURABLE_REGION_FIRST		( 1UL )	/* NOT USED IN THIS PORT */
#define portmpuSTACK_REGION						( 9UL )	/* NOT USED IN THIS PORT */
#define portmpuGLOBAL_CONFIGURABLE_REGION_FIRST	( 1UL )
#define portmpuGLOBAL_CONFIGURABLE_REGION_LAST	( uxPortMPUGetNumberRegions() - 10U ) 

/* MPU configuration system region lookup table indexes - Used as ulRegionIdentifier in calls to vPortMPUConfigureSystemRegion.
 * (The actual region number depends of the number of MPU regions the processor supports) */
#define portmpuUNPRIVILEGED_FLASH_REGION_LOOKUP	( 0xFFFFFFFFUL )
#define portmpuKERNEL_FLASH_REGION_LOOKUP		( 0xFFFFFFFEUL )
#define portmpuKERNEL_RAM_REGION_LOOKUP			( 0xFFFFFFFDUL )

/* MPU configurable task regions (LAST - FIRST + 1). */
#define portmpuCONFIGURABLE_REGION_NUM			( 6UL )

/* MPU configurable Task regions plus 1 for task stack region.*/
#define portmpuTASK_REGION_NUM					( portmpuCONFIGURABLE_REGION_NUM + 1UL )

/* The minimum number of supported regions. */
#define portmpuMINIMUM_SUPPORTED_REGIONS		( 12UL )

/*
 * Constants used to configure the region attributes.
 */

/* Region Share enable bit */
#define portmpuREGION_SHAREABLE					( 0x01UL << 2U )

/* Instruction Executable access disable bit */
#define portmpuREGION_EXECUTE_NEVER				( 0x01UL << 12U )

/* Sub-Region Disable definitions */
#define portmpuREGION_1ST_SUB_REGION_DISABLE	( 0x01UL <<  8U )
#define portmpuREGION_2ND_SUB_REGION_DISABLE	( 0x01UL <<  9U )
#define portmpuREGION_3RD_SUB_REGION_DISABLE	( 0x01UL << 10U )
#define portmpuREGION_4TH_SUB_REGION_DISABLE	( 0x01UL << 11U )
#define portmpuREGION_5TH_SUB_REGION_DISABLE	( 0x01UL << 12U )
#define portmpuREGION_6TH_SUB_REGION_DISABLE	( 0x01UL << 13U )
#define portmpuREGION_7TH_SUB_REGION_DISABLE	( 0x01UL << 14U )
#define portmpuREGION_8TH_SUB_REGION_DISABLE	( 0x01UL << 15U )

/* Region Data Access Permission field bit definitions */
#define portmpuREGION_PRIVILEGED_NO_ACCESS_USER_NO_ACCESS		( 0x00UL << 8U )
#define portmpuREGION_PRIVILEGED_READ_WRITE_USER_NO_ACCESS		( 0x01UL << 8U )
#define portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_ONLY		( 0x02UL << 8U )
#define portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE		( 0x03UL << 8U )
#define portmpuREGION_PRIVILEGED_READ_ONLY_USER_NO_ACCESS		( 0x05UL << 8U )
#define portmpuREGION_PRIVILEGED_READ_ONLY_USER_READ_ONLY		( 0x06UL << 8U )

/* Region Type extension, Cached and Buffered field definitions */
#define portmpuREGION_STRONGLY_ORDERED															( 0x00UL )
#define portmpuREGION_SHARED_DEVICE																( 0x01UL )
#define portmpuREGION_OUTER_AND_INNER_WRITE_THROUGH_NO_WRITE_ALLOCATE							( 0x02UL )
#define portmpuREGION_OUTER_AND_INNER_WRITE_BACK_NO_WRITE_ALLOCATE								( 0x03UL )
#define portmpuREGION_OUTER_AND_INNER_NONCACHEABLE												( 0x08UL )
#define portmpuREGION_OUTER_AND_INNER_WRITE_BACK_WRITE_AND_WRITE_ALLOCATE						( 0x0BUL )
#define portmpuREGION_NONSHARED_DEVICE															( 0x10UL )
#define portmpuREGION_OUTER_NONCACHEABLE_INNER_WRITE_BACK_WRITE_ALLOCATE						( 0x21UL )
#define portmpuREGION_OUTER_NONCACHEABLE_INNER_WRITE_THROUGH_NO_WRITE_ALLOCATE					( 0x22UL )
#define portmpuREGION_OUTER_NONCACHEABLE_INNER_WRITE_BACK_NO_WRITE_ALLOCATE						( 0x23UL )
#define portmpuREGION_OUTER_WRITE_BACK_WRITE_ALLOCATE_INNER_NONCACHEABLE						( 0x28UL )
#define portmpuREGION_OUTER_WRITE_BACK_WRITE_ALLOCATE_INNER_WRITE_THROUGH_NO_WRITE_ALLOCATE		( 0x2AUL )
#define portmpuREGION_OUTER_WRITE_BACK_WRITE_ALLOCATE_INNER_WRITE_BACK_NO_WRITE_ALLOCATE		( 0x2BUL )
#define portmpuREGION_OUTER_WRITE_THROUGH_NO_WRITE_ALLOCATE_INNER_NONCACHEABLE					( 0x30UL )
#define portmpuREGION_OUTER_WRITE_THROUGH_NO_WRITE_ALLOCATE_INNER_WRITE_BACK_WRITE_ALLOCATE		( 0x31UL )
#define portmpuREGION_OUTER_WRITE_THROUGH_NO_WRITE_ALLOCATE_INNER_WRITE_BACK_NO_WRITE_ALLOCATE	( 0x33UL )
#define portmpuREGION_OUTER_WRITE_BACK_NO_WRITE_ALLOCATE_INNER_NONCACHEABLE						( 0x38UL )
#define portmpuREGION_OUTER_WRITE_BACK_NO_WRITE_ALLOCATE_INNER_WRITE_BACK_WRITE_ALLOCATE		( 0x39UL )
#define portmpuREGION_OUTER_WRITE_BACK_NO_WRITE_ALLOCATE_INNER_WRITE_THROUGH_NO_WRITE_ALLOCATE	( 0x3AUL )

/* These define the default cache policies for the memory regions as used by the default memory map */
#define portmpuREGION_INTERNAL_FLASH_DEFAULT_CACHE_POLICY	( portmpuREGION_OUTER_AND_INNER_WRITE_THROUGH_NO_WRITE_ALLOCATE )
#define portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY		( portmpuREGION_OUTER_AND_INNER_WRITE_BACK_WRITE_AND_WRITE_ALLOCATE )
#define portmpuREGION_PERIPHERAL_DEFAULT_CACHE_POLICY		( portmpuREGION_STRONGLY_ORDERED )

/* Attributes for every task stack region. */
#define portmpuTASK_STACK_ATTRIBUTES	( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE | \
										  portmpuREGION_EXECUTE_NEVER | \
										  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY )


/*-----------------------------------------------------------------------------
 * Public Port MPU Types
 *---------------------------------------------------------------------------*/

/* MPU region register settings - holds actual MPU region values. */
typedef struct portmpuRegionRegisters
{
	portUInt32Type ulRegionBaseAddress;
	portUInt32Type ulRegionAttribute;
	portUInt32Type ulRegionSize;

} portmpuRegionRegistersType;


/*-----------------------------------------------------------------------------
 * Public Port MPU External Variables
 *---------------------------------------------------------------------------*/

/* Kernel Function & Kernel Data region boundaries.
 * These are imported from the linker command file. */
extern portUInt32Type __ICFEDIT_intvec_start__;
extern portUInt32Type __ICFEDIT_region_ROM_start__;
extern portUInt32Type __ICFEDIT_region_ROM_end__;

extern portUInt32Type __kernel_functions_block__$$Limit;

extern portUInt32Type __kernel_data_block__$$Base;
extern portUInt32Type __kernel_data_block__$$Limit;


/*-----------------------------------------------------------------------------
 * Public Port MPU Variables
 *---------------------------------------------------------------------------*/

/* System MPU configurable region definitions. */
#define portmpuREGION_SYS_CONFIGURATION						\
{															\
	portmpuUNPRIVILEGED_FLASH_REGION_LOOKUP,				\
 	( portUInt32Type ) &__ICFEDIT_region_ROM_start__,		\
	( portUInt32Type ) &__ICFEDIT_region_ROM_end__,			\
	( portmpuREGION_PRIVILEGED_READ_ONLY_USER_READ_ONLY |	\
	  portmpuREGION_INTERNAL_FLASH_DEFAULT_CACHE_POLICY		\
	),														\
	0U														\
},															\
{															\
	portmpuKERNEL_FLASH_REGION_LOOKUP,						\
	( portUInt32Type ) &__ICFEDIT_intvec_start__,			\
	( portUInt32Type ) &__kernel_functions_block__$$Limit,	\
	( portmpuREGION_PRIVILEGED_READ_ONLY_USER_NO_ACCESS |	\
	  portmpuREGION_INTERNAL_FLASH_DEFAULT_CACHE_POLICY		\
	),														\
	0U														\
},															\
{															\
 	portmpuKERNEL_RAM_REGION_LOOKUP,						\
	( portUInt32Type ) &__kernel_data_block__$$Base,		\
	( portUInt32Type ) &__kernel_data_block__$$Limit,		\
	( portmpuREGION_PRIVILEGED_READ_WRITE_USER_NO_ACCESS |	\
	  portmpuREGION_EXECUTE_NEVER |							\
	  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY		\
	),														\
	0U														\
}


/*-----------------------------------------------------------------------------
 * Public Port MPU Functions
 *---------------------------------------------------------------------------*/

KERNEL_FUNCTION void vPortMPUEnable( void );

KERNEL_FUNCTION void vPortMPUConfigureSystemRegion( portUInt32Type ulRegionIdentifier,
													portUInt32Type ulRegionBeginAddress,
													portUInt32Type ulRegionAccess,
													portUInt32Type ulRegionSize,
													portUInt32Type ulSubRegionDisable );

KERNEL_FUNCTION void vPortMPUEncodeTCBRegion( portmpuRegionRegistersType *pxPortMPURegion,
											  portUInt32Type ulRegionIdentifier,
											  portUInt32Type ulBaseAddress,
											  portUInt32Type ulAccessPermissions,
											  portUInt32Type ulLengthInBytes,
											  portUInt32Type ulSubRegionDisable );

KERNEL_FUNCTION void vPortMPUCreateTCBMirrorRegion( const portmpuRegionRegistersType *pxPortMPURegion,
													portmpuRegionRegistersType *pxPortMPUMirror );

/* uxPortMPUGetNumberRegions() is defined within portasm.s. */
portUnsignedBaseType uxPortMPUGetNumberRegions( void );


/*---------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* PORTMPU_H */
