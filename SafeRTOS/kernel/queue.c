/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/


#define QUEUE_C

/* Scheduler includes. */
#define KERNEL_SOURCE_FILE
#include "SafeRTOS.h"
#include "task.h"

#include "queue.h"

/* Constants used with the xRxLock and xTxLock structure members. */
#define queueUNLOCKED			( ( portBaseType ) -1 )
#define queueLOCKED_UNMODIFIED	( ( portBaseType ) 0 )

#define queueQUEUE_IS_MUTEX		( 0xA5A5A5A5U )
#define queueQUEUE_IS_QUEUE		( ~0xA5A5A5A5U )

#define	queueSEND_TO_BACK		( ( portBaseType ) 0 )
#define	queueSEND_TO_FRONT		( ( portBaseType ) 1 )

/* SAFERTOSTRACE QUEUETYPEBASE */

/*
 * Definition of the queue used by the scheduler.
 * Items are queued by copy, not reference.
 */
typedef struct QueueDefinition
{
	portInt8Type *pcHead;						/* Points to the beginning of the queue storage area. */
	portInt8Type *pcTail;						/* Points to the byte at the end of the queue storage area. */

	portInt8Type *pcWriteTo;					/* Points to the free next place in the storage area. */
	portInt8Type *pcReadFrom;					/* Points to the last place that a queued item was read from. */

	xList xTasksWaitingToSend;					/* List of tasks that are blocked waiting to post onto this queue.  Stored in priority order. */
	xList xTasksWaitingToReceive;				/* List of tasks that are blocked waiting to read from this queue.  Stored in priority order. */

	portUnsignedBaseType uxItemsWaiting;		/* The number of items currently in the queue. */
	portUnsignedBaseType uxMaxNumberOfItems;	/* The length of the queue defined as the number of items it will hold, not the number of bytes. */
	portUnsignedBaseType uxItemSize;			/* The size of each item that the queue will hold. */

	portBaseType xRxLock;						/* Set to queueUNLOCKED when the queue is not locked, or notes whether or not the queue has been accessed if the queue is locked. */
	portBaseType xTxLock;						/* Set to queueUNLOCKED when the queue is not locked, or notes whether or not the queue has been accessed if the queue is locked. */

	portUnsignedBaseType uxQueueType;			/* Indicates whether the queue is a mutex or just a regular queue. */
	portUnsignedBaseType uxMutexRecursiveDepth;	/* Indicates the depth of recursive calls to a mutex. */
	xListItem xMutexHolder;						/* A list item so that tasks can keep a record of multiple mutexes held. */

	portUnsignedBaseType uxHeadMirror;			/* The uxHeadMirror will be set to the bitwise inverse (XOR) of the value assigned to pcHead. */

	/* SAFERTOSTRACE QUEUEDEFINITION */
} xQUEUE;
/*-----------------------------------------------------------*/

/*
 * Since xQueueSend and xQueueGiveMutex require essentially the same
 * functionality, the API functions are redirected to prvQueueSendDataToQueue.
 */
KERNEL_FUNCTION static portBaseType prvQueueSendDataToQueue( xQUEUE * const pxQueue, const void * const pvItemToQueue, portTickType xTicksToWait, const portBaseType xPosition );

/*
 * FromISR version of prvQueueSendDataToQueue().
 */
KERNEL_FUNCTION static portBaseType prvQueueSendDataToQueueFromISR( xQUEUE * const pxQueue, const void * const pvItemToQueue, const portBaseType xPosition, portBaseType *pxHigherPriorityTaskWoken );

/*
 * Since xQueueReceive, xQueuePeek and xQueueTakeMutex require essentially the
 * same functionality, the API functions are redirected to prvReceiveDataFromQueue.
 */
KERNEL_FUNCTION static portBaseType prvReceiveDataFromQueue( xQUEUE *pxQueue, void * const pvBuffer, portTickType xTicksToWait, portBaseType xRemoveDataFromQueue );

/*
 * Checks the validity of all the parameters passed to the xQueueCreate
 * function.  Returns a negative number if any parameter is incorrect.
 */
KERNEL_FUNCTION static portBaseType prvCheckQueueCreateParameters( portInt8Type *pcQueueMemory, portUnsignedBaseType uxBufferLength, portUnsignedBaseType uxQueueLength, portUnsignedBaseType uxItemSize, const xQueueHandle *pxQueue );

/*
 * Unlocks a queue locked by a call to prvLOCK_QUEUE.  Locking a queue does not
 * prevent an ISR from adding or removing items to the queue, but does prevent
 * an ISR from removing tasks from the queue event lists.  If an ISR finds a
 * queue is locked it will instead set the appropriate queue lock to MODIFIED
 * to indicate that a task may require unblocking.  When the queue in unlocked
 * these lock variables are inspected, and the appropriate action taken.
 */
KERNEL_FUNCTION static void prvUnlockQueue( xQueueHandle xQueue );

/*
 * Uses a critical section to determine if there is any data in a queue.
 *
 * @return pdTRUE if the queue contains no items, otherwise pdFALSE.
 */
KERNEL_FUNCTION static portBaseType prvIsQueueEmpty( const xQueueHandle xQueue );

/*
 * Uses a critical section to determine if there is any space in a queue.
 *
 * @return pdTRUE if there is no space, otherwise pdFALSE;
 */
KERNEL_FUNCTION static portBaseType prvIsQueueFull( const xQueueHandle xQueue );

/*
 * Function that copies an item into the front of a queue.
 * This is done by copying the item byte for byte, not by reference.
 * Updates the queue state to ensure its integrity after the copy.
 */
KERNEL_FUNCTION static void prvCopyDataToQueueFront( xQUEUE * const pxQueue, const void * const pvItemToQueue );

/*
 * Function that copies an item into the front of a queue.
 * This is done by copying the item byte for byte, not by reference.
 * Updates the queue state to ensure its integrity after the copy.
 */
KERNEL_FUNCTION static void prvCopyDataToQueueBack( xQUEUE * const pxQueue, const void * const pvItemToQueue );

/*
 * Function that copies an item from a queue into a buffer.
 * This is done by copying the item byte for byte, not by reference.
 * Updates the queue state to insure its integrity after the copy.
*/
KERNEL_FUNCTION static void prvCopyDataFromQueue( xQUEUE * const pxQueue, void * const pvBuffer );

/*
 * Macro that checks whether the queue is valid or not. This is done by checking
 * the one's complement of uxHeadMirror against pcHead in the queue.
 */
#define prvIS_QUEUE_VALID( pxQueue )	\
	( ( portBaseType )( ( ( portUnsignedBaseType )( ( pxQueue )->pcHead ) ) == ~( ( pxQueue )->uxHeadMirror ) ) )

/* MCDC Test Point: PROTOTYPE */

/*-----------------------------------------------------------*/

/*
 * Macro to mark a queue as locked.  Locking a queue prevents an ISR from
 * accessing the queue event lists.  Only set it to locked if it is currently
 * unlocked as it may already be locked and modified.
 */
#define prvLOCK_QUEUE( pxQueue )									\
	portENTER_CRITICAL_WITHIN_API();								\
	{																\
		if( queueUNLOCKED == ( pxQueue )->xRxLock )					\
		{															\
			( pxQueue )->xRxLock = queueLOCKED_UNMODIFIED;			\
																	\
			/* MCDC Test Point: STD_IF_IN_MACRO "prvLOCK_QUEUE" */	\
		}															\
		/* MCDC Test Point: ADD_ELSE_IN_MACRO "prvLOCK_QUEUE" */	\
																	\
		if( queueUNLOCKED == ( pxQueue )->xTxLock )					\
		{															\
			( pxQueue )->xTxLock = queueLOCKED_UNMODIFIED;			\
																	\
			/* MCDC Test Point: STD_IF_IN_MACRO "prvLOCK_QUEUE" */	\
		}															\
		/* MCDC Test Point: ADD_ELSE_IN_MACRO "prvLOCK_QUEUE" */	\
	}																\
	portEXIT_CRITICAL_WITHIN_API()

/*-----------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xQueueCreate( portInt8Type *pcQueueMemory,
										   portUnsignedBaseType uxBufferLength,
										   portUnsignedBaseType uxQueueLength,
										   portUnsignedBaseType uxItemSize,
										   xQueueHandle *pxQueue )
{
portBaseType xReturn;
/* Cast param 'xQueueHandle* pxQueue' to xQUEUE** type. */
xQUEUE **ppxQueue = ( xQUEUE ** ) pxQueue;

	xReturn = prvCheckQueueCreateParameters( pcQueueMemory, uxBufferLength, uxQueueLength, uxItemSize, pxQueue );

	if( pdPASS == xReturn )
	{
		*ppxQueue = ( xQUEUE * ) pcQueueMemory;

		portENTER_CRITICAL_WITHIN_API();
		{
			if( prvIS_QUEUE_VALID( *ppxQueue ) != pdFALSE )
			{
				xReturn = errQUEUE_ALREADY_IN_USE;

				/* MCDC Test Point: STD_IF "xQueueCreate" */
			}
			else
			{
				/* Initialise the queue members as described above where the
				 * queue type is defined. */
				( *ppxQueue )->pcHead = pcQueueMemory + sizeof( xQUEUE );
				( *ppxQueue )->pcTail = ( *ppxQueue )->pcHead + ( uxQueueLength * uxItemSize );
				( *ppxQueue )->uxItemsWaiting = ( portUnsignedBaseType ) 0U;
				( *ppxQueue )->pcWriteTo = ( *ppxQueue )->pcHead;
				( *ppxQueue )->pcReadFrom = ( *ppxQueue )->pcHead + ( ( uxQueueLength - 1U ) * uxItemSize );
				( *ppxQueue )->uxMaxNumberOfItems = uxQueueLength;
				( *ppxQueue )->uxItemSize = uxItemSize;
				( *ppxQueue )->xRxLock = queueUNLOCKED;
				( *ppxQueue )->xTxLock = queueUNLOCKED;
				( *ppxQueue )->uxQueueType = queueQUEUE_IS_QUEUE;

				/* These elements are only used if the queue is a mutex, however
				 * ensure that they are initialised anyway. */
				( *ppxQueue )->uxMutexRecursiveDepth = 0U;
				vListInitialiseItem( &( ( *ppxQueue )->xMutexHolder ) );
				listSET_LIST_ITEM_OWNER( &( ( *ppxQueue )->xMutexHolder ), *ppxQueue );

				/* Likewise ensure the event lists start with the correct state. */
				vListInitialise( &( ( *ppxQueue )->xTasksWaitingToSend ) );
				vListInitialise( &( ( *ppxQueue )->xTasksWaitingToReceive ) );

				/* The mutex inheritance mechanism requires a reverse look up
				 * so set the owner of the end marker to the queue itself. */
				listSET_LIST_ITEM_OWNER( &( ( *ppxQueue )->xTasksWaitingToSend.xListEnd ), *ppxQueue );
				listSET_LIST_ITEM_OWNER( &( ( *ppxQueue )->xTasksWaitingToReceive.xListEnd ), *ppxQueue );

				/* Finally, Set Queue Head Memory Check Mirror Value */
				( *ppxQueue )->uxHeadMirror = ~( ( portUnsignedBaseType )( *ppxQueue )->pcHead );

				/* SAFERTOSTRACE QUEUECREATE */

				/* MCDC Test Point: STD_ELSE "xQueueCreate" */
			}
		}
		portEXIT_CRITICAL_WITHIN_API();
	}
	/* MCDC Test Point: ADD_ELSE "xQueueCreate" */

	/* SAFERTOSTRACE QUEUECREATEFAILED */

	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xQueueSend( xQueueHandle xQueue, const void * const pvItemToQueue, portTickType xTicksToWait )
{
portBaseType xReturn = pdPASS;
/* Cast param 'xQueueHandle xQueue' to 'xQUEUE *' type. */
xQUEUE *pxQueue = ( xQUEUE * ) xQueue;

	if( xTaskIsSchedulerSuspended() != pdFALSE )
	{
		xReturn = errSCHEDULER_IS_SUSPENDED;

		/* MCDC Test Point: STD_IF "xQueueSend" */
	}
	else if( NULL == pxQueue )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueSend" */
	}
	else if( prvIS_QUEUE_VALID( pxQueue ) == pdFALSE )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueSend" */
	}
	else if( queueQUEUE_IS_QUEUE != pxQueue->uxQueueType )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueSend" */
	}
	else if( ( NULL == pvItemToQueue ) && ( 0U != pxQueue->uxItemSize ) )
	{
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: NULL (Separate expansion) */
	}
	else
	{
		xReturn = prvQueueSendDataToQueue( pxQueue, pvItemToQueue, xTicksToWait, queueSEND_TO_BACK );

		/* MCDC Test Point: STD_ELSE "xQueueSend" */
	}
	/* MCDC Test Point: ADD_IF_WRAPPER_BEGIN "( NULL != pxQueue )" */
	/* MCDC Test Point: EXP_IF_AND "xQueueSend" "( NULL == pvItemToQueue )" "( 0U != pxQueue->uxItemSize )" "Deviate: FF" */
	/* MCDC Test Point: ADD_IF_WRAPPER_END "( NULL != pxQueue )" */

	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xQueueSendToFront( xQueueHandle xQueue, const void * const pvItemToQueue, portTickType xTicksToWait )
{
portBaseType xReturn = pdPASS;
/* Cast param 'xQueueHandle xQueue' to 'xQUEUE *' type. */
xQUEUE *pxQueue = ( xQUEUE * ) xQueue;

	if( xTaskIsSchedulerSuspended() != pdFALSE )
	{
		xReturn = errSCHEDULER_IS_SUSPENDED;

		/* MCDC Test Point: STD_IF "xQueueSendToFront" */
	}
	else if( NULL == pxQueue )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueSendToFront" */
	}
	else if( prvIS_QUEUE_VALID( pxQueue ) == pdFALSE )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueSendToFront" */
	}
	else if( queueQUEUE_IS_QUEUE != pxQueue->uxQueueType )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueSendToFront" */
	}
	else if( ( NULL == pvItemToQueue ) && ( 0U != pxQueue->uxItemSize ) )
	{
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: NULL (Separate expansion) */
	}
	else
	{
		xReturn = prvQueueSendDataToQueue( pxQueue, pvItemToQueue, xTicksToWait, queueSEND_TO_FRONT );

		/* MCDC Test Point: STD_ELSE "xQueueSendToFront" */
	}
	/* MCDC Test Point: ADD_IF_WRAPPER_BEGIN "( NULL != pxQueue )" */
	/* MCDC Test Point: EXP_IF_AND "xQueueSendToFront" "( NULL == pvItemToQueue )" "( 0U != pxQueue->uxItemSize )" "Deviate: FF" */
	/* MCDC Test Point: ADD_IF_WRAPPER_END "( NULL != pxQueue )" */

	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xQueueSendFromISR( xQueueHandle xQueue, const void * const pvItemToQueue, portBaseType *pxHigherPriorityTaskWoken )
{
portBaseType xReturn;
/* Cast param 'xQueueHandle xQueue' to xQUEUE* type. */
xQUEUE *pxQueue = ( xQUEUE * ) xQueue;

	/* Similar to xQueueSend, except we don't block if there is no room
	 * in the queue. Also we don't directly wake a task that was blocked on a
	 * queue read, instead we return a flag to say whether a context switch is
	 * required or not (i.e. has a task with a higher priority than us been
	 * woken by this post). */
	if( NULL == pxQueue )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_IF "xQueueSendFromISR" */
	}
	else if( prvIS_QUEUE_VALID( pxQueue ) == pdFALSE )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueSendFromISR" */
	}
	else if( queueQUEUE_IS_QUEUE != pxQueue->uxQueueType )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueSendFromISR" */
	}
	else if( ( NULL == pvItemToQueue ) && ( 0U != pxQueue->uxItemSize ) )
	{
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: NULL (expanded elsewhere) "xQueueSendFromISR" */
	}
	else if( NULL == pxHigherPriorityTaskWoken )
	{
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_ELSE_IF "xQueueSendFromISR" */
	}
	else
	{
		xReturn = prvQueueSendDataToQueueFromISR( xQueue, pvItemToQueue, queueSEND_TO_BACK, pxHigherPriorityTaskWoken );

		/* MCDC Test Point: STD_ELSE "xQueueSendFromISR" */
	}
	/* SAFERTOSTRACE QUEUESENDFROMISRFAILED */

	/* MCDC Test Point: ADD_IF_WRAPPER_BEGIN "( NULL != pxQueue )" */
	/* MCDC Test Point: EXP_IF_AND "xQueueSendFromISR" "( NULL == pvItemToQueue )" "( 0U != pxQueue->uxItemSize )" "Deviate: FF" */
	/* MCDC Test Point: ADD_IF_WRAPPER_END "( NULL != pxQueue )" */

	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xQueueSendToFrontFromISR( xQueueHandle xQueue, const void * const pvItemToQueue, portBaseType *pxHigherPriorityTaskWoken )
{
portBaseType xReturn;
/* Cast param 'xQueueHandle xQueue' to xQUEUE* type. */
xQUEUE *pxQueue = ( xQUEUE * ) xQueue;

	/* Similar to xQueueSendToFront, except we don't block if there is no room
	 * in the queue. Also we don't directly wake a task that was blocked on a
	 * queue read, instead we return a flag to say whether a context switch is
	 * required or not (i.e. has a task with a higher priority than us been
	 * woken by this post). */
	if( NULL == pxQueue )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_IF "xQueueSendToFrontFromISR" */
	}
	else if( prvIS_QUEUE_VALID( pxQueue ) == pdFALSE )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueSendToFrontFromISR" */
	}
	else if( queueQUEUE_IS_QUEUE != pxQueue->uxQueueType )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueSendToFrontFromISR" */
	}
	else if( ( NULL == pvItemToQueue ) && ( 0U != pxQueue->uxItemSize ) )
	{
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: NULL (expanded elsewhere) "xQueueSendToFrontFromISR" */
	}
	else if( NULL == pxHigherPriorityTaskWoken )
	{
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_ELSE_IF "xQueueSendToFrontFromISR" */
	}
	else
	{
		xReturn = prvQueueSendDataToQueueFromISR( xQueue, pvItemToQueue, queueSEND_TO_FRONT, pxHigherPriorityTaskWoken );

		/* MCDC Test Point: STD_ELSE "xQueueSendToFrontFromISR" */
	}
	/* SAFERTOSTRACE QUEUESENDFROMISRFAILED */

	/* MCDC Test Point: ADD_IF_WRAPPER_BEGIN "( NULL != pxQueue )" */
	/* MCDC Test Point: EXP_IF_AND "xQueueSendToFrontFromISR" "( NULL == pvItemToQueue )" "( 0U != pxQueue->uxItemSize )" "Deviate: FF" */
	/* MCDC Test Point: ADD_IF_WRAPPER_END "( NULL != pxQueue )" */

	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION static portBaseType prvQueueSendDataToQueueFromISR( xQUEUE * const pxQueue, const void * const pvItemToQueue, const portBaseType xPosition, portBaseType *pxHigherPriorityTaskWoken )
{
portBaseType xReturn;
portUnsignedBaseType uxSavedInterruptStatus;

	uxSavedInterruptStatus = taskSET_INTERRUPT_MASK_FROM_ISR();
	{
		if( pxQueue->uxItemsWaiting < pxQueue->uxMaxNumberOfItems )
		{
			/* SAFERTOSTRACE QUEUESENDFROMISR */

			if( queueSEND_TO_FRONT == xPosition )
			{
				prvCopyDataToQueueFront( pxQueue, pvItemToQueue );

				/* MCDC Test Point: STD_IF "prvQueueSendDataToQueueFromISR" */
			}
			else
			{
				prvCopyDataToQueueBack( pxQueue, pvItemToQueue );

				/* MCDC Test Point: STD_ELSE "prvQueueSendDataToQueueFromISR" */
			}

			/* If the queue is locked we do not alter the event list.  This will
			be done when the queue is unlocked later. */
			if( queueUNLOCKED == pxQueue->xTxLock )
			{
				if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE )
				{
					if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToReceive ) ) != pdFALSE )
					{
						/* The task waiting has a higher priority so record that a
						context	switch is required. */
						*pxHigherPriorityTaskWoken = pdTRUE;

						/* MCDC Test Point: STD_IF "prvQueueSendDataToQueueFromISR" */
					}
					/* MCDC Test Point: ADD_ELSE "prvQueueSendDataToQueueFromISR" */
				}
				/* MCDC Test Point: ADD_ELSE "prvQueueSendDataToQueueFromISR" */
			}
			else
			{
				/* Increment the lock count so the task that unlocks the queue
				knows how many data items were posted while it was locked. */
				++( pxQueue->xTxLock );

				/* MCDC Test Point: STD_ELSE "prvQueueSendDataToQueueFromISR" */
			}

			xReturn = pdPASS;
		}
		else
		{
			xReturn = errQUEUE_FULL;

			/* MCDC Test Point: STD_ELSE "prvQueueSendDataToQueueFromISR" */
		}
	}
	taskCLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedInterruptStatus );

	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xQueueReceive( xQueueHandle xQueue, void * const pvBuffer, portTickType xTicksToWait )
{
portBaseType xReturn = pdTRUE;
/* Cast param 'xQueueHandle xQueue' to xQUEUE* type. */
xQUEUE* pxQueue = ( xQUEUE* ) xQueue;

	if( xTaskIsSchedulerSuspended( ) != pdFALSE )
	{
		xReturn = errSCHEDULER_IS_SUSPENDED;

		/* MCDC Test Point: STD_IF "xQueueReceive" */
	}
	else if( NULL == pxQueue )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueReceive" */
	}
	else if( prvIS_QUEUE_VALID( pxQueue ) == pdFALSE )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueReceive" */
	}
	else if( queueQUEUE_IS_QUEUE != pxQueue->uxQueueType )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueReceive" */
	}
	else if( ( NULL == pvBuffer ) && ( 0U != pxQueue->uxItemSize ) )
	{
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: NULL (Separate expansion) "xQueueReceive" */
	}
	else
	{
		/* Pass the parameters straight to prvReceiveDataFromQueue() with
		 * xRemoveDataFromQueue set to pdTRUE so that the data is actually removed
		 * from the queue. */
		xReturn = prvReceiveDataFromQueue( pxQueue, pvBuffer, xTicksToWait, pdTRUE );

		/* MCDC Test Point: STD_ELSE "xQueueReceive" */
	}

	/* MCDC Test Point: ADD_IF_WRAPPER_BEGIN "( NULL != pxQueue )" */
	/* MCDC Test Point: EXP_IF_AND "xQueueReceive" "( NULL == pvBuffer )" "( 0U != pxQueue->uxItemSize )" "Deviate: FF" */
	/* MCDC Test Point: ADD_IF_WRAPPER_END "( NULL != pxQueue )" */

	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xQueuePeek( xQueueHandle xQueue, void * const pvBuffer, portTickType xTicksToWait )
{
portBaseType xReturn = pdTRUE;
/* Cast param 'xQueueHandle xQueue' to xQUEUE* type. */
xQUEUE* pxQueue = ( xQUEUE* ) xQueue;

	if( xTaskIsSchedulerSuspended( ) != pdFALSE )
	{
		xReturn = errSCHEDULER_IS_SUSPENDED;

		/* MCDC Test Point: STD_IF "xQueuePeek" */
	}
	else if( NULL == pxQueue )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueuePeek" */
	}
	else if( prvIS_QUEUE_VALID( pxQueue ) == pdFALSE )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueuePeek" */
	}
	else if( queueQUEUE_IS_QUEUE != pxQueue->uxQueueType )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueuePeek" */
	}
	else if( ( NULL == pvBuffer ) && ( 0U != pxQueue->uxItemSize ) )
	{
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: NULL (Separate expansion) "xQueuePeek" */
	}
	else
	{
		/* Pass the parameters straight to prvReceiveDataFromQueue() with
		 * xRemoveDataFromQueue set to pdFALSE so that the data is left on the
		 * queue */
		xReturn = prvReceiveDataFromQueue( pxQueue, pvBuffer, xTicksToWait, pdFALSE );

		/* MCDC Test Point: STD_ELSE "xQueuePeek" */
	}

	/* MCDC Test Point: ADD_IF_WRAPPER_BEGIN "( NULL != pxQueue )" */
		/* MCDC Test Point: EXP_IF_AND "xQueuePeek" "( NULL == pvBuffer )" "( 0U != pxQueue->uxItemSize )" "Deviate: FF" */
	/* MCDC Test Point: ADD_IF_WRAPPER_END "( NULL != pxQueue )" */

	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xQueueReceiveFromISR( xQueueHandle xQueue, void * const pvBuffer, portBaseType *pxHigherPriorityTaskWoken )
{
portBaseType xReturn;
portUnsignedBaseType uxSavedInterruptStatus;
/* Cast param 'xQueueHandle xQueue' to xQUEUE* type. */
xQUEUE* pxQueue = ( xQUEUE* ) xQueue;

	if( NULL == pxHigherPriorityTaskWoken )
	{
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_IF "xQueueReceiveFromISR" */
	}
	else if( NULL == pxQueue )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueReceiveFromISR" */
	}
	else if( prvIS_QUEUE_VALID( pxQueue ) == pdFALSE )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueReceiveFromISR" */
	}
	else if( queueQUEUE_IS_QUEUE != pxQueue->uxQueueType )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueReceiveFromISR" */
	}
	else if( ( NULL == pvBuffer ) && ( 0U != pxQueue->uxItemSize ) )
	{
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: NULL (expanded elsewhere) "xQueueReceiveFromISR" */
	}
	else
	{
		uxSavedInterruptStatus = taskSET_INTERRUPT_MASK_FROM_ISR();
		{
			if( pxQueue->uxItemsWaiting > 0U )
			{
				/* SAFERTOSTRACE QUEUERECEIVEFROMISR */

				/* We cannot block from an ISR, so check there is data available. */

				/* Copy the data from the queue. */
				prvCopyDataFromQueue( pxQueue, pvBuffer );

				/* If the queue is locked we will not modify the event list.  Instead
				we update the lock count so the task that unlocks the queue will know
				that an ISR has removed data while the queue was locked. */
				if( queueUNLOCKED == pxQueue->xRxLock )
				{
					if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToSend ) ) == pdFALSE )
					{
						if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToSend ) ) != pdFALSE )
						{
							/* The task waiting has a higher priority than us so
							report that a context switch is required. */
							*pxHigherPriorityTaskWoken = pdTRUE;

							/* MCDC Test Point: STD_IF "xQueueReceiveFromISR" */
						}
						/* MCDC Test Point: ADD_ELSE "xQueueReceiveFromISR" */
					}
					/* MCDC Test Point: ADD_ELSE "xQueueReceiveFromISR" */
				}
				else
				{
					/* Increment the Rx lock so the task that unlocks the queue knows
					how many items were removed while it was locked. */
					++( pxQueue->xRxLock );

					/* MCDC Test Point: STD_ELSE "xQueueReceiveFromISR" */
				}

				xReturn = pdPASS;
			}
			else
			{
				xReturn = errQUEUE_EMPTY;

				/* MCDC Test Point: STD_ELSE "xQueueReceiveFromISR" */
			}
		}
		taskCLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedInterruptStatus );
	}
	/* SAFERTOSTRACE QUEUERECEIVEFROMISRFAILED */

	/* MCDC Test Point: ADD_IF_WRAPPER_BEGIN "( NULL != pxQueue )" */
		/* MCDC Test Point: EXP_IF_AND "xQueueReceiveFromISR" "( NULL == pvBuffer )" "( 0U != pxQueue->uxItemSize )" "Deviate: FF" */
	/* MCDC Test Point: ADD_IF_WRAPPER_END "( NULL != pxQueue )" */

	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION portBaseType xQueueMessagesWaiting( const xQueueHandle xQueue, portUnsignedBaseType *puxMessagesWaiting )
{
portBaseType xReturn;
/* Cast param 'xQueueHandle xQueue' to xQUEUE* type. */
xQUEUE* pxQueue = ( xQUEUE* ) xQueue;

	if( NULL == pxQueue )
	{
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_IF "xQueueMessagesWaiting" */
	}
	else if( prvIS_QUEUE_VALID( pxQueue ) == pdFALSE )
	{
		xReturn = errINVALID_QUEUE_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueMessagesWaiting" */
	}
	else if( NULL == puxMessagesWaiting )
	{
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: STD_ELSE_IF "xQueueMessagesWaiting" */
	}
	else
	{
		xReturn = pdPASS;

		portENTER_CRITICAL_WITHIN_API();
		{
			*puxMessagesWaiting = pxQueue->uxItemsWaiting;

			/* MCDC Test Point: STD_ELSE "xQueueMessagesWaiting" */
		}
		portEXIT_CRITICAL_WITHIN_API();
	}

	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION void vQueueSetSemaphoreInitialCount( xQueueHandle xQueue, portUnsignedBaseType uxInitialCount )
{
	/* This function is only called from the semaphore creation routines if
	the queue handle is valid. As this is within a critical section no further
	parameter checking is performed here and no status is returned. This
	routine is provided here as we need access to the internal queue structure
	to set the initial count. */
/* Cast param 'xQueueHandle xQueue' to xQUEUE* type. */
xQUEUE* pxQueue = ( xQUEUE* ) xQueue;

	pxQueue->uxItemsWaiting = uxInitialCount;

	/* MCDC Test Point: STD "vQueueSetSemaphoreInitialCount" */
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION void vQueueInitialiseMutex( xQueueHandle xQueue )
{
/* Cast param 'xQueueHandle xQueue' to xQUEUE* type. */
xQUEUE* pxQueue = ( xQUEUE* ) xQueue;

	/* This function is only called with a valid handle. The queue create
	 * function will set all the queue structure members correctly for a
	 * generic queue, but this function is creating a mutex.  Overwrite
	 * those members that need to be set differently. */

	/* Queue is a mutex. */
	pxQueue->uxQueueType = queueQUEUE_IS_MUTEX;

	/* Start with the mutex in the 'non taken' state. */
	pxQueue->uxItemsWaiting = 1U;

	/* MCDC Test Point: STD "vQueueInitialiseMutex" */
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION static void prvUnlockQueue( xQueueHandle xQueue )
{
	/* THIS FUNCTION MUST BE CALLED WITH THE SCHEDULER SUSPENDED. */
/* Cast param 'xQueueHandle xQueue' to xQUEUE* type. */
xQUEUE* pxQueue = ( xQUEUE* ) xQueue;

	/* The lock variable indicates if extra data items were placed or
	removed from the queue while the queue was locked.  When a queue is
	locked items can be added or removed, but the event lists cannot be
	updated. */
	portENTER_CRITICAL_WITHIN_API();
	{
		/* MCDC Test Point: WHILE_EXTERNAL "prvUnlockQueue" "( pxQueue->xTxLock > queueLOCKED_UNMODIFIED )" */

		/* See if data was added to the queue while it was locked.  We should
		unblock a task for each posted item as we don't want a task to unblock,
		remove just one multiple items, then go and do something else without
		unblocking any other tasks due to the remaining data.  If the first
		task drains the queue then any other unblocked tasks will simply
		re-block for the remains of their block period. */
		while( pxQueue->xTxLock > queueLOCKED_UNMODIFIED )
		{
			/* Data was posted while the queue was locked.  Are any tasks
			blocked waiting for data to become available? */
			if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE )
			{
				/* Tasks that are removed from the event list will get added to
				the pending ready list as the scheduler is still suspended. */
				if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToReceive ) ) != pdFALSE )
				{
					/* The task waiting has a higher priority so record that a
					context	switch is required. */
					vTaskPendYield();

					/* MCDC Test Point: STD_IF "prvUnlockQueue" */
				}
				/* MCDC Test Point: ADD_ELSE "prvUnlockQueue" */

				--( pxQueue->xTxLock );
			}
			else
			{
				/* MCDC Test Point: STD_ELSE "prvUnlockQueue" */

				break;
			}
			/* MCDC Test Point: WHILE_INTERNAL "prvUnlockQueue" "( pxQueue->xTxLock > queueLOCKED_UNMODIFIED )" */
		}

		pxQueue->xTxLock = queueUNLOCKED;
	}
	portEXIT_CRITICAL_WITHIN_API();

	/* Do the same for the Rx lock. */
	portENTER_CRITICAL_WITHIN_API();
	{
		/* MCDC Test Point: WHILE_EXTERNAL "prvUnlockQueue" "( pxQueue->xRxLock > queueLOCKED_UNMODIFIED )" */
		while( pxQueue->xRxLock > queueLOCKED_UNMODIFIED )
		{
			if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToSend ) ) == pdFALSE )
			{
				if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToSend ) ) != pdFALSE )
				{
					vTaskPendYield();

					/* MCDC Test Point: STD_IF "prvUnlockQueue" */
				}
				/* MCDC Test Point: ADD_ELSE "prvUnlockQueue" */

				--( pxQueue->xRxLock );
			}
			else
			{
				/* MCDC Test Point: STD_ELSE "prvUnlockQueue" */

				break;
			}
			/* MCDC Test Point: WHILE_INTERNAL "prvUnlockQueue" "( pxQueue->xRxLock > queueLOCKED_UNMODIFIED )" */
		}

		pxQueue->xRxLock = queueUNLOCKED;
	}
	portEXIT_CRITICAL_WITHIN_API();
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION static portBaseType prvIsQueueEmpty( const xQueueHandle xQueue )
{
portBaseType xReturn;
/* Cast param 'xQueueHandle xQueue' to xQUEUE* type. */
xQUEUE* pxQueue = ( xQUEUE* ) xQueue;

	portENTER_CRITICAL_WITHIN_API();
	{
		if( 0U == pxQueue->uxItemsWaiting )
		{
			xReturn = pdTRUE;

			/* MCDC Test Point: STD_IF "prvIsQueueEmpty" */
		}
		else
		{
			xReturn = pdFALSE;

			/* MCDC Test Point: STD_ELSE "prvIsQueueEmpty" */
		}
	}
	portEXIT_CRITICAL_WITHIN_API();

	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION static portBaseType prvIsQueueFull( const xQueueHandle xQueue )
{
portBaseType xReturn;
/* Cast param 'xQueueHandle xQueue' to xQUEUE* type. */
xQUEUE* pxQueue = ( xQUEUE* ) xQueue;

	portENTER_CRITICAL_WITHIN_API();
	{
		if( pxQueue->uxMaxNumberOfItems == pxQueue->uxItemsWaiting )
		{
			xReturn = pdTRUE;

			/* MCDC Test Point: STD_IF "prvIsQueueFull" */
		}
		else
		{
			xReturn = pdFALSE;

			/* MCDC Test Point: STD_ELSE "prvIsQueueFull" */
		}
	}
	portEXIT_CRITICAL_WITHIN_API();

	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION static void prvCopyDataToQueueFront( xQUEUE * const pxQueue, const void * const pvItemToQueue )
{
	/* Copy the data. */
	vPortCopyBytes( ( void * ) pxQueue->pcReadFrom, pvItemToQueue, pxQueue->uxItemSize );

	/* Increment number of items. */
	++( pxQueue->uxItemsWaiting );

	/* Increment write pointer. */
	pxQueue->pcReadFrom -= pxQueue->uxItemSize;
	if( pxQueue->pcReadFrom < pxQueue->pcHead )
	{
		pxQueue->pcReadFrom = ( pxQueue->pcTail - pxQueue->uxItemSize );

		/* MCDC Test Point: STD_IF "prvCopyDataToQueueFront" */
	}
	/* MCDC Test Point: ADD_ELSE "prvCopyDataToQueueFront" */
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION static void prvCopyDataToQueueBack( xQUEUE * const pxQueue, const void * const pvItemToQueue )
{
	/* Copy the data. */
	vPortCopyBytes( ( void * ) pxQueue->pcWriteTo, pvItemToQueue, pxQueue->uxItemSize );

	/* Increment number of items. */
	++( pxQueue->uxItemsWaiting );

	/* Increment write pointer. */
	pxQueue->pcWriteTo += pxQueue->uxItemSize;
	if( pxQueue->pcWriteTo >= pxQueue->pcTail )
	{
		pxQueue->pcWriteTo = pxQueue->pcHead;

		/* MCDC Test Point: STD_IF "prvCopyDataToQueueBack" */
	}
	/* MCDC Test Point: ADD_ELSE "prvCopyDataToQueueBack" */
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION static void prvCopyDataFromQueue( xQUEUE * const pxQueue, void * const pvBuffer )
{
	/* Increment read pointer. */
	pxQueue->pcReadFrom += pxQueue->uxItemSize;
	if( pxQueue->pcReadFrom >= pxQueue->pcTail )
	{
		pxQueue->pcReadFrom = pxQueue->pcHead;

		/* MCDC Test Point: STD_IF "prvCopyDataFromQueue" */
	}
	/* MCDC Test Point: ADD_ELSE "prvCopyDataFromQueue" */

	/* Decrement number of items. */
	--( pxQueue->uxItemsWaiting );

	/* Copy the data. */
	vPortCopyBytes( pvBuffer, ( void * ) pxQueue->pcReadFrom, pxQueue->uxItemSize );
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION static portBaseType prvQueueSendDataToQueue( xQUEUE * const pxQueue, const void * const pvItemToQueue, portTickType xTicksToWait, const portBaseType xPosition )
{
portBaseType xReturn = pdPASS;
portBaseType xContextSwitchRequired = pdFALSE;
xTimeOutType xTimeOut;

	do
	{
		/* If xTicksToWait is zero then we are not going to block even
		if there is no room in the queue to post. */
		if( xTicksToWait > ( portTickType ) 0U )
		{
			/* Ensure no scheduling or queue manipulation can occur.
			This does not disable interrupts. */
			vTaskSuspendScheduler();
			prvLOCK_QUEUE( pxQueue );

			if( pdPASS == xReturn )
			{
				/* This is the first time through the function - note the
				time now.  This must be done with the scheduler suspended
				to ensure time does not elapse before the call to
				xTaskCheckForTimeOut() below - otherwise we would not
				attempt to block. */
				vTaskSetTimeOut( &xTimeOut );

				/* MCDC Test Point: STD_IF "prvQueueSendDataToQueue" */
			}
			/* MCDC Test Point: ADD_ELSE "prvQueueSendDataToQueue" */

			if( pdTRUE == prvIsQueueFull( pxQueue ) )
			{
				/* Need to call xTaskCheckForTimeout again as time could
				have passed since it was last called if this is not the
				first time around this loop. */
				if( xTaskCheckForTimeOut( &xTimeOut, &xTicksToWait ) == pdFALSE )
				{
					/* SAFERTOSTRACE QUEUESENDBLOCKED */

					/* Place ourselves on the event list of the queue. */
					vTaskPlaceOnEventList( &( pxQueue->xTasksWaitingToSend ), xTicksToWait );

					/* Unlocking the queue means queue events can effect the
					event list.  It is possible	that interrupts occurring now
					remove this task from the event	list again - but as the
					scheduler is suspended the task will go onto the pending
					ready last instead of the actual ready list. */
					prvUnlockQueue( pxQueue );

					/* Resuming the scheduler will move tasks from the pending
					ready list into the ready list - so it is feasible that this
					task is already in a ready list before it yields - in which
					case the yield will not cause a context switch unless there
					is also a higher priority task in the pending ready list. */
					if( pdFALSE == xTaskResumeScheduler() )
					{
						taskYIELD_WITHIN_API();

						/* MCDC Test Point: STD_IF "prvQueueSendDataToQueue" */
					}
					/* MCDC Test Point: ADD_ELSE "prvQueueSendDataToQueue" */
				}
				else
				{
					prvUnlockQueue( pxQueue );
					( void ) xTaskResumeScheduler();

					/* MCDC Test Point: STD_ELSE "prvQueueSendDataToQueue" */
				}
			}
			else
			{
				/* The queue was not full so we can just unlock the
				scheduler and queue again before carrying on. */
				prvUnlockQueue( pxQueue );
				( void ) xTaskResumeScheduler();

				/* MCDC Test Point: STD_ELSE "prvQueueSendDataToQueue" */
			}
		}
		/* MCDC Test Point: ADD_ELSE "prvQueueSendDataToQueue" */

		/* Higher priority tasks and interrupts can execute during
		this time and could possibly refill the queue - even if we
		unblocked because space became available. */

		portENTER_CRITICAL_WITHIN_API();
		{
			/* Is there room on the queue now?  To be running we must be
			the highest priority task wanting to access the queue. */
			if( pxQueue->uxItemsWaiting < pxQueue->uxMaxNumberOfItems )
			{
				/* SAFERTOSTRACE QUEUESEND */

				if( queueSEND_TO_FRONT == xPosition )
				{
					prvCopyDataToQueueFront( pxQueue, pvItemToQueue );

					/* MCDC Test Point: STD_IF "prvQueueSendDataToQueue" */
				}
				else
				{
					prvCopyDataToQueueBack( pxQueue, pvItemToQueue );

					/* MCDC Test Point: STD_ELSE "prvQueueSendDataToQueue" */
				}

				if( queueQUEUE_IS_MUTEX == pxQueue->uxQueueType )
				{
					/* The mutex is no longer being held, we need to tell
					 * the task to remove the mutex from its list and
					 * disinherit priority if necessary. */
					xContextSwitchRequired = xTaskPriorityDisinherit( &( pxQueue->xMutexHolder ) );

					/* MCDC Test Point: STD_IF "prvQueueSendDataToQueue" */
				}
				else
				{
					xReturn = pdPASS;
					/* MCDC Test Point: STD_ELSE "prvQueueSendDataToQueue" */
				}

				/* If there was a task waiting for data to arrive on the queue,
				 * then unblock it now. */
				/* MCDC Test Point: EXP_IF_MACRO "listLIST_IS_EMPTY(include/list.h)" "listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE" */
				if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE )
				{
					if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToReceive ) ) == pdTRUE )
					{
						/* The unblocked task has a priority higher than
						our own so request an immediate yield. */
						xContextSwitchRequired = pdTRUE;

						/* MCDC Test Point: STD_IF "prvQueueSendDataToQueue" */
					}
					/* MCDC Test Point: ADD_ELSE "prvQueueSendDataToQueue" */
				}
				/* MCDC Test Point: ADD_ELSE "prvQueueSendDataToQueue" */

				if( pdTRUE == xContextSwitchRequired )
				{
					/* Yield if requested due to a queue unblock event or
					 * a mutex priority (dis)inheritance action. */
					taskYIELD_WITHIN_API();

					/* MCDC Test Point: STD_IF "prvQueueSendDataToQueue" */
				}
				/* MCDC Test Point: ADD_ELSE "prvQueueSendDataToQueue" */
			}
			else
			{
				/* Setting xReturn to errQUEUE_FULL will force its timeout
				to be re-evaluated.  This is necessary in case interrupts
				and higher priority tasks accessed the queue between this
				task being unblocked and subsequently attempting to write
				to the queue. */
				xReturn = errQUEUE_FULL;

				/* MCDC Test Point: STD_ELSE "prvQueueSendDataToQueue" */
			}
		}
		portEXIT_CRITICAL_WITHIN_API();

		if( errQUEUE_FULL == xReturn )
		{
			if( xTicksToWait > ( portTickType ) 0U )
			{
				if( xTaskCheckForTimeOut( &xTimeOut, &xTicksToWait ) == pdFALSE )
				{
					xReturn = errERRONEOUS_UNBLOCK;

					/* MCDC Test Point: STD_IF "prvQueueSendDataToQueue" */
				}
				/* MCDC Test Point: ADD_ELSE "prvQueueSendDataToQueue" */
			}
			/* MCDC Test Point: ADD_ELSE "prvQueueSendDataToQueue" */
		}
		/* MCDC Test Point: ADD_ELSE "prvQueueSendDataToQueue" */

		/* MCDC Test Point: WHILE_INTERNAL "prvQueueSendDataToQueue" "( errERRONEOUS_UNBLOCK == xReturn )" */
	} while( errERRONEOUS_UNBLOCK == xReturn );

	/* SAFERTOSTRACE QUEUESENDFAILED */

	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION static portBaseType prvReceiveDataFromQueue( xQUEUE *pxQueue, void * const pvBuffer, portTickType xTicksToWait, portBaseType xRemoveDataFromQueue )
{
portBaseType xReturn = pdPASS;
xTimeOutType xTimeOut;
portInt8Type *pcOriginalReadPosition;

	do
	{
		if( xTicksToWait > ( portTickType ) 0U )
		{
			vTaskSuspendScheduler();
			prvLOCK_QUEUE( pxQueue );

			if( pdPASS == xReturn )
			{
				/* This is the first time through the function - note the
				time now.  This must be done with the scheduler suspended
				to ensure time does not elapse before the call to
				xTaskCheckForTimeOut() below - otherwise we would not
				attempt to block. */
				vTaskSetTimeOut( &xTimeOut );

				/* MCDC Test Point: STD_IF "prvReceiveDataFromQueue" */
			}
			/* MCDC Test Point: ADD_ELSE "prvReceiveDataFromQueue" */

			if( pdTRUE == prvIsQueueEmpty( pxQueue ) )
			{
				/* Need to call xTaskCheckForTimeout again as time could
				have passed since it was last called if this is not the
				first time around this loop. */
				if( xTaskCheckForTimeOut( &xTimeOut, &xTicksToWait ) == pdFALSE )
				{
					if( pxQueue->uxQueueType == queueQUEUE_IS_MUTEX )
					{
						portENTER_CRITICAL_WITHIN_API();
						{
							vTaskPriorityInherit( &( pxQueue->xMutexHolder ) );
							/* MCDC Test Point: STD_IF "prvReceiveDataFromQueue" */
						}
						portEXIT_CRITICAL_WITHIN_API();
					}
					/* MCDC Test Point: ADD_ELSE "prvReceiveDataFromQueue" */

					/* SAFERTOSTRACE QUEUERECEIVEBLOCKED */
					vTaskPlaceOnEventList( &( pxQueue->xTasksWaitingToReceive ), xTicksToWait );
					prvUnlockQueue( pxQueue );
					if( xTaskResumeScheduler() == pdFALSE )
					{
						taskYIELD_WITHIN_API();
						/* MCDC Test Point: STD_IF "prvReceiveDataFromQueue" */
					}
					/* MCDC Test Point: ADD_ELSE "prvReceiveDataFromQueue" */

					if( queueQUEUE_IS_MUTEX == pxQueue->uxQueueType )
					{
						portENTER_CRITICAL_WITHIN_API();
						{
							/* Check to see whether we need to disinherit the
							 * priority of the task holding the mutex as a result
							 * of a blocking task timing out. This call will also
							 * be made if the mutex has been obtained but in this
							 * case no actions will be taken. */
							vTaskReEvaluateInheritedPriority( &( pxQueue->xMutexHolder ) );

							/* MCDC Test Point: STD_IF "prvReceiveDataFromQueue" */
						}
						portEXIT_CRITICAL_WITHIN_API();
					}
					/* MCDC Test Point: ADD_ELSE "prvReceiveDataFromQueue" */
				}
				else
				{
					prvUnlockQueue( pxQueue );
					( void ) xTaskResumeScheduler();

					/* MCDC Test Point: STD_ELSE "prvReceiveDataFromQueue" */
				}
			}
			else
			{
				prvUnlockQueue( pxQueue );
				( void ) xTaskResumeScheduler();

				/* MCDC Test Point: STD_ELSE "prvReceiveDataFromQueue" */
			}
		}
		/* MCDC Test Point: ADD_ELSE "prvReceiveDataFromQueue" */

		portENTER_CRITICAL_WITHIN_API();
		{
			if( pxQueue->uxItemsWaiting > 0U )
			{
				/*
				 * Remember our read position so it can be reset if the data
				 * is to be left on the queue.
				 */
				pcOriginalReadPosition = pxQueue->pcReadFrom;

				/* SAFERTOSTRACE QUEUERECEIVE */

				/* Retrieve the data. */
				prvCopyDataFromQueue( pxQueue, pvBuffer );
				xReturn = pdPASS;

				if( pdTRUE == xRemoveDataFromQueue )
				{
					if( queueQUEUE_IS_MUTEX == pxQueue->uxQueueType )
					{
						/* Record the information required to implement
						priority inheritance should it become necessary. */
						vTaskLogMutexTaken( &( pxQueue->xMutexHolder ) );

						/* MCDC Test Point: STD_IF "prvReceiveDataFromQueue" */
					}
					/* MCDC Test Point: ADD_ELSE "prvReceiveDataFromQueue" */

					/* Are there any tasks waiting to send to the queue? */
					if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToSend ) ) == pdFALSE )
					{
						if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToSend ) ) == pdTRUE )
						{
							/*
							 * The unblocked task has a higher priority than
							 * this task.
							 */
							taskYIELD_WITHIN_API();

							/* MCDC Test Point: STD_IF "prvReceiveDataFromQueue" */
						}
						/* MCDC Test Point: ADD_ELSE "prvReceiveDataFromQueue" */
					}
					/* MCDC Test Point: ADD_ELSE "prvReceiveDataFromQueue" */
				}
				else
				{
					/*
					 * We are not removing the data, so reset the read
					 * pointer and message count.
					 */
					pxQueue->pcReadFrom = pcOriginalReadPosition;
					++( pxQueue->uxItemsWaiting );

					/*
					 * The data is being left in the queue, so see if there
					 * are any other tasks waiting for the data.
					 */
					if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE )
					{
						if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToReceive ) ) == pdTRUE )
						{
							/*
							 * The unblocked task has a higher priority than
							 * this task.
							 */
							taskYIELD_WITHIN_API();

							/* MCDC Test Point: STD_IF "prvReceiveDataFromQueue" */
						}
						/* MCDC Test Point: ADD_ELSE "prvReceiveDataFromQueue" */
					}
					/* MCDC Test Point: ADD_ELSE "prvReceiveDataFromQueue" */
					/* MCDC Test Point: STD_ELSE "prvReceiveDataFromQueue" */
				}
			}
			else
			{
				xReturn = errQUEUE_EMPTY;

				/* MCDC Test Point: STD_ELSE "prvReceiveDataFromQueue" */
			}
		}
		portEXIT_CRITICAL_WITHIN_API();

		if( errQUEUE_EMPTY == xReturn )
		{
			if( xTicksToWait > ( portTickType ) 0U )
			{
				if( xTaskCheckForTimeOut( &xTimeOut, &xTicksToWait ) == pdFALSE )
				{
					xReturn = errERRONEOUS_UNBLOCK;

					/* MCDC Test Point: STD_IF "prvReceiveDataFromQueue" */
				}
				/* MCDC Test Point: ADD_ELSE "prvReceiveDataFromQueue" */
			}
			/* MCDC Test Point: ADD_ELSE "prvReceiveDataFromQueue" */
		}
		/* MCDC Test Point: ADD_ELSE "prvReceiveDataFromQueue" */

		/* MCDC Test Point: WHILE_INTERNAL "prvReceiveDataFromQueue" "( errERRONEOUS_UNBLOCK == xReturn )" */
	} while( errERRONEOUS_UNBLOCK == xReturn );

	/* SAFERTOSTRACE QUEUERECEIVEFAILED */

	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION static portBaseType prvCheckQueueCreateParameters( portInt8Type *pcQueueMemory, portUnsignedBaseType uxBufferLength, portUnsignedBaseType uxQueueLength, portUnsignedBaseType uxItemSize, const xQueueHandle *pxQueue )
{
portBaseType xReturn = pdPASS;

	if( 0U != ( ( portUnsignedBaseType ) pcQueueMemory & portWORD_ALIGNMENT_MASK ) )
	{
		xReturn = errINVALID_BYTE_ALIGNMENT;

		/* MCDC Test Point: STD_IF "prvCheckQueueCreateParameters" */
	}
	/* MCDC Test Point: ADD_ELSE "prvCheckQueueCreateParameters" */

	if( 0U == uxQueueLength )
	{
		xReturn = errINVALID_QUEUE_LENGTH;

		/* MCDC Test Point: STD_IF "prvCheckQueueCreateParameters" */
	}
	/* MCDC Test Point: ADD_ELSE "prvCheckQueueCreateParameters" */

	if( uxBufferLength != ( ( uxQueueLength * uxItemSize ) + sizeof( xQUEUE ) ) )
	{
		xReturn = errINVALID_BUFFER_SIZE;

		/* MCDC Test Point: STD_IF "prvCheckQueueCreateParameters" */
	}
	/* MCDC Test Point: ADD_ELSE "prvCheckQueueCreateParameters" */

	if( ( NULL == pcQueueMemory ) || ( NULL == pxQueue ) )
	{
		xReturn = errNULL_PARAMETER_SUPPLIED;

		/* MCDC Test Point: NULL (Separate expansion) */
	}
	/* MCDC Test Point: EXP_IF_OR "prvCheckQueueCreateParameters" "( NULL == pcQueueMemory )" "( NULL == pxQueue )" "Deviate: TT" */

	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION	portBaseType xQueueGiveMutex( xQueueHandle xMutex )
{
portBaseType xReturn;
xQUEUE* pxMutex = ( xQUEUE* ) xMutex;

	if( pdFALSE != xTaskIsSchedulerSuspended() )
	{
		xReturn = errSCHEDULER_IS_SUSPENDED;

		/* MCDC Test Point: STD_IF "xQueueGiveMutex" */
	}
	else if( NULL == pxMutex )
	{
		xReturn = errINVALID_MUTEX_HANDLE;

		/* MCDC Test Point: STD_IF "xQueueGiveMutex" */
	}
	else if( prvIS_QUEUE_VALID( pxMutex ) == pdFALSE )
	{
		xReturn = errINVALID_MUTEX_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueGiveMutex" */
	}
	else if( queueQUEUE_IS_MUTEX != pxMutex->uxQueueType )
	{
		xReturn = errINVALID_MUTEX_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueGiveMutex" */
	}
	else
	{
		/* If this is the task that holds the mutex then pxMutexHolder will not
		change outside of this task.  If this task does not hold the mutex then
		pxMutexHolder can never coincidentally equal the tasks handle, and as
		this is the only condition we are interested in it does not matter if
		pxMutexHolder is accessed simultaneously by another task.  Therefore no
		mutual exclusion is required to test the pxMutexHolder variable. */
		if( ( portTaskHandleType ) listGET_LIST_ITEM_VALUE( &( pxMutex->xMutexHolder ) ) == xTaskGetCurrentTaskHandle() )
		{
			/* uxRecursiveCallCount cannot be zero if pxMutexHolder is equal to
			the task handle, however check for corruption. Also,
			uxRecursiveCallCount is only modified by the mutex holder, and as
			there can only be one, no mutual exclusion is required to modify the
			uxRecursiveCallCount member. */
			if( 0U == pxMutex->uxMutexRecursiveDepth )
			{
				xReturn = errMUTEX_CORRUPTED;
				/* MCDC Test Point: STD_IF "xQueueGiveMutex" */
			}
			else
			{
				( pxMutex->uxMutexRecursiveDepth )--;

				/* Has the recursive call count unwound to 0? */
				if( 0U == pxMutex->uxMutexRecursiveDepth )
				{
					/* Return the mutex.  This will automatically unblock any other
					task that might be waiting to access the mutex. */
					xReturn = prvQueueSendDataToQueue( pxMutex, NULL, 0U, queueSEND_TO_BACK );

					/* MCDC Test Point: STD_IF "xQueueGiveMutex" */
				}
				else
				{
					xReturn = pdPASS;
					/* MCDC Test Point: STD_ELSE "xQueueGiveMutex" */
				}
				/* MCDC Test Point: STD_ELSE "xQueueGiveMutex" */
			}
		}
		else
		{
			/* The mutex cannot be given because the calling task is not the
			holder. */
			xReturn = errMUTEX_NOT_OWNED_BY_CALLER;

			/* MCDC Test Point: STD_ELSE "xQueueGiveMutex" */
		}
		/* MCDC Test Point: STD_ELSE "xQueueGiveMutex" */
	}
	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION	portBaseType xQueueForcedMutexRelease( xQueueHandle xMutex )
{
xQUEUE* pxMutex = ( xQUEUE* ) xMutex;

	/* We are in the process of deleting a task holding the mutex and we are in
	 * a critical section therefore there is no need to consider mutual
	 * exclusion or simultaneous access. */

	/* Reset the recursive depth */
	pxMutex->uxMutexRecursiveDepth = 0U;

	/* MCDC Test Point: STD "xQueueForcedMutexRelease" */
	
	/* Return the mutex.  This will automatically unblock any other
	 * task that might be waiting to access the mutex. */
	return prvQueueSendDataToQueue( pxMutex, NULL, 0U, queueSEND_TO_BACK );
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION	portBaseType xQueueTakeMutex( xQueueHandle xMutex, portTickType xTicksToWait )
{
portBaseType xReturn;
xQUEUE* pxMutex = ( xQUEUE* ) xMutex;

	if( pdFALSE != xTaskIsSchedulerSuspended() )
	{
		xReturn = errSCHEDULER_IS_SUSPENDED;

		/* MCDC Test Point: STD_IF "xQueueTakeMutex" */
	}
	else if( NULL == pxMutex )
	{
		xReturn = errINVALID_MUTEX_HANDLE;
		/* MCDC Test Point: STD_IF "xQueueTakeMutex" */
	}
	else if( prvIS_QUEUE_VALID( pxMutex ) == pdFALSE )
	{
		xReturn = errINVALID_MUTEX_HANDLE;
		/* MCDC Test Point: STD_ELSE_IF "xQueueTakeMutex" */
	}
	else if( queueQUEUE_IS_MUTEX != pxMutex->uxQueueType )
	{
		xReturn = errINVALID_MUTEX_HANDLE;

		/* MCDC Test Point: STD_ELSE_IF "xQueueTakeMutex" */
	}
	else
	{
		/* Comments regarding mutual exclusion as per those within xQueueGiveMutex(). */

		if( ( portTaskHandleType ) listGET_LIST_ITEM_VALUE( &( pxMutex->xMutexHolder ) ) == xTaskGetCurrentTaskHandle() )
		{
			( pxMutex->uxMutexRecursiveDepth )++;
			xReturn = pdPASS;

			/* MCDC Test Point: STD_IF "xQueueTakeMutex" */
		}
		else
		{
			xReturn = prvReceiveDataFromQueue( pxMutex, NULL, xTicksToWait, pdTRUE );


			/* pdPASS will only be returned if the mutex was successfully
			obtained.  The calling task may have entered the Blocked state
			before reaching here. */
			if( pdPASS == xReturn )
			{
				( pxMutex->uxMutexRecursiveDepth )++;

				/* MCDC Test Point: STD_IF "xQueueTakeMutex" */
			}
			/* MCDC Test Point: ADD_ELSE "xQueueTakeMutex" */

			/* MCDC Test Point: STD_ELSE "xQueueTakeMutex" */
		}
		/* MCDC Test Point: STD_ELSE "xQueueTakeMutex" */
	}
	return xReturn;
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION	portUnsignedBaseType uxQueueGetBlockedTaskPriority( xQueueHandle xQueue  )
{
portUnsignedBaseType uxEventListValue = 0U;
xQUEUE *pxQueue = ( xQUEUE * ) xQueue ;
xListItem *pxBlockedListItem = NULL;

	if( pdFALSE == listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) )
	{
		pxBlockedListItem = listGET_HEAD_ENTRY( &( pxQueue->xTasksWaitingToReceive ) );
		uxEventListValue = listGET_LIST_ITEM_VALUE( pxBlockedListItem );
		/* MCDC Test Point: STD_IF "uxQueueGetBlockedTaskPriority" */
	}
	else
	{
		/* return 0 if no tasks are waiting for the queue item, so set to max. */
		uxEventListValue = configMAX_PRIORITIES;
		/* MCDC Test Point: STD_ELSE "uxQueueGetBlockedTaskPriority" */
	}
	return ( configMAX_PRIORITIES - uxEventListValue );
}
/*-----------------------------------------------------------*/

KERNEL_FUNCTION	void vQueueRemoveListItemAndCheckInheritance( xListItem * pxListItem )
{
xQUEUE* pxQueue;
xList* pxTaskWaitingList;

	/* This function is called from within a critical section, therefore it is
	 * safe to operate on the list items within the TCB and mutex objects. */

	/* if the list we are blocked on is a mutex, then deleting
	 * ourselves may affect priority inheritance of the holding
	 * task, perform a re-evaluation. */

	/* Get the list owner. We can guarantee that its not NULL. */
	pxTaskWaitingList = pxListItem->pvContainer;

	/* The Queue/Mutex handle should be the owner of the end marker. */
	pxQueue = listGET_LIST_ITEM_OWNER( listGET_END_MARKER( pxTaskWaitingList ) );

	/* Remove ourselves from the list. */
	vListRemove( pxListItem );

	/* if the list is a mutex then force re-evaluation of any
	 * inherited priority. */
	if( queueQUEUE_IS_MUTEX == pxQueue->uxQueueType )
	{
		vTaskReEvaluateInheritedPriority( &( pxQueue->xMutexHolder ) );

		/* MCDC Test Point: STD_IF "vQueueRemoveListItemAndCheckInheritance" */
	}
	/* MCDC Test Point: ADD_ELSE "vQueueRemoveListItemAndCheckInheritance" */
}
/*-----------------------------------------------------------*/


/* SAFERTOSTRACE QUEUEGETQUEUETYPE */

#ifdef SAFERTOS_MODULE_TEST
	#include "QueueCTestHeaders.h"
	#include "QueueCTest.h"
#endif /* SAFERTOS_MODULE_TEST */

