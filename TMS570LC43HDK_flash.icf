/*###ICF### Section handled by ICF editor, don't touch! ****/
/*-Editor annotation file-*/
/* IcfEditorFile="$TOOLKIT_DIR$\config\ide\IcfEditor\a_v1_0.xml" */
/*-Specials-*/
define symbol __ICFEDIT_intvec_start__ = 0x00000000;
/*-Memory Regions-*/
define symbol __ICFEDIT_region_ROM_start__ = 0x00000000;
define symbol __ICFEDIT_region_ROM_end__   = 0x002FFFFF;
define symbol __ICFEDIT_region_RAM_start__ = 0x08002000;
define symbol __ICFEDIT_region_RAM_end__   = 0x0803FFFF;
/*-Sizes-*/
define symbol __ICFEDIT_size_cstack__   = 0x1000;
define symbol __ICFEDIT_size_svcstack__ = 0x100;
define symbol __ICFEDIT_size_irqstack__ = 0x100;
define symbol __ICFEDIT_size_fiqstack__ = 0x100;
define symbol __ICFEDIT_size_undstack__ = 0x100;
define symbol __ICFEDIT_size_abtstack__ = 0x100;
define symbol __ICFEDIT_size_heap__     = 0;
/**** End of ICF editor section. ###ICF###*/


define symbol __region_app_code_start__ = __ICFEDIT_intvec_start__;
define symbol __region_app_code_end__   = __ICFEDIT_region_ROM_end__;

/* Define a memory region that covers the entire 4 GB addressible space of the
 * processor. */
define memory mem with size = 4G;


/* Define a region for the on-chip Flash. */
define region ROM_region   = mem:[from __region_app_code_start__ to __region_app_code_end__];


/* Define a region that corresponds to the total size reserved for the stacks.
 * NOTE: It is imperative that the total stack size here matches exactly the
 * total of the stack sizes used by the HALCoGen generated file sys_core.asm */
define symbol __total_stack_size__ = __ICFEDIT_size_cstack__ +
									 __ICFEDIT_size_svcstack__ +
									 __ICFEDIT_size_fiqstack__ +
									 __ICFEDIT_size_irqstack__ +
									 __ICFEDIT_size_abtstack__ +
									 __ICFEDIT_size_undstack__;
define region STACK   = mem:[from 0x08000000 size __total_stack_size__];


/* Define a region for the on-chip SRAM - the RAM start address must be adjusted
 * to accommodate the STACK region. */
define region RAM_region   = mem:[from __ICFEDIT_region_RAM_start__  to __ICFEDIT_region_RAM_end__];


define symbol __region_DRAM_start__   = 0x80000000;
define symbol __region_DRAM_end__     = 0x807FFFFF;
define region DRAM_region  = mem:[from __region_DRAM_start__   to __region_DRAM_end__];


/* Define a block of memory to be used as the heap - note that SafeRTOS does not
use the heap. */
define block HEAP      with alignment = 8, size = __ICFEDIT_size_heap__     { };


/* Indicate that the read/write values should be initialized by copying from
 * Flash. */
initialize by copy { readwrite };

/* Indicate that the noinit values should be left alone. */
do not initialize  { section .noinit };


/* Define a block for the kernel's functions.  - 0x38 because the first part of
 * the 16K is taken by the interrupt vectors. */
define block __kernel_functions_block__ with alignment = 8, size = 0x8000 - 0x38 { section __kernel_functions__ };

/* Place the interrupt vectors at the required address, followed by the kernel's
 * functions. */
place at address mem:__ICFEDIT_intvec_start__ { readonly section .intvecs, block __kernel_functions_block__ };

/* Place the remainder of the read-only items into flash. */
place in ROM_region   { readonly };


/* Define a block for the kernel's data. */
define block __kernel_data_block__ with alignment = 0x800, size = 0x800 { section __kernel_data__ };


/* Define a block for the Maths Test task data. */
define block __maths_test_data_block__ with alignment = 0x40, size = 0x40 { section __maths_test_data__ };

//-------------------------------BLINKY INCLUDE-------------------------------//
//----------------------------------------------------------------------------//
define block __blinky_data_block__ with alignment = 0x40, size = 0x40 { section __blinky_data__ };

define block __blinky_q_data_block__ with alignment = 0x80, size = 0x80 { section __blinky_q_data__ };

//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//

/* Define a block for the Block Q task data. */
define block __block_q_data_block__ with alignment = 0x80, size = 0x80 { section __block_q_data__ };

/* Define a block for the Block Time Test task data. */
define block __block_tim_data_block__ with alignment = 0x20, size = 0x20 { section __block_tim_data__ };

/* Define a block for the Com Test Task data. */
define block __com_test_data_block__ with alignment = 0x20, size = 0x20
{
	section __com_test_data__,
	section __com_test_zero_data__
};

/* Define a block for the Counting Semaphore Task data. */
define block __counting_semaphore_task_data_block__ with alignment = 0x40, size = 0x40
{
	section __counting_semaphore_task_data__,
	section __counting_semaphore_task_zero_data__
};

/* Define a block for the Create Delete Test task data. */
define block __create_delete_data_block__ with alignment = 0x20, size = 0x20 { section __create_delete_data__ };

/* Define a block for the Dynamic Task data. */
define block __dynamic_task_data_block__ with alignment = 0x40, size = 0x40 { section __dynamic_task_data__ };

/* Define a block for the LED Task data. */
define block __led_task_data_block__ with alignment = 0x20, size = 0x20 { section __led_task_data__ };

/* Define a block for the Poll Q task data. */
define block __poll_q_data_block__ with alignment = 0x20, size = 0x20 { section __poll_q_data__ };

/* Define a block for the Binary Semaphore Task data. */
define block __binary_semaphore_task_data_block__ with alignment = 0x40, size = 0x40 { section __binary_semaphore_task_data__ };

/* Define a block for the Timer Demo data. */
define block __timer_demo_task_data_block__ with alignment = 0x800, size = 0x800
{
	section __timer_demo_task_data__,
	section __timer_demo_task_zero_data__
};

/* Define a block for the Timer Demo data. */
define block __task_notify_demo_data_block__ with alignment = 0x20, size = 0x20
{
	section __task_notify_demo_data__,
	section __task_notify_demo_zero_data__
};

/* Define a block for the Recursive Mutex Demo data. */
define block __rec_mutex_data_block__ with alignment = 0x400, size = 0x400
{
	section __rec_mutex_data__
};

/* Define a block for the Timer Demo data. */
define block __idle_task_data_block__ with alignment = 0x20, size = 0x20
{
	section __idle_task_data__,
	section __idle_task_zero_data__
};

/* Place the kernel's data at the start of SRAM (immediately after the stacks). */
/* place at start of RAM_region { block __kernel_data_block__ }; */

place in RAM_region   { readwrite,
						block __kernel_data_block__,
						block __maths_test_data_block__,
						block __block_q_data_block__,
						block __block_tim_data_block__,
						block __com_test_data_block__,
						block __counting_semaphore_task_data_block__,
						block __create_delete_data_block__,
//-------------------------------BLINKY INCLUDE-------------------------------//
//----------------------------------------------------------------------------//
                                                block __blinky_data_block__,
                                                block __blinky_q_data_block__,
//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//
						block __dynamic_task_data_block__,
						block __led_task_data_block__,
						block __poll_q_data_block__,
						block __binary_semaphore_task_data_block__,
						block __timer_demo_task_data_block__,
						block __task_notify_demo_data_block__,
						block __rec_mutex_data_block__,
						block __idle_task_data_block__,
						block HEAP };
place in DRAM_region  { section DRAM };

/* Export symbols required by c code. */
export symbol __ICFEDIT_intvec_start__;
export symbol __ICFEDIT_region_ROM_start__;
export symbol __ICFEDIT_region_ROM_end__;
