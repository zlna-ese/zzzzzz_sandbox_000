/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/
//----------------------------------------------------------------------------//
// MODIFIED PORTSPECIFICS.H :added SafeRTOS Blinky Task for TI TMS570LC43xHDK
//----------------------------------------------------------------------------//
//Thomas Latrofa
//Zoomlion Heavy Industries North America
//11-25-2019
#ifndef PORT_SPECIFICS_H
#define PORT_SPECIFICS_H

/* SafeRTOS Includes */
#include "SafeRTOS_API.h"
//----------------------------------------------------------------------------//
// START SafeRTOS BLINKY Constants
//----------------------------------------------------------------------------//
// Stack size 
#define portspecBLINKY_STACK_SIZE	( 512U )
// Task stacks  
extern portInt8Type acBlinkyStack[];
//----------------------------------------------------------------------------//
// MPU / LINKER parameters
//----------------------------------------------------------------------------//
// Only generate if Blinky File exists
#ifdef BLINKY_C
// Define RAM SECTION so all Blinky static data is in the same section. 
#define portspecBLINKY_DATA_SECTION		_Pragma("location=\"__blinky_data__\"")
//Linker Defined Symbol for Reserved Data Section Start Address 
extern portUInt32Type __blinky_data_block__$$Base;
extern portUInt32Type __blinky_data_block__$$Limit;
// These constants are used to give the Unprivileged tasks
// access to the file scope check variables. 
#define portspecBLINKY_DATA_ADDR		( ( void * ) &__blinky_data_block__$$Base )
#define portspecBLINKY_DATA_SIZE		( ( portUInt32Type ) &__blinky_data_block__$$Limit - ( portUInt32Type ) &__blinky_data_block__$$Base )
// This definition corresponds to the xMPUParameters structure that is passed
// to xTaskCreate() as part of the xTaskParameters structure when tasks are created. 
// It defines the privilege level and region definitions for the task(s). 
#define portspecBLINKY_MPU_PRIV_NO_REGION_PARMS	        \
pdTRUE,							\
{							\
	mpuPRIVILEGED_TASK,				\
	{						\
		{ NULL, 0UL, 0UL, 0UL },	        \
		{ NULL, 0UL, 0UL, 0UL },	        \
		{ NULL, 0UL, 0UL, 0UL },		\
		{ NULL, 0UL, 0UL, 0UL },		\
		{ NULL, 0UL, 0UL, 0UL },		\
		{ NULL, 0UL, 0UL, 0UL }			\
	}											\
}
#define xBlinkyPortParameters		portspecBLINKY_MPU_PRIV_NO_REGION_PARMS
#endif
//----------------------------------------------------------------------------//
// END SafeRTOS BLINKY Constants
//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//
// START SafeRTOS BLINKY QUEUE Constants
//----------------------------------------------------------------------------//
#define portspecBLINKY_Q_STACK_SIZE			( 512U )
#define portspecBLINKY_Q_QUEUE_LENGTH_1		( 1U )
#define portspecBLINKY_Q_QUEUE_LENGTH_5		( 5U )
#define portspecBLINKY_Q_QUEUE_ITEM_SIZE		( sizeof( portUInt16Type ) )
#define portspecBLINKY_Q_BUFFER_LENGTH_1		( ( portspecBLINKY_Q_QUEUE_LENGTH_1 * portspecBLINKY_Q_QUEUE_ITEM_SIZE ) + portQUEUE_OVERHEAD_BYTES )
#define portspecBLINKY_Q_BUFFER_LENGTH_5		( ( portspecBLINKY_Q_QUEUE_LENGTH_5 * portspecBLINKY_Q_QUEUE_ITEM_SIZE ) + portQUEUE_OVERHEAD_BYTES )

/* Task stacks */
extern portInt8Type acBlinkyQTask1Stack[];
extern portInt8Type acBlinkyQTask2Stack[];

/* Queue buffers */
extern portInt8Type acBlinkyQBuffer[];

/* MPU parameters */
#ifdef BLINKYQ_C

/* Define portspecBLOCK_Q_DATA_SECTION
 * so that all Blocking Queue static data is in the same section. */
#define portspecBLINKY_Q_DATA_SECTION	_Pragma("location=\"__blinky_q_data__\"")

/* A linker defined symbol that gives the start address of the Blocking Queue
 * data section. */
extern portUInt32Type __blinky_q_data_block__$$Base;
extern portUInt32Type __blinky_q_data_block__$$Limit;

/* The size of the Blocking Queue data section. */
#define portspecBLINKY_Q_DATA_ADDR		( ( void * ) &__blinky_q_data_block__$$Base )
#define portspecBLINKY_Q_DATA_SIZE		( ( portUInt32Type ) &__blinky_q_data_block__$$Limit - ( portUInt32Type ) &__blinky_q_data_block__$$Base )

/* This definition corresponds to the xUsingFPU and xMPUParameters structure
 * members that are passed to xTaskCreate() as part of the xTaskParameters
 * structure when the Blocking Queue tasks are created. It defines the
 * privilege level and region definitions for the Blocking Queue tasks as well
 * as informing the scheduler that the FPU will not be used by these tasks.
 * All the Blocking Queue tasks run in Unprivileged (User) mode with 1
 * additional region that corresponds to the linker section that holds the
 * Blocking Queue static data. */
#define portspecBLINKY_Q_TASK_PARAMETERS		\
pdTRUE,						\
{							\
	mpuPRIVILEGED_TASK,				\
	{						\
		{ NULL, 0UL, 0UL, 0UL },	        \
		{ NULL, 0UL, 0UL, 0UL },	        \
		{ NULL, 0UL, 0UL, 0UL },		\
		{ NULL, 0UL, 0UL, 0UL },		\
		{ NULL, 0UL, 0UL, 0UL },		\
		{ NULL, 0UL, 0UL, 0UL }			\
	}						\
}

#define xBlinkyQueueConsumerTask1PortParameters	portspecBLINKY_Q_TASK_PARAMETERS
#define xBlinkyQueueProducerTask1PortParameters	portspecBLINKY_Q_TASK_PARAMETERS

#endif /* BLINKYQ_C */


/*-----------------------------------------------------------------------------
 * Constants common to all demo tasks
 *---------------------------------------------------------------------------*/

/* This constant is used where it is desirable to locate all task TCBs in a
 * specified area of memory.
 * Not required for this product variant. */
#define portspecTCB_DATA_SECTION

/* This constant is used where it is desirable to locate all common privileged
 * demo data in a specified area of memory.
 * Not required for this product variant. */
#define portspecCOMMON_PRIV_DATA_SECTION

/* The Flash tasks and the ComTest tasks need access to the GPIO peripheral
 * register address region in order to toggle the LED indicators. */
#define portspecLED_PERIPHERALS_ADDR	( ( void * ) 0xFFF7B800UL )
#define portspecLED_PERIPHERALS_SIZE	( 0x00000100UL )


/*-----------------------------------------------------------------------------
 * Maths Test task parameters
 *---------------------------------------------------------------------------*/

/* Stack size */
#define portspecMATHS_TEST_STACK_SIZE	( configMINIMAL_STACK_SIZE_WITH_FPU )

/* Task stacks */
extern portInt8Type acUnprivilegedTask1Stack[];
extern portInt8Type acUnprivilegedTask2Stack[];
extern portInt8Type acUnprivilegedTask3Stack[];
extern portInt8Type acUnprivilegedTask4Stack[];

extern portInt8Type acPrivilegedTask1Stack[];
extern portInt8Type acPrivilegedTask2Stack[];
extern portInt8Type acPrivilegedTask3Stack[];
extern portInt8Type acPrivilegedTask4Stack[];

#ifdef MATHS_TEST_C

/* MPU parameters */
/* Define portspecMATHS_TEST_DATA_SECTION
 * so that all Maths Test static data is in the same section. */
#define portspecMATHS_TEST_DATA_SECTION		_Pragma("location=\"__maths_test_data__\"")

/* Linker Defined Symbol for Reserved Data Section Start Address */
extern portUInt32Type __maths_test_data_block__$$Base;
extern portUInt32Type __maths_test_data_block__$$Limit;

/* These constants are used to give the Unprivileged tasks
 * access to the file scope check variables. */
#define portspecMATHS_TEST_DATA_ADDR		( ( void * ) &__maths_test_data_block__$$Base )
#define portspecMATHS_TEST_DATA_SIZE		( ( portUInt32Type ) &__maths_test_data_block__$$Limit - ( portUInt32Type ) &__maths_test_data_block__$$Base )
/* This definition corresponds to the xMPUParameters structure that is passed
 * to xTaskCreate() as part of the xTaskParameters structure when the Maths
 * Test tasks are created. It defines the privilege level and region
 * definitions for the Maths test tasks. All the Blocking Queue tasks run
 * in Unprivileged (User) mode with 1 additional region that corresponds to the
 * linker section that holds the Blocking Queue static data. */
#define portspecMATHS_MPU_PRIV_NO_REGION_PARMS	\
pdTRUE,											\
{												\
	mpuPRIVILEGED_TASK,							\
	{											\
		{ NULL, 0UL, 0UL, 0UL },				\
		{ NULL, 0UL, 0UL, 0UL },				\
		{ NULL, 0UL, 0UL, 0UL },				\
		{ NULL, 0UL, 0UL, 0UL },				\
		{ NULL, 0UL, 0UL, 0UL },				\
		{ NULL, 0UL, 0UL, 0UL }					\
	}											\
}

#define portspecMATHS_MPU_UNPRIV_REGION_PARMS						\
pdTRUE,																\
{																	\
	mpuUNPRIVILEGED_TASK,											\
	{																\
		{ 															\
			portspecMATHS_TEST_DATA_ADDR,							\
			portspecMATHS_TEST_DATA_SIZE,							\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE |	\
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY ),	\
			0UL														\
		},															\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL }										\
	}																\
}

#define xMathsPrivTask1PortParameters		portspecMATHS_MPU_PRIV_NO_REGION_PARMS
#define xMathsPrivTask2PortParameters		portspecMATHS_MPU_PRIV_NO_REGION_PARMS
#define xMathsPrivTask3PortParameters		portspecMATHS_MPU_PRIV_NO_REGION_PARMS
#define xMathsPrivTask4PortParameters		portspecMATHS_MPU_PRIV_NO_REGION_PARMS

#define xMathsUnprivTask1PortParameters		portspecMATHS_MPU_UNPRIV_REGION_PARMS
#define xMathsUnprivTask2PortParameters		portspecMATHS_MPU_UNPRIV_REGION_PARMS
#define xMathsUnprivTask3PortParameters		portspecMATHS_MPU_UNPRIV_REGION_PARMS
#define xMathsUnprivTask4PortParameters		portspecMATHS_MPU_UNPRIV_REGION_PARMS

#endif /* MATHS_TEST_C */


/*-----------------------------------------------------------------------------
 * Blocking Queue demo parameters.
 *---------------------------------------------------------------------------*/

/* Constants */
#define portspecBLOCK_Q_STACK_SIZE			( 512U )
#define portspecBLOCK_Q_QUEUE_LENGTH_1		( 1U )
#define portspecBLOCK_Q_QUEUE_LENGTH_5		( 5U )
#define portspecBLOCK_Q_QUEUE_ITEM_SIZE		( sizeof( portUInt16Type ) )
#define portspecBLOCK_Q_BUFFER_LENGTH_1		( ( portspecBLOCK_Q_QUEUE_LENGTH_1 * portspecBLOCK_Q_QUEUE_ITEM_SIZE ) + portQUEUE_OVERHEAD_BYTES )
#define portspecBLOCK_Q_BUFFER_LENGTH_5		( ( portspecBLOCK_Q_QUEUE_LENGTH_5 * portspecBLOCK_Q_QUEUE_ITEM_SIZE ) + portQUEUE_OVERHEAD_BYTES )

/* Task stacks */
extern portInt8Type acQueueTestTask1Stack[];
extern portInt8Type acQueueTestTask2Stack[];
extern portInt8Type acQueueTestTask3Stack[];
extern portInt8Type acQueueTestTask4Stack[];
extern portInt8Type acQueueTestTask5Stack[];
extern portInt8Type acQueueTestTask6Stack[];

/* Queue buffers */
extern portInt8Type acQueue1Buffer[];
extern portInt8Type acQueue2Buffer[];
extern portInt8Type acQueue3Buffer[];

/* MPU parameters */
#ifdef BLOCK_Q_C

/* Define portspecBLOCK_Q_DATA_SECTION
 * so that all Blocking Queue static data is in the same section. */
#define portspecBLOCK_Q_DATA_SECTION	_Pragma("location=\"__block_q_data__\"")

/* A linker defined symbol that gives the start address of the Blocking Queue
 * data section. */
extern portUInt32Type __block_q_data_block__$$Base;
extern portUInt32Type __block_q_data_block__$$Limit;

/* The size of the Blocking Queue data section. */
#define portspecBLOCK_Q_DATA_ADDR		( ( void * ) &__block_q_data_block__$$Base )
#define portspecBLOCK_Q_DATA_SIZE		( ( portUInt32Type ) &__block_q_data_block__$$Limit - ( portUInt32Type ) &__block_q_data_block__$$Base )

/* This definition corresponds to the xUsingFPU and xMPUParameters structure
 * members that are passed to xTaskCreate() as part of the xTaskParameters
 * structure when the Blocking Queue tasks are created. It defines the
 * privilege level and region definitions for the Blocking Queue tasks as well
 * as informing the scheduler that the FPU will not be used by these tasks.
 * All the Blocking Queue tasks run in Unprivileged (User) mode with 1
 * additional region that corresponds to the linker section that holds the
 * Blocking Queue static data. */
#define portspecBLOCK_Q_TASK_PARAMETERS								\
pdFALSE,															\
{																	\
	mpuUNPRIVILEGED_TASK,											\
	{																\
		{															\
			portspecBLOCK_Q_DATA_ADDR,								\
			portspecBLOCK_Q_DATA_SIZE,								\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE |	\
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY ),	\
			0UL														\
		},															\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL }										\
	}																\
}

#define xBlockingQueueConsumerTask1PortParameters	portspecBLOCK_Q_TASK_PARAMETERS
#define xBlockingQueueProducerTask1PortParameters	portspecBLOCK_Q_TASK_PARAMETERS
#define xBlockingQueueConsumerTask2PortParameters	portspecBLOCK_Q_TASK_PARAMETERS
#define xBlockingQueueProducerTask2PortParameters	portspecBLOCK_Q_TASK_PARAMETERS
#define xBlockingQueueProducerTask3PortParameters	portspecBLOCK_Q_TASK_PARAMETERS
#define xBlockingQueueConsumerTask3PortParameters	portspecBLOCK_Q_TASK_PARAMETERS

#endif /* BLOCK_Q_C */

/*-----------------------------------------------------------------------------
 * Block Time Test task parameters
 *---------------------------------------------------------------------------*/

 /* Task constants */
#define portspecBLOCK_TIME_STACK_SIZE		( 512U )
#define portspecBLOCK_TIME_QUEUE_LENGTH		( 5U )
#define portspecBLOCK_TIME_QUEUE_ITEM_SIZE	( sizeof( portBaseType ) )
#define portspecBLOCK_TIME_BUFFER_LENGTH	( ( portspecBLOCK_TIME_QUEUE_LENGTH * portspecBLOCK_TIME_QUEUE_ITEM_SIZE ) + portQUEUE_OVERHEAD_BYTES )

/* Task stacks */
extern portInt8Type acBlockTimeTestTask1Stack[];
extern portInt8Type acBlockTimeTestTask2Stack[];

/* The buffer for the queue to use. */
extern portInt8Type acQueueBuffer[];

/* MPU parameters */
#ifdef BLOCK_TIME_TEST_C

/* Define portspecBLOCK_TIME_TEST_DATA_SECTION
 * so that all Block Time Test static data is in the same section. */
#define portspecBLOCK_TIME_TEST_DATA_SECTION	_Pragma("location=\"__block_tim_data__\"")

/* A linker defined symbol that gives the start address of the Block Time Test
 * data section. */
extern portUInt32Type __block_tim_data_block__$$Base;
extern portUInt32Type __block_tim_data_block__$$Limit;

/* The size of the Block Time Test data section. */
#define portspecBLOCK_TIME_TEST_DATA_ADDR		( ( void * ) &__block_tim_data_block__$$Base )
#define portspecBLOCK_TIME_TEST_DATA_SIZE		( ( portUInt32Type ) &__block_tim_data_block__$$Limit - ( portUInt32Type ) &__block_tim_data_block__$$Base )

/* This definition corresponds to the xUsingFPU and xMPUParameters structure
 * members that are passed to xTaskCreate() as part of the xTaskParameters
 * structure when the Block Time Test tasks are created. It defines the
 * privilege level and region definitions for the Block Time Test tasks as well
 * as informing the scheduler that the FPU will not be used by these tasks.
 * All the Block Time Test tasks run in Unprivileged (User) mode with 1
 * additional region that corresponds to the linker section that holds the
 * Block Time Test static data. */
#define portspecBLOCK_TIME_TEST_PARAMETERS							\
pdFALSE,															\
{																	\
	mpuUNPRIVILEGED_TASK,											\
	{																\
		{															\
			portspecBLOCK_TIME_TEST_DATA_ADDR,						\
			portspecBLOCK_TIME_TEST_DATA_SIZE,						\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE |	\
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY ),	\
			0UL														\
		},															\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL }										\
	}																\
}

#define xPrimaryBlockTimeTestTaskPortParameters		portspecBLOCK_TIME_TEST_PARAMETERS
#define xSecondaryBlockTimeTestTaskPortParameters	portspecBLOCK_TIME_TEST_PARAMETERS

#endif /* BLOCK_TIME_TEST_C */


/*-----------------------------------------------------------------------------
 * ComTest demo parameters.
 *---------------------------------------------------------------------------*/

/* Constants */
#define portspecCOM_TEST_TASK_STACK_SIZE	( 512U )

/* Task stacks */
extern portInt8Type acTxStack[];
extern portInt8Type acRxStack[];

/* MPU parameters */
#ifdef COMTEST_C

/* Define portspecCOM_TEST_DATA_SECTION and portspecCOM_TEST_ZERO_DATA_SECTION
 * so that all Com Test static data is in the same section. */
#define portspecCOM_TEST_DATA_SECTION		_Pragma("location=\"__com_test_data__\"")
#define portspecCOM_TEST_ZERO_DATA_SECTION	_Pragma("location=\"__com_test_zero_data__\"")

/* A linker defined symbol that gives the start address of the ComTest data
 * section. */
extern portUInt32Type __com_test_data_block__$$Base;
extern portUInt32Type __com_test_data_block__$$Limit;

/* The size of the Com Test data section. */
#define portspecCOM_TEST_DATA_ADDR			( ( void * ) &__com_test_data_block__$$Base )
#define portspecCOM_TEST_DATA_SIZE			( ( portUInt32Type ) &__com_test_data_block__$$Limit - ( portUInt32Type ) &__com_test_data_block__$$Base )

/* The COM Test Tasks also need access to the SCI control registers. */
#define portspecSCI_PERIPHERAL_ADDR			( ( void * ) 0xFFF7E400UL )
#define portspecSCI_PERIPHERAL_SIZE			( 0x00000100UL )

/* This definition corresponds to the xUsingFPU and xMPUParameters structure
 * members that are passed to xTaskCreate() as part of the xTaskParameters
 * structure when the ComTest tasks are created. It defines the privilege level
 * and region definitions for the ComTest tasks as well as informing the
 * scheduler that the FPU will not be used by these tasks. All the ComTest
 * tasks run in Unprivileged (User) mode with 3 additional regions: the first
 * corresponds to the linker section that holds the ComTest tasks static data,
 * the second allows access to the N2HET1 peripheral registers and the third
 * allows access to the SCI peripheral registers. */
#define portspecCOM_TEST_PARAMETERS									\
pdFALSE,															\
{																	\
	mpuUNPRIVILEGED_TASK,											\
	{																\
		{															\
			portspecCOM_TEST_DATA_ADDR,								\
			portspecCOM_TEST_DATA_SIZE,								\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE |	\
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY ),	\
			0UL														\
		},															\
		{															\
			portspecLED_PERIPHERALS_ADDR, 							\
			portspecLED_PERIPHERALS_SIZE,							\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE |	\
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_PERIPHERAL_DEFAULT_CACHE_POLICY ),		\
			0UL														\
		},															\
		{															\
			portspecSCI_PERIPHERAL_ADDR, 							\
			portspecSCI_PERIPHERAL_SIZE,							\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE |	\
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_PERIPHERAL_DEFAULT_CACHE_POLICY ),		\
			0UL														\
		},															\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL }										\
	}																\
}

#define xComTxTaskPortParameters	portspecCOM_TEST_PARAMETERS
#define xComRxTaskPortParameters	portspecCOM_TEST_PARAMETERS

#endif /* COMTEST_C */


/*-----------------------------------------------------------------------------
 * Counting Semaphore demo parameters.
 *---------------------------------------------------------------------------*/

/* Counting Semaphore Test task constants. */
#define portspecCOUNTSEM_TASK_STACK_SIZE	( 512U )
#define portspecCOUNTSEM_TASK3_STACK_SIZE	( 512U )

/* Task stacks. */
extern portInt8Type acCountSemTestTask1Stack[];
extern portInt8Type acCountSemTestTask2Stack[];
extern portInt8Type acCountSemTestTask3Stack[];

/* Counting Semaphore Test Semaphore buffers.
 * No actual data is stored into these buffers so the buffer
 * need only be large enough to hold the queue structure itself. */
extern portInt8Type acCountSem1[];
extern portInt8Type acCountSem2[];
extern portInt8Type acCountSem3[];

/* MPU parameters */
#ifdef COUNTING_SEMAPHORE_TEST_C

/* The Interrupt Task will run unprivileged. */
#define portspecCOUNTSEM_INTERRUPT_TASK_RUNS_UNPRIVILEGED	1


/* Define portspecCOUNTSEM_TASK_DATA_SECTION and
 * portspecCOUNTSEM_TASK_ZERO_DATA_SECTION so that all Counting Semaphore
 * static data is in the same section. */
#define portspecCOUNTSEM_TASK_DATA_SECTION		_Pragma("location=\"__counting_semaphore_task_data__\"")
#define portspecCOUNTSEM_TASK_ZERO_DATA_SECTION	_Pragma("location=\"__counting_semaphore_task_zero_data__\"")

/* A linker defined symbol that gives the start address of the Counting
 * Semaphore demo data section. */
extern portUInt32Type __counting_semaphore_task_data_block__$$Base;
extern portUInt32Type __counting_semaphore_task_data_block__$$Limit;

/* The size of the Counting Semaphore Test data section. */
#define portspecCOUNTSEM_TASK_DATA_ADDR			( ( void * ) &__counting_semaphore_task_data_block__$$Base )
#define portspecCOUNTSEM_TASK_DATA_SIZE			( ( portUInt32Type ) &__counting_semaphore_task_data_block__$$Limit - ( portUInt32Type ) &__counting_semaphore_task_data_block__$$Base )

/* This definition corresponds to the xUsingFPU and xMPUParameters structure
 * members that are passed to xTaskCreate() as part of the xTaskParameters
 * structure when the Counting Semaphore tasks are created. It defines the
 * privilege level and region definitions for the Counting Semaphore tasks as
 * well as informing the scheduler that the FPU will not be used by these tasks.
 * All the Counting Semaphore tasks run in Unprivileged (User) mode with 1
 * additional region that corresponds to the linker section that holds the
 * Counting Semaphore static data. */
#define portspecCOUNTING_SEMAPHORE_PARAMETERS						\
pdFALSE,															\
{																	\
	mpuUNPRIVILEGED_TASK,											\
	{																\
		{															\
			portspecCOUNTSEM_TASK_DATA_ADDR,						\
			portspecCOUNTSEM_TASK_DATA_SIZE,						\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE | \
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY ),	\
			0UL														\
		},															\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL }										\
	}																\
}

#define portspecINTERRUPT_SEMAPHORE_PARAMETERS	\
pdFALSE,										\
{												\
	mpuPRIVILEGED_TASK,							\
	{											\
		{ NULL, 0UL, 0UL, 0UL },				\
		{ NULL, 0UL, 0UL, 0UL },				\
		{ NULL, 0UL, 0UL, 0UL },				\
		{ NULL, 0UL, 0UL, 0UL },				\
		{ NULL, 0UL, 0UL, 0UL },				\
		{ NULL, 0UL, 0UL, 0UL }					\
	}											\
}

#define portspecINTERRUPT_MPU_UPDATE_PARAMETERS						\
{																	\
	mpuUNPRIVILEGED_TASK,											\
	{																\
		{															\
			portspecCOUNTSEM_TASK_DATA_ADDR,						\
			portspecCOUNTSEM_TASK_DATA_SIZE,						\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE |	\
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY ),	\
			0UL														\
		},															\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL }										\
	}																\
}

#define xCountSemaphoreTestTask1PortParameters		portspecCOUNTING_SEMAPHORE_PARAMETERS
#define xCountSemaphoreTestTask2PortParameters		portspecCOUNTING_SEMAPHORE_PARAMETERS
#define xCountSemaphoreTestTask3PortParameters		portspecINTERRUPT_SEMAPHORE_PARAMETERS
#define xCountSemaphoreTestTask3MpuUpdParameters	portspecINTERRUPT_MPU_UPDATE_PARAMETERS

#endif /* COUNTING_SEMAPHORE_TEST_C */


/*-----------------------------------------------------------------------------
 * Create Delete demo parameters.
 *---------------------------------------------------------------------------*/

 /* Task constants */
#define portspecCREATE_TASK_STACK_SIZE		( 1024UL )
#define portspecSUICIDAL_TASK_STACK_SIZE	( configMINIMAL_STACK_SIZE_NO_FPU )

/* Task stacks */
extern portInt8Type acCreateTaskStack[];
extern portInt8Type acSuicidalTask1Stack[];
extern portInt8Type acSuicidalTask2Stack[];

/* MPU parameters */
#ifdef SUICIDE_TASK_C

/* Define portspecCREATE_DELETE_DATA_SECTION so that all Create Delete Test
 * static data is in the same section. */
#define portspecCREATE_DELETE_DATA_SECTION	_Pragma("location=\"__create_delete_data__\"")

/* A linker defined symbol that gives the start address of the Create Delete
 * Test data section. */
extern portUInt32Type __create_delete_data_block__$$Base;
extern portUInt32Type __create_delete_data_block__$$Limit;

/* The size of the Create Delete Test data section. */
#define portspecCREATE_DELETE_DATA_ADDR		( ( void * ) &__create_delete_data_block__$$Base )
#define portspecCREATE_DELETE_DATA_SIZE		( ( portUInt32Type ) &__create_delete_data_block__$$Limit - ( portUInt32Type ) &__create_delete_data_block__$$Base )

/* This definition corresponds to the xUsingFPU and xMPUParameters structure
 * members that are passed to xTaskCreate() as part of the xTaskParameters
 * structure when the Create Delete Test tasks are created. It defines the
 * privilege level and region definitions for the Create Delete Test tasks as
 * well as informing the scheduler that the FPU will not be used by these tasks.
 * All the Create Delete Test tasks run in Unprivileged (User) mode with 1
 * additional region that corresponds to the linker section that holds the
 * Create Delete Test static data. */
#define portspecCREATE_DELETE_TASK_PARAMETERS						\
pdFALSE,															\
{																	\
	mpuUNPRIVILEGED_TASK,											\
	{																\
		{															\
			portspecCREATE_DELETE_DATA_ADDR,						\
			portspecCREATE_DELETE_DATA_SIZE,						\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE |	\
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY ),	\
			0UL														\
		},															\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL }										\
	}																\
}

#define xCreateTaskPortParameters		portspecCREATE_DELETE_TASK_PARAMETERS
#define xSuicidalTask1PortParameters	portspecCREATE_DELETE_TASK_PARAMETERS
#define xSuicidalTask2PortParameters	portspecCREATE_DELETE_TASK_PARAMETERS

#endif /* SUICIDE_TASK_C */


/*-----------------------------------------------------------------------------
 * Dynamic task demo parameters.
 *---------------------------------------------------------------------------*/

/* Constants */
#define portspecDYNAMIC_TASK_STACK_SIZE						( 512U )
#define portspecDYNAMIC_TASK_SUSPENDED_QUEUE_LENGTH			( 1 )
#define portspecDYNAMIC_TASK_QUEUE_ITEM_SIZE				( sizeof( portUInt32Type ) )
#define portspecDYNAMIC_TASK_SUSPEND_QUEUE_BUFFER_LENGTH	( ( portspecDYNAMIC_TASK_SUSPENDED_QUEUE_LENGTH * portspecDYNAMIC_TASK_QUEUE_ITEM_SIZE ) + portQUEUE_OVERHEAD_BYTES )

/* Task stacks */
extern portInt8Type acContinuousIncrementTaskStack[];
extern portInt8Type acLimitedIncrementTaskStack[];
extern portInt8Type acCounterControlTaskStack[];
extern portInt8Type acQueueSendWhenSuspendedTaskStack[];
extern portInt8Type acQueueReceiveWhenSuspendedTaskStack[];

/* Queue buffers */
extern portInt8Type acSuspendTestQueueBuffer[];

/* MPU parameters */
#ifdef DYNAMIC_MANIPULATION_C

/* Define portspecDYNAMIC_TASK_DATA_SECTION so that all Dynamic Task static
 * data is in the same section. */
#define portspecDYNAMIC_TASK_DATA_SECTION	_Pragma("location=\"__dynamic_task_data__\"")

/* A linker defined symbol that gives the start address of the Dynamic Task
 * demo data section. */
extern portUInt32Type __dynamic_task_data_block__$$Base;
extern portUInt32Type __dynamic_task_data_block__$$Limit;

/* The size of the Dynamic task data section. */
#define portspecDYNAMIC_TASK_DATA_ADDR		( ( void * ) &__dynamic_task_data_block__$$Base )
#define portspecDYNAMIC_TASK_DATA_SIZE		( ( portUInt32Type ) &__dynamic_task_data_block__$$Limit - ( portUInt32Type ) &__dynamic_task_data_block__$$Base )

/* This definition corresponds to the xUsingFPU and xMPUParameters structure
 * members that are passed to xTaskCreate() as part of the xTaskParameters
 * structure when the Dynamic Tasks are created. It defines the privilege level
 * and region definitions for the Dynamic Tasks as well as informing the
 * scheduler that the FPU will not be used by these tasks. All the Dynamic
 * Tasks run in Unprivileged (User) mode with 1 additional region that
 * corresponds to the linker section that holds the Dynamic Tasks static data. */
#define portspecDYNAMIC_TASK_PARAMETERS							\
pdFALSE,														\
{																\
	mpuUNPRIVILEGED_TASK,										\
	{															\
		{														\
			portspecDYNAMIC_TASK_DATA_ADDR,						\
			portspecDYNAMIC_TASK_DATA_SIZE,						\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE |	\
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY ),	\
			0UL													\
		},														\
		{ NULL, 0UL, 0UL, 0UL },								\
		{ NULL, 0UL, 0UL, 0UL },								\
		{ NULL, 0UL, 0UL, 0UL },								\
		{ NULL, 0UL, 0UL, 0UL },								\
		{ NULL, 0UL, 0UL, 0UL }									\
	}															\
}

#define xContinuousIncrementTaskPortParameters			portspecDYNAMIC_TASK_PARAMETERS
#define xLimitedIncrementTaskPortParameters				portspecDYNAMIC_TASK_PARAMETERS
#define xCounterControlTaskPortParameters				portspecDYNAMIC_TASK_PARAMETERS
#define xQueueSendWhenSuspendedTaskPortParameters		portspecDYNAMIC_TASK_PARAMETERS
#define xQueueReceiveWhenSuspendedTaskPortParameters	portspecDYNAMIC_TASK_PARAMETERS

#endif /* DYNAMIC_MANIPULATION_C */


/*-----------------------------------------------------------------------------
 * LED Flash demo parameters.
 *---------------------------------------------------------------------------*/

/* Constants */
#define portspecNUMBER_OF_LEDS			( 3UL )
#define portspecLED_TASK_STACK_SIZE		( configMINIMAL_STACK_SIZE_NO_FPU )

/* Structure used to pass parameters to the LED Flash tasks. */
typedef struct LED_FLASH_TASK_PARAMETERS
{
	portUnsignedBaseType uxLEDNumber;	/* The LED to be flashed. */
	portTickType xFlashRate_ms;			/* The flash rate in milliseconds. */
} xLedFlashTaskParameters;

/* LED Flash Task input parameters */
extern const xLedFlashTaskParameters xLedTaskParameters[ portspecNUMBER_OF_LEDS ];

/* Task stacks */
extern portInt8Type acLedTaskStack[ portspecNUMBER_OF_LEDS ][ portspecLED_TASK_STACK_SIZE ];

/* MPU parameters */
#ifdef FLASH_LED_C

/* Define portspecLED_TASK_DATA_SECTION so that all LED Task static data is in
 * the same section. */
#define portspecLED_TASK_DATA_SECTION	_Pragma("location=\"__led_task_data__\"")

/* A linker defined symbol that gives the start address of the LED Flash demo
 * data section. */
extern portUInt32Type __led_task_data_block__$$Base;
extern portUInt32Type __led_task_data_block__$$Limit;

/* The size of the LED Flash demo data section. */
#define portspecLED_TASK_DATA_ADDR		( ( void * ) &__led_task_data_block__$$Base )
#define portspecLED_TASK_DATA_SIZE		( ( portUInt32Type ) &__led_task_data_block__$$Limit - ( portUInt32Type ) &__led_task_data_block__$$Base )

/* This definition corresponds to the xUsingFPU and xMPUParameters structure
 * members that are passed to xTaskCreate() as part of the xTaskParameters
 * structure when the LED Tasks are created. It defines the privilege level and
 * region definitions for the LED Tasks as well as informing the scheduler that
 * the FPU will not be used by these tasks. All the LED Tasks run in
 * Unprivileged (User) mode with 2 additional regions: The first corresponds to
 * the linker section that holds the LED task static data and the second allows
 * access to the N2HET1 peripheral registers. */
#define portspecLED_FLASH_TASK_PARAMETERS							\
pdFALSE,															\
{																	\
	mpuUNPRIVILEGED_TASK,											\
	{																\
		{															\
			portspecLED_TASK_DATA_ADDR,								\
			portspecLED_TASK_DATA_SIZE,								\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE | \
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY ),	\
			0UL														\
		},															\
		{															\
			portspecLED_PERIPHERALS_ADDR, 							\
			portspecLED_PERIPHERALS_SIZE,							\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE |	\
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_PERIPHERAL_DEFAULT_CACHE_POLICY ),		\
			0UL														\
		},															\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL }										\
	}																\
}

#define xLEDFlashTaskPortParameters		portspecLED_FLASH_TASK_PARAMETERS

#endif /* FLASH_LED_C */


/*-----------------------------------------------------------------------------
 * Poll Queue task parameters
 *---------------------------------------------------------------------------*/

/* Task constants */
#define portspecPOLL_Q_STACK_SIZE		( configMINIMAL_STACK_SIZE_NO_FPU )
#define portspecPOLL_Q_QUEUE_LENGTH		( 10U )
#define portspecPOLL_Q_QUEUE_ITEM_SIZE	( sizeof( portUInt16Type ) )
#define portspecPOLL_Q_BUFFER_LENGTH	( ( portspecPOLL_Q_QUEUE_LENGTH * portspecPOLL_Q_QUEUE_ITEM_SIZE ) + portQUEUE_OVERHEAD_BYTES )

/* Task stacks */
extern portInt8Type acPollQueueTestTask1Stack[];
extern portInt8Type acPollQueueTestTask2Stack[];
extern portInt8Type acPollQueueTestTask3Stack[];

/* Queue buffers */
extern portInt8Type acPollQueue1Buffer[];
extern portInt8Type acPollQueue2Buffer[];

/* MPU parameters */
#ifdef POLLED_Q_C

/* Define portspecPOLL_Q_DATA_SECTION so that all Poll Queue static data is in
 * the same section. */
#define portspecPOLL_Q_DATA_SECTION		_Pragma("location=\"__poll_q_data__\"")

/* A linker defined symbol that gives the start address of the Poll Queue data
 * section. */
extern portUInt32Type __poll_q_data_block__$$Base;
extern portUInt32Type __poll_q_data_block__$$Limit;

/* The size of the Poll Queue data section. */
#define portspecPOLL_Q_DATA_ADDR		( ( void * ) &__poll_q_data_block__$$Base )
#define portspecPOLL_Q_DATA_SIZE		( ( portUInt32Type ) &__poll_q_data_block__$$Limit - ( portUInt32Type ) &__poll_q_data_block__$$Base )

/* This definition corresponds to the xUsingFPU and xMPUParameters structure
 * members that are passed to xTaskCreate() as part of the xTaskParameters
 * structure when the Poll Queue tasks are created. It defines the privilege
 * level and region definitions for the Poll Queue tasks as well as informing
 * the scheduler that the FPU will not be used by these tasks. All the Poll
 * Queue tasks run in Unprivileged (User) mode with 1 additional region that
 * corresponds to the linker section that holds the Poll Queue static data. */
#define portspecPOLL_Q_PARAMETERS									\
pdFALSE,															\
{																	\
	mpuUNPRIVILEGED_TASK,											\
	{																\
		{															\
			portspecPOLL_Q_DATA_ADDR,								\
			portspecPOLL_Q_DATA_SIZE,								\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE | \
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY ),	\
			0UL														\
		},															\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL }										\
	}																\
}

#define xPolledQueueConsumerTaskPortParameters	portspecPOLL_Q_PARAMETERS
#define xPolledQueueProducerTaskPortParameters	portspecPOLL_Q_PARAMETERS
#define xPolledQueueSendFrontTaskPortParameters	portspecPOLL_Q_PARAMETERS

#endif /* POLLED_Q_C */


/*-----------------------------------------------------------------------------
 * Binary Semaphore demo parameters.
 *---------------------------------------------------------------------------*/

/* Constants */
#define portspecSEMAPHORE_TASK_STACK_SIZE	( 512U )

/* Task stacks */
extern portInt8Type acSemaphoreTestTask1Stack[];
extern portInt8Type acSemaphoreTestTask2Stack[];
extern portInt8Type acSemaphoreTestTask3Stack[];
extern portInt8Type acSemaphoreTestTask4Stack[];

/* Semaphore buffers.
 * No actual data is stored into these buffers so the buffer need only be large
 * enough to hold the queue structure itself. */
extern portInt8Type acSemaphore1[];
extern portInt8Type acSemaphore2[];

/* MPU parameters */
#ifdef SEMAPHORE_TEST_C

/* Define portspecSEMAPHORE_TASK_DATA_SECTION so that all Binary Semaphore
 * static data is in the same section. */
#define portspecSEMAPHORE_TASK_DATA_SECTION			_Pragma("location=\"__binary_semaphore_task_data__\"")

/* A linker defined symbol that gives the start address of the Binary Semaphore
 * demo data section. */
extern portUInt32Type __binary_semaphore_task_data_block__$$Base;
extern portUInt32Type __binary_semaphore_task_data_block__$$Limit;

/* The size of the Binary Semaphore demo data section. */
#define portspecBINARY_SEMAPHORE_TASK_DATA_ADDR		( ( void * ) &__binary_semaphore_task_data_block__$$Base )
#define portspecBINARY_SEMAPHORE_TASK_DATA_SIZE		( ( portUInt32Type ) &__binary_semaphore_task_data_block__$$Limit - ( portUInt32Type ) &__binary_semaphore_task_data_block__$$Base )

/* This definition corresponds to the xUsingFPU and xMPUParameters structure
 * members that are passed to xTaskCreate() as part of the xTaskParameters
 * structure when the Binary Semaphore tasks are created. It defines the
 * privilege level and region definitions for the Binary Semaphore tasks as
 * well as informing the scheduler that the FPU will not be used by these tasks.
 * All the Binary Semaphore tasks run in Unprivileged (User) mode with 1
 * additional region that corresponds to the linker section that holds the
 * Binary Semaphore tasks static data. */
#define portspecBINARY_SEMAPHORE_PARAMETERS							\
pdFALSE,															\
{																	\
	mpuUNPRIVILEGED_TASK,											\
	{																\
		{															\
			portspecBINARY_SEMAPHORE_TASK_DATA_ADDR,				\
			portspecBINARY_SEMAPHORE_TASK_DATA_SIZE,				\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE | \
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY ),	\
			0UL														\
		},															\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL }										\
	}																\
}

#define xSemaphoreTestTask1PortParameters	portspecBINARY_SEMAPHORE_PARAMETERS
#define xSemaphoreTestTask2PortParameters	portspecBINARY_SEMAPHORE_PARAMETERS
#define xSemaphoreTestTask3PortParameters	portspecBINARY_SEMAPHORE_PARAMETERS
#define xSemaphoreTestTask4PortParameters	portspecBINARY_SEMAPHORE_PARAMETERS

#endif /* SEMAPHORE_TEST_C */


/*-----------------------------------------------------------------------------
 * Timer demo parameters.
 *---------------------------------------------------------------------------*/

/* Constants */
#define portspecTIMER_TASK_STACK_SIZE	( 512UL )

/* Task stacks */
extern portInt8Type acTimerTestTaskStack[ portspecTIMER_TASK_STACK_SIZE ];

/* MPU parameters */
#ifdef TIMERTEST_C

/* Define portspecTIMER_TEST_DATA_SECTION and
 * portspecTIMER_TEST_ZERO_DATA_SECTION to nothing as this port cannot
 * use this syntax method to assign variables to sections. */
#define portspecTIMER_TEST_DATA_SECTION			_Pragma("location=\"__timer_demo_task_data__\"")
#define portspecTIMER_TEST_ZERO_DATA_SECTION	_Pragma("location=\"__timer_demo_task_zero_data__\"")

/* A linker defined symbol that gives the start address of the Timer demo data
 * section. */
extern portUInt32Type __timer_demo_task_data_block__$$Base;
extern portUInt32Type __timer_demo_task_data_block__$$Limit;

/* The size of the Timer demo data section. */
#define portspecTIMER_TEST_DATA_ADDR	( ( void * ) &__timer_demo_task_data_block__$$Base )
#define portspecTIMER_TEST_DATA_SIZE	( ( portUInt32Type ) &__timer_demo_task_data_block__$$Limit - ( portUInt32Type ) &__timer_demo_task_data_block__$$Base )

/* This definition corresponds to the xMPUParameters structure that is passed
 * to xTaskCreate() as part of the xTaskParameters structure when the Timer
 * demo tasks are created. It defines the privilege level and region
 * definitions for the Timer demo tasks. All the Timer demo tasks run in
 * Unprivileged (User) mode with 1 additional region that corresponds to the
 * linker section that holds the Timer demo static data. */
#define portspecTIMER_TEST_TASK_PARAMETERS							\
pdFALSE,															\
{																	\
	mpuUNPRIVILEGED_TASK,											\
	{																\
		{															\
			portspecTIMER_TEST_DATA_ADDR,							\
			portspecTIMER_TEST_DATA_SIZE, 							\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE |	\
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY ),	\
			0UL														\
		},															\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL }										\
	}																\
}

#define xTimerTestTaskPortParameters	portspecTIMER_TEST_TASK_PARAMETERS

#endif /* TIMERTEST_C */


/*-----------------------------------------------------------------------------
 * Task Notify demo task parameters
 *---------------------------------------------------------------------------*/

/* Constants */
#define portspecNOTIFIED_TASK_STACK_SIZE	( 512UL )

/* Task stacks */
extern portInt8Type acNotifiedTaskStack[ portspecNOTIFIED_TASK_STACK_SIZE ];

/* MPU parameters */
#ifdef TASKNOTIFYDEMO_C

/* Define portspecTASK_NOTIFY_DATA_SECTION and
 * portspecTASK_NOTIFY_ZERO_DATA_SECTION to nothing as this port cannot use
 * this syntax method to assign variables to sections. */
#define portspecTASK_NOTIFY_DATA_SECTION		_Pragma("location=\"__task_notify_demo_data__\"")
#define portspecTASK_NOTIFY_ZERO_DATA_SECTION	_Pragma("location=\"__task_notify_demo_zero_data__\"")

/* A linker defined symbol that gives the start address of the Task Notify demo
 * data section. */
extern portUInt32Type __task_notify_demo_data_block__$$Base;
extern portUInt32Type __task_notify_demo_data_block__$$Limit;

/* The size of the Notify demo data section. */
#define portspecTASK_NOTIFY_DATA_ADDR		( ( void * ) &__task_notify_demo_data_block__$$Base )
#define portspecTASK_NOTIFY_DATA_SIZE		( ( portUInt32Type ) &__task_notify_demo_data_block__$$Limit - ( portUInt32Type ) &__task_notify_demo_data_block__$$Base )

/* This definition corresponds to the xMPUParameters structure that is passed
 * to xTaskCreate() as part of the xTaskParameters structure when the Notify
 * demo tasks are created. It defines the privilege level and region
 * definitions for the Notify demo tasks. All the Notify demo tasks run in
 * Unprivileged (User) mode with 1 additional region that corresponds to the
 * linker section that holds the Notify demo static data. */
#define portspecNOTIFIED_TASK_PARAMETERS							\
pdFALSE,															\
{																	\
	mpuUNPRIVILEGED_TASK,											\
	{																\
		{															\
			portspecTASK_NOTIFY_DATA_ADDR,							\
			portspecTASK_NOTIFY_DATA_SIZE,							\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE |	\
			  portmpuREGION_EXECUTE_NEVER | 						\
			  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY ),	\
			0UL														\
		},															\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL }										\
	}																\
}

#define xNotifiedTaskPortParameters		portspecNOTIFIED_TASK_PARAMETERS

#endif /* TASKNOTIFYDEMO_C */


/*-----------------------------------------------------------------------------
 * Recursive mutex task parameters
 *---------------------------------------------------------------------------*/

#define portspecMUTEX_STACK_SIZE		( 512U )

/* Mutex demo task stacks */
extern portInt8Type acRecursiveMutexControllingTaskStack[ portspecMUTEX_STACK_SIZE ];
extern portInt8Type acRecursiveMutexBlockingTaskStack[ portspecMUTEX_STACK_SIZE ];
extern portInt8Type acRecursiveMutexPollingTaskStack[ portspecMUTEX_STACK_SIZE ];

/* Queue Buffer */
extern portInt8Type acMutexBuffer[ portQUEUE_OVERHEAD_BYTES ];

#ifdef TASK_MUTEX_C

/* MPU parameters */
/* Define portspecREC_MUTEX_DATA_SECTION
 * so that all static data is in the same section. */
#define portspecMUTEX_TASK_DATA_SECTION		_Pragma("location=\"__rec_mutex_data__\"")

/* A linker defined symbol that gives the start address of the Mutex demo
 * data section. */
extern portUInt32Type __rec_mutex_data_block__$$Base;
extern portUInt32Type __rec_mutex_data_block__$$Limit;

/* The size of the Notify demo data section. */
#define portspecREC_MUTEX_DATA_ADDR		( ( void * ) &__rec_mutex_data_block__$$Base )
#define portspecREC_MUTEX_DATA_SIZE		( ( portUInt32Type ) &__rec_mutex_data_block__$$Limit - ( portUInt32Type ) &__rec_mutex_data_block__$$Base )


/* This definition corresponds to the xMPUParameters structure that is passed
 * to xTaskCreate() as part of the xTaskParameters structure when the
 * Recursive Mutex tasks are created. It defines the privilege level and region
 * definitions for the Poll Queue tasks. All the Poll Queue tasks run in
 * Unprivileged (User) mode with 1 additional region that corresponds to the
 * linker section that holds the Poll Queue static data. */
#define portspecREC_MUTEX_PARAMETERS								\
pdFALSE,															\
{																	\
	mpuUNPRIVILEGED_TASK,											\
	{																\
		{															\
			portspecREC_MUTEX_DATA_ADDR,							\
			portspecREC_MUTEX_DATA_SIZE,							\
			( portmpuREGION_PRIVILEGED_READ_WRITE_USER_READ_WRITE |	\
			  portmpuREGION_EXECUTE_NEVER |							\
			  portmpuREGION_INTERNAL_RAM_DEFAULT_CACHE_POLICY ),	\
			0U														\
		},															\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL },									\
		{ NULL, 0UL, 0UL, 0UL }										\
	}																\
}

#define xRecursiveMutexControllingTaskPortParameters	portspecREC_MUTEX_PARAMETERS
#define xRecursiveMutexBlockingTaskPortParameters		portspecREC_MUTEX_PARAMETERS
#define xRecursiveMutexPollingTaskPortParameters		portspecREC_MUTEX_PARAMETERS

#endif  /* TASK_MUTEX_C */

/*---------------------------------------------------------------------------*/

#endif /* PORT_SPECIFICS_H */
