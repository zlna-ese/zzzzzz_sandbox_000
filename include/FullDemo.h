/*
	Copyright (C)2006 onward WITTENSTEIN aerospace & simulation limited. All rights reserved.

	This file is part of the SafeRTOS product, see projdefs.h for version number
	information.

	SafeRTOS is distributed exclusively by WITTENSTEIN high integrity systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on use and distribution. It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses authorize use by processor, compiler, business unit, and product.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com

	http://www.HighIntegritySystems.com
*/

#ifndef FULL_DEMO_H
#define FULL_DEMO_H



/*-----------------------------------------------------------------------------
 * Public Constants
 *---------------------------------------------------------------------------*/

/* The SVC hook function can be implemented in either
 * C or assembler. With a C hook function, SWI_ALIAS
 * pseudo-functions can only have up to four arguments,
 * and the call sequence is less efficient. */
#define fulldemoIMPLEMENT_SVC_HOOK_IN_ASM       ( 1 )

#define fulldemoTIMER_TASK_PRIORITY		( 2U )
#define fulldemoTIMER_CMD_QUEUE_LEN		( 10U )

/* SVC numbers for demo. */
#define fulldemoSVC_INCREMENT_VALUE			( 10U )
#define fulldemoSVC_ADD_64BIT               ( 11U )
#define fulldemoSVC_FOUR_PARAMS             ( 12U )



/*-----------------------------------------------------------------------------
 * Public Prototypes
 *---------------------------------------------------------------------------*/
#ifndef  __IAR_SYSTEMS_ASM__
/* Prototype for the function that creates the demo application tasks. */
portBaseType xFullDemoCreate( void );

/* Following four prototypes are the SafeRTOS hook (or callback) functions.
 * These are passed to the kernel during initialisation.  */
void vDemoApplicationIdleHook( void );
void vDemoApplicationTickHook( void );

void vDemoApplicationTaskDeleteHook( portTaskHandleType xTaskBeingDeleted );

void vDemoApplicationErrorHook( portTaskHandleType xHandleOfTaskWithError,
								const portCharType *pcErrorString,
								portBaseType xErrorCode );

portUInt64Type uxDemoApplicationSvcHook( portUnsignedBaseType uxArg1, portUnsignedBaseType uxArg2, portUnsignedBaseType uxArg3, portUnsignedBaseType uxArg4 );

void vDemoApplicationSSIHook( portUnsignedBaseType uxSSINumber );

#pragma swi_number=fulldemoSVC_INCREMENT_VALUE
__swi void vDemoApplicationSvcIncrValue( void );

#pragma swi_number=fulldemoSVC_ADD_64BIT
__swi portUInt64Type uxDemoApplicationSvcAdd64Bit( portUInt64Type uxValue );

#pragma swi_number=fulldemoSVC_FOUR_PARAMS
__swi portUnsignedBaseType uxDemoApplicationSvcFourParams( portUInt32Type ulArg1, portUInt32Type ulArg2, portUInt32Type ulArg3, portUInt32Type ulArg4  );

/*---------------------------------------------------------------------------*/
#endif /* __IAR_SYSTEMS_ASM__ */
#endif /* FULL_DEMO_H */
